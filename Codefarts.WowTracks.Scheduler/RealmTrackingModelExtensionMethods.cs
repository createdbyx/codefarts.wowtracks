﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Linq;
    using System.Security.Policy;

    using Codefarts.WowTracks.DataModels;

    public static class RealmTrackingModelExtensionMethods
    {
        public static Uri BuildAuctionHouseUrl(this RealmTrackingModel model)
        {
            var region = WOWSharp.Community.Region.AllRegions.FirstOrDefault(x => x.Name == model.Region);
            if (region == null)
            {
                throw new ArgumentException("Could not determine region!", "model");
            }

            return new Uri(new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + region.Host, UriKind.RelativeOrAbsolute), string.Format("/api/wow/auction/data/{0}", model.Name));
        }
    }
}
