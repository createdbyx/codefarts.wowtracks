﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WOWSharp.Community;


    public class GenericCacheManager : ICacheManager
    {
        private static GenericCacheManager instance;

        public static GenericCacheManager Instance
        {
            get
            {
                return instance ?? (instance = new GenericCacheManager());
            }
        }

        /// <summary>
        /// Cache storage
        /// </summary>
        private readonly Dictionary<string, object> dataDictionary = new Dictionary<string, object>();

        /// <summary>
        /// Add data
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public Task AddDataAsync(string key, object value)
        {
            this.dataDictionary[key] = value;
            return Task.FromResult(true);
        }

        /// <summary>
        /// Remove data
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Task<object> LookupDataAsync(string key)
        {
            object o;
            this.dataDictionary.TryGetValue(key, out o);
            return Task.FromResult(o);
        }
    }
}