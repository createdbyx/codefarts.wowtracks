﻿namespace Codefarts.WowTracks.Scheduler
{
    partial class RealmEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tiInterval = new TimeIntervalControl.TimeInterval();
            this.tiAHInterval = new TimeIntervalControl.TimeInterval();
            this.chkTrackAH = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAccept.Location = new System.Drawing.Point(86, 214);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(167, 214);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tracking interval";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Auction house interval";
            // 
            // tiInterval
            // 
            this.tiInterval.CurrentTicks = ((long)(0));
            this.tiInterval.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiInterval.Days = 0;
            this.tiInterval.Hours = 0;
            this.tiInterval.Location = new System.Drawing.Point(12, 25);
            this.tiInterval.Minutes = 0;
            this.tiInterval.Name = "tiInterval";
            this.tiInterval.Seconds = 0;
            this.tiInterval.Size = new System.Drawing.Size(162, 39);
            this.tiInterval.TabIndex = 4;
            // 
            // tiAHInterval
            // 
            this.tiAHInterval.CurrentTicks = ((long)(0));
            this.tiAHInterval.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiAHInterval.Days = 0;
            this.tiAHInterval.Hours = 0;
            this.tiAHInterval.Location = new System.Drawing.Point(12, 83);
            this.tiAHInterval.Minutes = 0;
            this.tiAHInterval.Name = "tiAHInterval";
            this.tiAHInterval.Seconds = 0;
            this.tiAHInterval.Size = new System.Drawing.Size(162, 39);
            this.tiAHInterval.TabIndex = 5;
            // 
            // chkTrackAH
            // 
            this.chkTrackAH.AutoSize = true;
            this.chkTrackAH.Location = new System.Drawing.Point(15, 128);
            this.chkTrackAH.Name = "chkTrackAH";
            this.chkTrackAH.Size = new System.Drawing.Size(127, 17);
            this.chkTrackAH.TabIndex = 6;
            this.chkTrackAH.Text = "Track Auction House";
            this.chkTrackAH.UseVisualStyleBackColor = true;
            // 
            // RealmEditor
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(254, 249);
            this.Controls.Add(this.chkTrackAH);
            this.Controls.Add(this.tiAHInterval);
            this.Controls.Add(this.tiInterval);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RealmEditor";
            this.Text = "Realm Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private TimeIntervalControl.TimeInterval tiInterval;
        private TimeIntervalControl.TimeInterval tiAHInterval;
        private System.Windows.Forms.CheckBox chkTrackAH;
    }
}