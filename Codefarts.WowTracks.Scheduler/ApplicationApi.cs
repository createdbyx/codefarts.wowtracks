﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Collections.Generic;

    using Codefarts.WowTracks.DataModels;

    internal class ApplicationApi : SchedulerApi.Application
    {
        private string dataFolder;

        internal event EventHandler<ItemIdRegionModelEventArgs> RequestedDownloadItemId;

        protected virtual void OnRequestedDownloadItemId(ItemIdRegionModel e)
        {
            var handler = this.RequestedDownloadItemId;
            if (handler != null)
            {
                handler(this, new ItemIdRegionModelEventArgs(e));
            }
        }

        public override string DataFolder
        {
            get
            {
                return this.dataFolder;
            }
            set
            {
                // does nothing
                //this.dataFolder = value;
            }
        }

        public override void DownloadItemData(IEnumerable<int> newIds, string region)
        {
            foreach (var id in newIds)
            {
                this.OnRequestedDownloadItemId(new ItemIdRegionModel(id, region));
            }
        }

        internal void SetDataFolder(string folder)
        {
            this.dataFolder = folder;
        }
    }
}