﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.ComponentModel;

    using Codefarts.WowTracks.DataModels;
    using System;
    using System.Windows.Forms;

    using SafeDataBinding;

    public partial class RealmEditorControl : UserControl
    {
        private RealmTrackingModel model;
        //  FlexBindingSource<RealmTrackingModel> _dataSource;

        public event EventHandler Revert;
        public event EventHandler Update;

        public RealmEditorControl()
        {
            InitializeComponent();
            //    this.InitializeDatabindings();
            //}

            //private void InitializeDatabindings()
            //{
            //    _dataSource = new FlexBindingSource<RealmTrackingModel>();
            //    _dataSource.CreateBinding(this.txtRegion, control => control.Text, data => data.Region);
            //    _dataSource.CreateBinding(this.txtRealm, control => control.Text, data => data.Name);
            //    _dataSource.CreateBinding(this.dtpStarted, control => control.Value, data => data.StartedTracking);
            //    //_dataSource.CreateBinding(this.tiStarted, control => control.CurrentTimeSpan, data => data.StartedTrackingTime);
            //    _dataSource.CreateBinding(this.dtpLastUpdate, control => control.Value, data => data.LastUpdate);
            //    //_dataSource.CreateBinding(this.tiLastUpdate, control => control.CurrentTimeSpan, data => data.LastUpdateTime);
            //    _dataSource.CreateBinding(this.tiInterval, control => control.CurrentTimeSpan, data => data.TrackingInterval);
            //    _dataSource.CreateBinding(this.chkTrackAuctionHouse, control => control.Checked, data => data.TrackAuctionHouse);
            //    _dataSource.CreateBinding(this.dtpAHStarted, control => control.Value, data => data.StartedTrackingAuctionHouseDate);
            //    _dataSource.CreateBinding(this.tiAHStarted, control => control.CurrentTimeSpan, data => data.StartedTrackingAuctionHouseTime);
            //    _dataSource.CreateBinding(this.dtpAHLastUpdate, control => control.Value, data => data.LastAuctionHouseUpdateDate);
            //    _dataSource.CreateBinding(this.tiAHLastUpdate, control => control.CurrentTimeSpan, data => data.LastAuctionHouseUpdateTime);
            //    _dataSource.CreateBinding(this.tiAHInterval, control => control.CurrentTimeSpan, data => data.AuctionHouseTrackingInterval);
        }

        [Browsable(false)]
        public RealmTrackingModel Model
        {
            get
            {
                return this.model;
            }

            set
            {

                this.model = value != null ? value.Clone() : value;
                //  this._dataSource.CurrentItem = this.model;
                this.realmTrackingModelBindingSource.DataSource = value ?? (object)typeof(RealmTrackingModel);
                this.Visible = value != null;
            }
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            if (this.Revert != null)
            {
                this.Revert(this, EventArgs.Empty);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.Update != null)
            {
                this.Update(this, EventArgs.Empty);
            }
        }
    }
}
