﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;

    public class RemoveItemEventArgs<T> : EventArgs
    {
        public T RemovedItem
        {
            get { return this.removedItem; }
        }

        private T removedItem;

        public RemoveItemEventArgs(T removedItem)
        {
            this.removedItem = removedItem;
        }
    }
}