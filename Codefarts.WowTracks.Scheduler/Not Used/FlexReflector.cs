﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;

namespace SafeDataBinding
{
    public static class FlexReflector
    {
        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            if (expression.Body.NodeType == ExpressionType.MemberAccess)
            {
                return (expression.Body as MemberExpression).Member.Name;
            }
            throw new NotSupportedException("This overload accepts only member access lambda expressions");
        }

        public static string GetPropertyName<TItem, TResult>(Expression<Func<TItem, TResult>> expression)
        {
            if (expression.Body.NodeType == ExpressionType.MemberAccess)
            {
                return (expression.Body as MemberExpression).Member.Name;
            }
            else if (expression.Body.NodeType == ExpressionType.Convert)
            {
                var unaryExpression = expression.Body as UnaryExpression;
                return (unaryExpression.Operand as MemberExpression).Member.Name;
            }
            throw new NotSupportedException("This overload accepts only member access lambda expressions");
        }
    }

    public static class FlexReflectorExtensions
    {
        public static string GetPropertyName<T>(this T instance, Expression<Func<T, object>> propertyAccesor)
        {
            return FlexReflector.GetPropertyName(propertyAccesor);
        }
    }
}
