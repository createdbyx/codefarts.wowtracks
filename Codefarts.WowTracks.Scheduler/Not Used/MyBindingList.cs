﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public class MyBindingList<T> : BindingList<T>
    {
        public event EventHandler<RemoveItemEventArgs<T>> RemovingItem;

        protected virtual void OnRemovingItem(RemoveItemEventArgs<T> args)
        {
            var removeItem = this.RemovingItem;
            if (removeItem != null)
            {
                removeItem(this, args);
            }
        }

        protected override void RemoveItem(int index)
        {
            this.OnRemovingItem(new RemoveItemEventArgs<T>(this[index]));
            base.RemoveItem(index);
        }

        public MyBindingList(IList<T> list)
            : base(list)
        {
        }

        public MyBindingList()
        {
        }
    }
}