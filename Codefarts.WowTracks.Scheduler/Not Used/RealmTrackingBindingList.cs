﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.Collections.Generic;           

    using Codefarts.WowTracks.DataModels;

    public class RealmTrackingBindingList:MyBindingList<RealmTrackingModel>
    {
        public RealmTrackingBindingList(IList<RealmTrackingModel> realms) : base(realms)
        {
        }
    }
}