﻿namespace Codefarts.WowTracks.Scheduler
{
    partial class RealmEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkTrackAuctionHouse = new System.Windows.Forms.CheckBox();
            this.realmTrackingModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpStarted = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpLastUpdate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpTracking = new System.Windows.Forms.TabPage();
            this.tpAuctionHouse = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpAHStarted = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpAHLastUpdate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.txtRealm = new System.Windows.Forms.TextBox();
            this.btnRevert = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tiInterval = new TimeIntervalControl.TimeInterval();
            this.tiLastUpdate = new TimeIntervalControl.TimeInterval();
            this.tiStarted = new TimeIntervalControl.TimeInterval();
            this.tiAHInterval = new TimeIntervalControl.TimeInterval();
            this.tiAHLastUpdate = new TimeIntervalControl.TimeInterval();
            this.tiAHStarted = new TimeIntervalControl.TimeInterval();
            ((System.ComponentModel.ISupportInitialize)(this.realmTrackingModelBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tpTracking.SuspendLayout();
            this.tpAuctionHouse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkTrackAuctionHouse
            // 
            this.chkTrackAuctionHouse.AutoSize = true;
            this.chkTrackAuctionHouse.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.realmTrackingModelBindingSource, "TrackAuctionHouse", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTrackAuctionHouse.Location = new System.Drawing.Point(6, 6);
            this.chkTrackAuctionHouse.Name = "chkTrackAuctionHouse";
            this.chkTrackAuctionHouse.Size = new System.Drawing.Size(127, 17);
            this.chkTrackAuctionHouse.TabIndex = 22;
            this.chkTrackAuctionHouse.Text = "Track Auction House";
            this.chkTrackAuctionHouse.UseVisualStyleBackColor = true;
            // 
            // realmTrackingModelBindingSource
            // 
            this.realmTrackingModelBindingSource.DataSource = typeof(Codefarts.WowTracks.DataModels.RealmTrackingModel);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Region";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Realm";
            // 
            // dtpStarted
            // 
            this.dtpStarted.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.realmTrackingModelBindingSource, "StartedTracking", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dtpStarted.Location = new System.Drawing.Point(107, 14);
            this.dtpStarted.Name = "dtpStarted";
            this.dtpStarted.Size = new System.Drawing.Size(200, 20);
            this.dtpStarted.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Started Tracking";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Last Update";
            // 
            // dtpLastUpdate
            // 
            this.dtpLastUpdate.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.realmTrackingModelBindingSource, "LastUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dtpLastUpdate.Location = new System.Drawing.Point(107, 85);
            this.dtpLastUpdate.Name = "dtpLastUpdate";
            this.dtpLastUpdate.Size = new System.Drawing.Size(200, 20);
            this.dtpLastUpdate.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Capture Interval";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpTracking);
            this.tabControl1.Controls.Add(this.tpAuctionHouse);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(732, 343);
            this.tabControl1.TabIndex = 31;
            // 
            // tpTracking
            // 
            this.tpTracking.Controls.Add(this.label1);
            this.tpTracking.Controls.Add(this.tiInterval);
            this.tpTracking.Controls.Add(this.dtpStarted);
            this.tpTracking.Controls.Add(this.label3);
            this.tpTracking.Controls.Add(this.tiLastUpdate);
            this.tpTracking.Controls.Add(this.tiStarted);
            this.tpTracking.Controls.Add(this.dtpLastUpdate);
            this.tpTracking.Controls.Add(this.label2);
            this.tpTracking.Location = new System.Drawing.Point(4, 22);
            this.tpTracking.Name = "tpTracking";
            this.tpTracking.Padding = new System.Windows.Forms.Padding(3);
            this.tpTracking.Size = new System.Drawing.Size(724, 317);
            this.tpTracking.TabIndex = 0;
            this.tpTracking.Text = "Tracking";
            this.tpTracking.UseVisualStyleBackColor = true;
            // 
            // tpAuctionHouse
            // 
            this.tpAuctionHouse.Controls.Add(this.label4);
            this.tpAuctionHouse.Controls.Add(this.dtpAHStarted);
            this.tpAuctionHouse.Controls.Add(this.label5);
            this.tpAuctionHouse.Controls.Add(this.dtpAHLastUpdate);
            this.tpAuctionHouse.Controls.Add(this.label8);
            this.tpAuctionHouse.Controls.Add(this.chkTrackAuctionHouse);
            this.tpAuctionHouse.Controls.Add(this.tiAHInterval);
            this.tpAuctionHouse.Controls.Add(this.tiAHLastUpdate);
            this.tpAuctionHouse.Controls.Add(this.tiAHStarted);
            this.tpAuctionHouse.Location = new System.Drawing.Point(4, 22);
            this.tpAuctionHouse.Name = "tpAuctionHouse";
            this.tpAuctionHouse.Padding = new System.Windows.Forms.Padding(3);
            this.tpAuctionHouse.Size = new System.Drawing.Size(724, 317);
            this.tpAuctionHouse.TabIndex = 1;
            this.tpAuctionHouse.Text = "Auction House";
            this.tpAuctionHouse.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Started Tracking";
            // 
            // dtpAHStarted
            // 
            this.dtpAHStarted.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.realmTrackingModelBindingSource, "StartedTrackingAuctionHouse", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dtpAHStarted.Location = new System.Drawing.Point(100, 29);
            this.dtpAHStarted.Name = "dtpAHStarted";
            this.dtpAHStarted.Size = new System.Drawing.Size(200, 20);
            this.dtpAHStarted.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Capture Interval";
            // 
            // dtpAHLastUpdate
            // 
            this.dtpAHLastUpdate.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.realmTrackingModelBindingSource, "LastAuctionHouseUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dtpAHLastUpdate.Location = new System.Drawing.Point(100, 100);
            this.dtpAHLastUpdate.Name = "dtpAHLastUpdate";
            this.dtpAHLastUpdate.Size = new System.Drawing.Size(200, 20);
            this.dtpAHLastUpdate.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Last Update";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtRegion);
            this.splitContainer1.Panel1.Controls.Add(this.txtRealm);
            this.splitContainer1.Panel1.Controls.Add(this.btnRevert);
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdate);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(732, 392);
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.TabIndex = 32;
            // 
            // txtRegion
            // 
            this.txtRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.realmTrackingModelBindingSource, "Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtRegion.Enabled = false;
            this.txtRegion.Location = new System.Drawing.Point(65, 11);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(85, 20);
            this.txtRegion.TabIndex = 25;
            // 
            // txtRealm
            // 
            this.txtRealm.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.realmTrackingModelBindingSource, "Name", true));
            this.txtRealm.Enabled = false;
            this.txtRealm.Location = new System.Drawing.Point(199, 11);
            this.txtRealm.Name = "txtRealm";
            this.txtRealm.Size = new System.Drawing.Size(147, 20);
            this.txtRealm.TabIndex = 24;
            // 
            // btnRevert
            // 
            this.btnRevert.Location = new System.Drawing.Point(433, 11);
            this.btnRevert.Name = "btnRevert";
            this.btnRevert.Size = new System.Drawing.Size(75, 23);
            this.btnRevert.TabIndex = 23;
            this.btnRevert.Text = "Revert";
            this.btnRevert.UseVisualStyleBackColor = true;
            this.btnRevert.Click += new System.EventHandler(this.btnRevert_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(352, 11);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 22;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tiInterval
            // 
            this.tiInterval.CurrentTicks = ((long)(0));
            this.tiInterval.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiInterval.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "TrackingInterval", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tiInterval.Days = 0;
            this.tiInterval.Hours = 0;
            this.tiInterval.Location = new System.Drawing.Point(107, 156);
            this.tiInterval.Minutes = 0;
            this.tiInterval.Name = "tiInterval";
            this.tiInterval.Seconds = 0;
            this.tiInterval.Size = new System.Drawing.Size(162, 39);
            this.tiInterval.TabIndex = 30;
            // 
            // tiLastUpdate
            // 
            this.tiLastUpdate.CurrentTicks = ((long)(0));
            this.tiLastUpdate.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiLastUpdate.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "LastUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "T"));
            this.tiLastUpdate.Days = 0;
            this.tiLastUpdate.Hours = 0;
            this.tiLastUpdate.Location = new System.Drawing.Point(107, 111);
            this.tiLastUpdate.Minutes = 0;
            this.tiLastUpdate.Name = "tiLastUpdate";
            this.tiLastUpdate.Seconds = 0;
            this.tiLastUpdate.Size = new System.Drawing.Size(162, 39);
            this.tiLastUpdate.TabIndex = 25;
            // 
            // tiStarted
            // 
            this.tiStarted.CurrentTicks = ((long)(0));
            this.tiStarted.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiStarted.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "StartedTracking", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "T"));
            this.tiStarted.Days = 0;
            this.tiStarted.Hours = 0;
            this.tiStarted.Location = new System.Drawing.Point(107, 40);
            this.tiStarted.Minutes = 0;
            this.tiStarted.Name = "tiStarted";
            this.tiStarted.Seconds = 0;
            this.tiStarted.Size = new System.Drawing.Size(162, 39);
            this.tiStarted.TabIndex = 28;
            // 
            // tiAHInterval
            // 
            this.tiAHInterval.CurrentTicks = ((long)(0));
            this.tiAHInterval.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiAHInterval.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "AuctionHouseTrackingInterval", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "T"));
            this.tiAHInterval.Days = 0;
            this.tiAHInterval.Hours = 0;
            this.tiAHInterval.Location = new System.Drawing.Point(100, 171);
            this.tiAHInterval.Minutes = 0;
            this.tiAHInterval.Name = "tiAHInterval";
            this.tiAHInterval.Seconds = 0;
            this.tiAHInterval.Size = new System.Drawing.Size(162, 39);
            this.tiAHInterval.TabIndex = 38;
            // 
            // tiAHLastUpdate
            // 
            this.tiAHLastUpdate.CurrentTicks = ((long)(0));
            this.tiAHLastUpdate.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiAHLastUpdate.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "LastAuctionHouseUpdate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "T"));
            this.tiAHLastUpdate.Days = 0;
            this.tiAHLastUpdate.Hours = 0;
            this.tiAHLastUpdate.Location = new System.Drawing.Point(100, 126);
            this.tiAHLastUpdate.Minutes = 0;
            this.tiAHLastUpdate.Name = "tiAHLastUpdate";
            this.tiAHLastUpdate.Seconds = 0;
            this.tiAHLastUpdate.Size = new System.Drawing.Size(162, 39);
            this.tiAHLastUpdate.TabIndex = 33;
            // 
            // tiAHStarted
            // 
            this.tiAHStarted.CurrentTicks = ((long)(0));
            this.tiAHStarted.CurrentTimeSpan = System.TimeSpan.Parse("00:00:00");
            this.tiAHStarted.DataBindings.Add(new System.Windows.Forms.Binding("CurrentTicks", this.realmTrackingModelBindingSource, "StartedTrackingAuctionHouse", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "T"));
            this.tiAHStarted.Days = 0;
            this.tiAHStarted.Hours = 0;
            this.tiAHStarted.Location = new System.Drawing.Point(100, 55);
            this.tiAHStarted.Minutes = 0;
            this.tiAHStarted.Name = "tiAHStarted";
            this.tiAHStarted.Seconds = 0;
            this.tiAHStarted.Size = new System.Drawing.Size(162, 39);
            this.tiAHStarted.TabIndex = 36;
            // 
            // RealmEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "RealmEditorControl";
            this.Size = new System.Drawing.Size(732, 392);
            ((System.ComponentModel.ISupportInitialize)(this.realmTrackingModelBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tpTracking.ResumeLayout(false);
            this.tpTracking.PerformLayout();
            this.tpAuctionHouse.ResumeLayout(false);
            this.tpAuctionHouse.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkTrackAuctionHouse;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpStarted;
        private System.Windows.Forms.Label label1;
        private TimeIntervalControl.TimeInterval tiLastUpdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpLastUpdate;
        private TimeIntervalControl.TimeInterval tiStarted;
        private System.Windows.Forms.Label label3;
        private TimeIntervalControl.TimeInterval tiInterval;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpTracking;
        private System.Windows.Forms.TabPage tpAuctionHouse;
        private System.Windows.Forms.Label label4;
        private TimeIntervalControl.TimeInterval tiAHInterval;
        private System.Windows.Forms.DateTimePicker dtpAHStarted;
        private System.Windows.Forms.Label label5;
        private TimeIntervalControl.TimeInterval tiAHLastUpdate;
        private TimeIntervalControl.TimeInterval tiAHStarted;
        private System.Windows.Forms.DateTimePicker dtpAHLastUpdate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnRevert;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.TextBox txtRealm;
        private System.Windows.Forms.BindingSource realmTrackingModelBindingSource;
    }
}
