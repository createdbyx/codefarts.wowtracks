﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.IO;
    using System;
    using System.Windows.Forms;

    using Codefarts.Settings;
    using Codefarts.Settings.LinqXml;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // setup settings
            var settingsFile = Path.Combine(Environment.CurrentDirectory, "settings.xml");
            var manager = SettingsManager.Instance;
            manager.Values = new XmlDocumentLinqValues(settingsFile, true);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
