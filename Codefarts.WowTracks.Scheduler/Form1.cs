﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Collections.ObjectModel;

    using Codefarts.Logging;
    using Codefarts.Logging.WinFormControls;
    using Codefarts.Settings;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.Repository;
    using Codefarts.WowTracks.Scheduler.Properties;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    using Realm = WOWSharp.Community.Wow.Realm;

    public partial class Form1 : Form
    {
        private TrackingModel model;
        BackgroundDownloader downloader = new BackgroundDownloader();

        private ObservableCollection<DownloadProgressModel> downloadProgress;

        private TextBoxLogger textBoxLogger;

        private SettingsModel settingsData;

        private ApplicationApi applicationApi = new ApplicationApi();

        public Form1()
        {
            InitializeComponent();
        }

        public void Compose()
        {
            // try to connect with MEF types
            try
            {
                var parts = new MEFComposer();
                MEFHelpers.Compose(new[] { Path.Combine(Application.StartupPath, "Plugins") }, parts);
                RepositoryFactory.Instance.Repository = parts.Repositories.FirstOrDefault();
                Logging.Repositories.AddRange(parts.LoggingRepositories);
                foreach (var plugin in parts.Plugins)
                {
                    this.applicationApi.Plugins.Add(plugin);
                    plugin.Connect(this.applicationApi);
                }
            }
            catch
            {
                // ERR: handle error
            }
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            this.model = new TrackingModel();
            var settings = SettingsManager.Instance;
            this.applicationApi.RequestedDownloadItemId += this.applicationApi_RequestedDownloadItemId;
            this.SetupSettings();

            this.settingsData.PropertyChanged += this.settingsModel_PropertyChanged;
            this.Compose();

            this.btnTrack.Enabled = Directory.Exists(this.settingsData.DataFolder);

            this.cboType.SelectedIndex = 0;
            this.cboRegion.Items.AddRange(WOWSharp.Community.Region.AllRegions.ToArray());
            this.cboRegion.SelectedIndex = 0;

            var autoStartDownloads = settings.GetSetting(SettingConstants.AutomaticallyStartDownloadsOnStartup, false);
            this.chkAutoStartDownloads.Checked = autoStartDownloads;

            var data = await RepositoryFactory.GetRealmsBeingTracked(0, 100);
            this.model.Realms = new ObservableCollection<RealmTrackingModel>(data);
            foreach (var realm in this.model.Realms)
            {
                this.lvRealms.Items.Add(new RealmListViewItem() { Realm = realm });
            }

            this.downloader.Stopped += this.DownloaderStopped;
            this.downloader.LastAuctionHouseUpdated += this.downloader_LastAuctionHouseUpdated;
            this.downloader.DownloadProgress += this.downloader_DownloadProgress;
            this.downloader.AuctionHouseFilesSaved += (s, args) => this.applicationApi.OnAuctionHouseFilesSaved(args);
            this.downloader.ItemSaved += (s, args) => this.applicationApi.OnItemSaved(args);

            this.textBoxLogger = new TextBoxLogger(this.txtLogs) { AutoScroll = this.settingsData.AutoScrollLogs };
            Logging.Repositories.Add(this.textBoxLogger);
            Logging.Repositories.Add(new ToolStripStatusLabelLogger(this.tssDownloadStatus));

            this.settingsData.PropertyChanged += this.settingsData_PropertyChanged;
            this.downloadProgress = new ObservableCollection<DownloadProgressModel>();
            this.lbDownloads.DataSource = this.downloadProgress;

            if (autoStartDownloads)
            {
                this.StartBackgroundDownloads(false);
            }
        }

        private void settingsData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AutoScrollLogs")
            {
                this.textBoxLogger.AutoScroll = this.settingsData.AutoScrollLogs;
            }
        }

        private void applicationApi_RequestedDownloadItemId(object sender, ItemIdRegionModelEventArgs e)
        {
            this.downloader.EnqueueItemId(e.Model);
        }

        private void SetupSettings()
        {
            var settings = SettingsManager.Instance;

            this.settingsData = new SettingsModel();
            this.settingsData.DataFolder = settings.GetSetting(SettingConstants.DataFolder, string.Empty);
            this.settingsData.LogFolder = settings.GetSetting(SettingConstants.LogFolder, string.Empty);
            this.settingsData.AutomaticallyRunSchedulesOnStartup = settings.GetSetting(SettingConstants.AutomaticallyStartDownloadsOnStartup, false);
            this.settingsData.MaxProcessingTasks = settings.GetSetting(SettingConstants.MaximumProcessingTasks, 4);
            this.settingsData.MinimumWaitTimeBetweenSchedules = TimeSpan.FromMinutes(settings.GetSetting(SettingConstants.MinimumWaitTimeBetweenSchedules, 10f));
            this.settingsData.AutoScrollLogs = settings.GetSetting(SettingConstants.AutoScrollLogs, true);

            var binding = new Binding("Value", this.settingsData, "MaxProcessingTasks", false, DataSourceUpdateMode.OnPropertyChanged);
            binding.Format += (o, args) =>
                {
                    if (args.DesiredType != typeof(decimal))
                    {
                        return;
                    }

                    args.Value = decimal.Parse(args.Value.ToString());
                };

            binding.Parse += (o, args) =>
                {
                    if (args.DesiredType != typeof(int))
                    {
                        return;
                    }

                    args.Value = int.Parse(args.Value.ToString());
                };

            this.nudMaxProcessingTasks.DataBindings.Add(binding);

            binding = new Binding("CurrentTimeSpan", this.settingsData, "MinimumWaitTimeBetweenSchedules", false, DataSourceUpdateMode.OnPropertyChanged);
            this.tiWaitTimeBetweenSchedules.DataBindings.Add(binding);

            binding = new Binding("Checked", this.settingsData, "AutomaticallyRunSchedulesOnStartup", false, DataSourceUpdateMode.OnPropertyChanged);
            this.chkAutoStartDownloads.DataBindings.Add(binding);

            binding = new Binding("Checked", this.settingsData, "AutoScrollLogs", false, DataSourceUpdateMode.OnPropertyChanged);
            this.chkAutoScrollLogs.DataBindings.Add(binding);

            binding = new Binding("Text", this.settingsData, "DataFolder", false, DataSourceUpdateMode.OnPropertyChanged);
            this.txtDataFolder.DataBindings.Add(binding);

            binding = new Binding("Text", this.settingsData, "LogFolder", false, DataSourceUpdateMode.OnPropertyChanged);
            this.txtLogFolder.DataBindings.Add(binding);

            this.applicationApi.SetDataFolder(this.settingsData.DataFolder);
        }

        private void downloader_DownloadProgress(object sender, DownloadProgressEventArgs e)
        {
            this.UpdateDownloadList(e);
        }

        private void UpdateDownloadList(DownloadProgressEventArgs e)
        {
            try
            {
                if (this.lbDownloads.InvokeRequired)
                {
                    var d = new Action<DownloadProgressEventArgs>(this.UpdateDownloadList);
                    //var d = new UpdateDownloadListCallback(UpdateDownloadList);
                    this.Invoke(d, new object[] { e });
                }
                else
                {
                    var first = this.downloadProgress.FirstOrDefault(x => x.RealmName == e.RealmName);
                    if (first == null)
                    {
                        var newItem = new DownloadProgressModel();
                        newItem.Progress = e.Progress;
                        newItem.RealmName = e.RealmName;
                        newItem.TaskId = e.TaskId;
                        newItem.BytesPerSecond = e.BytesPerSecond;
                        this.downloadProgress.Add(newItem);
                    }
                    else
                    {
                        first.Progress = e.Progress;
                        first.BytesPerSecond = e.BytesPerSecond;
                    }

                    this.lbDownloads.Invalidate();
                }
            }
            catch (ObjectDisposedException ode)
            {
            }
        }

        private void downloader_LastAuctionHouseUpdated(object sender, RealmTrackingModelEventArgs e)
        {
            this.UpdateLastAuctionHouseUpdate(e.Model);
        }

        private void UpdateLastAuctionHouseUpdate(RealmTrackingModel e)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    // var d = new UpdateRealmTrackingModelCallback(UpdateLastAuctionHouseUpdate);
                    var d = new Action<RealmTrackingModel>(this.UpdateLastAuctionHouseUpdate);
                    this.Invoke(d, new object[] { e });
                }
                else
                {
                    lock (this.model)
                    {
                        var item = this.model.Realms.FirstOrDefault(x => x.Name == e.Name && x.Region == e.Region);
                        if (item != null)
                        {
                            item.LastAuctionHouseUpdate = e.LastAuctionHouseUpdate;
                            RepositoryFactory.UpdateRealm(item);
                        }
                    }
                }
            }
            catch (ObjectDisposedException ode)
            {
            }
        }

        private void DownloaderStopped(object sender, EventArgs e)
        {
            this.DownloadsHaveStopped();
        }

        private void DownloadsHaveStopped()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    var d = new Action(this.DownloadsHaveStopped);
                    this.Invoke(d, new object[] { });
                }
                else
                {

                    this.tssStartDownloads.Image = Resources.Play;
                    this.stopToolStripMenuItem.Enabled = false;
                    this.runToolStripMenuItem.Enabled = true;
                    this.runSelectedToolStripMenuItem.Enabled = true;
                }
            }
            catch (ObjectDisposedException ode)
            {

            }
        }

        private void StartBackgroundDownloads(bool selected)
        {
            lock (this.downloader)
            {
                if (this.downloader.IsDownloading)
                {
                    return;
                }

                this.tssStartDownloads.Image = Resources.Stop;
                this.stopToolStripMenuItem.Enabled = true;
                this.runToolStripMenuItem.Enabled = false;
                this.runSelectedToolStripMenuItem.Enabled = false;


                this.downloader.Realms = (selected ?
                    this.lvRealms.SelectedItems.Cast<RealmListViewItem>().Select(x => x.Realm) :
                    this.model.Realms.Where(x => x.TrackAuctionHouse)).Select(x => x.Clone()).ToList();

                try
                {
                    this.downloader.MaxProcessingTasks = this.settingsData.MaxProcessingTasks;
                    this.downloader.MinimumWaitTimeInMinutes = (int)this.settingsData.MinimumWaitTimeBetweenSchedules.TotalMinutes;
                    this.downloader.DataFolder = this.settingsData.DataFolder;
                    this.downloader.Run();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message);
                    this.DownloaderStopped(this, EventArgs.Empty);
                }
            }
        }

        private async void cboRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = this.cboRegion.SelectedItem as Region;
            try
            {
                await this.PopulateRealmList(selectedItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
        }

        private async Task PopulateRealmList(Region selectedItem)
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                Logging.Log(LogEntryType.Error, "Does not appear to have network access!", "Scheduler");
                return;
            }

            var client = new WowClient(selectedItem, null, GenericCacheManager.Instance);
            try
            {
                // Get realms
                var results = await client.GetRealmStatusAsync();
                this.cboRealm.DataSource = results.Realms;
            }
            catch (Exception ex)
            {
                Logging.Log(LogEntryType.Error, ex.Message, "Scheduler");
                if (MessageBox.Show(this, "Error getting realm status!\r\nWould you like to check for a available internet connection?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    if (!this.CheckConnection("http://www.google.com/"))
                    {
                        MessageBox.Show(this, "No internet connection detected!");
                        Logging.Log(LogEntryType.Information, "No internet connection!", "Scheduler");
                    }
                    else
                    {
                        MessageBox.Show(this, "There appears to be a valid internet connection? If you are still experiencing connection issues it may be related to " +
                            "weather or not your computer is sitting behind a router that can cause this app to think that there is a valid internet connection when there is not.");
                    }
                }
            }
        }

        private bool CheckConnection(string URL)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                var response = (HttpWebResponse)request.GetResponse();

                return response.StatusCode == HttpStatusCode.OK;
            }
            catch
            {
                return false;
            }
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.settingsData.DataFolder = dialog.SelectedPath;
                }
            }
        }

        private void btnTrack_Click(object sender, EventArgs e)
        {
            switch ((string)this.cboType.SelectedItem)
            {
                case "Realm":
                    this.AddRealm();
                    break;

                case "Guild":
                    break;

                case "Character":
                    break;
            }
        }

        private async void AddRealm()
        {
            if (this.cboRegion.SelectedItem == null && this.cboRealm.SelectedItem == null)
            {
                return;
            }

            try
            {
                var trackingModel = new RealmTrackingModel()
                        {
                            Region = ((Region)this.cboRegion.SelectedItem).Name,
                            Name = ((Realm)this.cboRealm.SelectedItem).Name,
                            StartedTracking = DateTime.Now,
                            LastUpdate = DateTime.Now.Subtract(TimeSpan.FromDays(30)),
                            TrackingInterval = TimeSpan.FromDays(1).Ticks,
                            TrackAuctionHouse = true,
                            StartedTrackingAuctionHouse = DateTime.Now,
                            LastAuctionHouseUpdate = DateTime.Now.Subtract(TimeSpan.FromDays(30)),
                            AuctionHouseTrackingInterval = TimeSpan.FromHours(1).Ticks
                        };

                if (this.model.Realms.Any(x => string.Compare(x.Name, trackingModel.Name, StringComparison.OrdinalIgnoreCase) == 0
                                            && string.Compare(x.Region, trackingModel.Region, StringComparison.OrdinalIgnoreCase) == 0))
                {
                    MessageBox.Show(this, "Already tracking!");
                    return;
                }

                this.model.Realms.Add(trackingModel);
                this.lvRealms.Items.Add(new RealmListViewItem() { Realm = trackingModel });
                await RepositoryFactory.TrackRealm(trackingModel);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private async void btnRemove_Click(object sender, EventArgs e)
        {
            switch (this.tcTrackers.SelectedIndex)
            {
                case 0:
                    if (MessageBox.Show(this, "Are you sure?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    {
                        return;
                    }

                    await this.RemoveSelectedRealms();
                    break;
            }
        }

        private async Task RemoveSelectedRealms()
        {
            while (this.lvRealms.SelectedItems.Count > 0)
            {
                var item = this.lvRealms.SelectedItems[0] as RealmListViewItem;
                await RepositoryFactory.StopTrackingRealm(item.Realm);
                for (var i = 0; i < this.model.Realms.Count; i++)
                {
                    var realm = this.model.Realms[i];
                    if (realm.Id == item.Realm.Id)
                    {
                        this.model.Realms.RemoveAt(i);
                        break;
                    }
                }

                item.Remove();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.stopToolStripMenuItem_Click(sender, EventArgs.Empty);

            foreach (var plugin in this.applicationApi.Plugins)
            {
                plugin.Disconnect();
            }

            this.applicationApi.Plugins.Clear();
        }

        private void chkAutoStartDownloads_CheckedChanged(object sender, EventArgs e)
        {
            SettingsManager.Instance.SetValue(SettingConstants.AutomaticallyStartDownloadsOnStartup, this.chkAutoStartDownloads.Checked);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tssStartDownloads_ButtonClick(object sender, EventArgs e)
        {
            if (this.downloader.IsDownloading)
            {
                this.downloader.Cancel();
            }
            else
            {
                this.StartBackgroundDownloads(false);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.lvRealms.Items)
            {
                item.Selected = true;
            }
        }

        private void removeSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btnRemove_Click(sender, e);
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.StartBackgroundDownloads(false);
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.downloader.Cancel();
            var startTime = DateTime.Now;
            while (this.downloader.IsDownloading)
            {
                // if running longer then 5 secs just exit
                if (DateTime.Now > startTime + TimeSpan.FromSeconds(5))
                {
                    break;
                }

                Thread.Sleep(50);
            }
        }

        private void settingsModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var settings = SettingsManager.Instance;
            settings.SetValue(SettingConstants.DataFolder, this.settingsData.DataFolder);
            settings.SetValue(SettingConstants.AutomaticallyStartDownloadsOnStartup, this.settingsData.AutomaticallyRunSchedulesOnStartup);
            settings.SetValue(SettingConstants.MinimumWaitTimeBetweenSchedules, this.settingsData.MinimumWaitTimeBetweenSchedules.TotalMinutes);
            settings.SetValue(SettingConstants.MaximumProcessingTasks, this.settingsData.MaxProcessingTasks);
            settings.SetValue(SettingConstants.AutoScrollLogs, this.settingsData.AutoScrollLogs);
        }

        /// <summary>Handles the Click event of the btnEdit control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            // currently only supports editing one realm record at a time
            if (this.lvRealms.SelectedItems.Count != 1)
            {
                MessageBox.Show(this, "Only one realm can be selected.");
                return;
            }

            // show the realm editing dialog 
            using (var dialog = new RealmEditor())
            {
                var item = this.lvRealms.SelectedItems[0] as RealmListViewItem;
                dialog.Realm = item.Realm;
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    item.Realm.TrackingInterval = dialog.Interval;
                    item.Realm.AuctionHouseTrackingInterval = dialog.AuctionHouseInterval;
                    item.Realm.TrackAuctionHouse = dialog.TrackAuctionHouse;
                    RepositoryFactory.UpdateRealm(item.Realm);
                }
            }
        }

        private void runSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.StartBackgroundDownloads(true);
        }

        private void btnSelectLogFolder_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.settingsData.LogFolder = dialog.SelectedPath;
                }
            }
        }
    }

   // public delegate void GenericCallback<T>(T e);
    //  public delegate void SetTextCallback(string text);
  //  public delegate void NoParametersCallback();
    // public delegate void UpdateDownloadListCallback(DownloadProgressEventArgs e);
    // public delegate void UpdateRealmTrackingModelCallback(RealmTrackingModel e);
}
