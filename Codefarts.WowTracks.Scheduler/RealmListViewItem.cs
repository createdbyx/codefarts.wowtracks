﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.ComponentModel;
    using System.Globalization;

    using Codefarts.WowTracks.DataModels;

    using ListViewItem = System.Windows.Forms.ListViewItem;

    /// <summary>
    /// The realm list view item.
    /// </summary>
    public class RealmListViewItem : ListViewItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RealmListViewItem"/> class.
        /// </summary>
        public RealmListViewItem()
        {
            foreach (var name in this.propertyNames)
            {
                this.SubItems.Add(new ListViewSubItem(this, name) { Name = name });
            }
        }

        /// <summary>
        /// Gets or sets the realm.
        /// </summary>
        public RealmTrackingModel Realm
        {
            get
            {
                return this.realm;
            }

            set
            {
                if (this.realm != null)
                {
                    this.realm.PropertyChanged -= this.realm_PropertyChanged;
                }

                this.realm = value;

                if (this.realm != null)
                {
                    this.realm.PropertyChanged += this.realm_PropertyChanged;
                }

                this.realm_PropertyChanged(this, new PropertyChangedEventArgs("Id"));

                foreach (var name in this.propertyNames)
                {
                    this.realm_PropertyChanged(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        /// <summary>
        /// The realm_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void realm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var doSet = new Action<string>(value => this.SubItems[e.PropertyName].Text = value);

            switch (e.PropertyName)
            {
                case "Id":
                    this.Text = this.realm.Id.ToString(CultureInfo.InvariantCulture);
                    break;

                case "Region":
                    doSet(this.realm.Region.ToString(CultureInfo.InvariantCulture));
                    break;

                case "Name":
                    doSet(this.realm.Name.ToString(CultureInfo.InvariantCulture));
                    break;

                case "StartedTracking":
                    doSet(this.realm.StartedTracking.ToString());
                    break;

                case "LastUpdate":
                    doSet(this.realm.LastUpdate.ToString());
                    break;

                case "TrackingInterval":
                    doSet(TimeSpan.FromTicks(this.realm.TrackingInterval).ToString("g"));
                    break;

                case "TrackAuctionHouse":
                    doSet(this.realm.TrackAuctionHouse.ToString(CultureInfo.InvariantCulture));
                    break;

                case "StartedTrackingAuctionHouse":
                    doSet(this.realm.StartedTrackingAuctionHouse.ToString());
                    break;

                case "LastAuctionHouseUpdate":
                    doSet(this.realm.LastAuctionHouseUpdate.ToString());
                    break;

                case "AuctionHouseTrackingInterval":
                    doSet(TimeSpan.FromTicks(this.realm.AuctionHouseTrackingInterval).ToString("g"));
                    break;
            }
        }

        /// <summary>
        /// The realm.
        /// </summary>
        private RealmTrackingModel realm;

        /// <summary>
        /// The property names.
        /// </summary>
        private readonly string[] propertyNames = new[]
                    {
                         "Region", "Name", "StartedTracking", "LastUpdate", "TrackingInterval", "TrackAuctionHouse", 
                        "StartedTrackingAuctionHouse", "LastAuctionHouseUpdate", "AuctionHouseTrackingInterval", 
                    };
    }
}