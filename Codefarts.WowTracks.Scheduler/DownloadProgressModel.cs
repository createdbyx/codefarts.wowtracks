﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.Scheduler.Annotations;

    public class DownloadProgressModel : INotifyPropertyChanged
    {
        private float progress;
        private int bytesPerSecond;

        public string RealmName { get; set; }

        public float Progress
        {
            get
            {
                return this.progress;
            }

            set
            {
                //if (Math.Abs(this.progress - value) < float.Epsilon)
                //{
                //    return;
                //}

                this.progress = value;
                this.OnPropertyChanged("Progress");
            }
        }

        public int BytesPerSecond
        {
            get
            {
                return this.bytesPerSecond;
            }

            set
            {
                //if (this.bytesPerSecond == value)
                //{
                //    return;
                //}

                this.bytesPerSecond = value;
                this.OnPropertyChanged("BytesPerSecond");
            }
        }

        public long TaskId { get; set; }

        public override string ToString()
        {
            var speed = "bytes";
            var value = (float)this.bytesPerSecond;
            if (value >= 1024)
            {
                value /= 1024;
                speed = "Kbs";
            }

            if (value >= 1024)
            {
                value /= 1024;
                speed = "Mbs";
            }

            if (value >= 1024)
            {
                value /= 1024;
                speed = "Gbs";
            }

            return string.Format("{0}: {1}% {2}{3} {4}", this.TaskId, Math.Round(this.Progress, 2), Math.Round(value, 2), speed, this.RealmName);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}