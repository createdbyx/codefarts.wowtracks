﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Policy;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.IdManager;
    using Codefarts.Logging;
    using Codefarts.WowTracks.CommandLibrary;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.SchedulerApi;

    using Newtonsoft.Json;

    /// <summary>
    /// Provides a background downloader for downloading data.
    /// </summary>
    public class BackgroundDownloader
    {
        public IList<RealmTrackingModel> Realms
        {
            get
            {
                return this.realms;
            }
            set
            {
                this.realms = value;
            }
        }

        private CancellationTokenSource backgroundCancel;

        public string DataFolder { get; set; }
        private bool isDownloading;

        private int maxProcessingTasks = 4;

        private IdManager taskIdManager = new IdManager();

        private int minimumWaitTimeInMinutes = 10;

        // default 4k buffer
        private int bufferSize = 8192;

        private IList<RealmTrackingModel> realms;

        private List<ItemIdRegionModel> itemIds = new List<ItemIdRegionModel>();

        private readonly object itemIdLock = new object();

        public event EventHandler<ItemDownloadedEventArgs> ItemSaved;

        protected virtual void OnItemSaved(ItemDownloadedEventArgs e)
        {
            var handler = this.ItemSaved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>Gets or sets the maximum concurrent processing tasks.</summary>
        /// <value>The maximum processing tasks.</value>
        /// <exception cref="System.ArgumentOutOfRangeException">If value is less then 1.</exception>
        public int MaxProcessingTasks
        {
            get
            {
                return this.maxProcessingTasks;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                this.maxProcessingTasks = value;
            }
        }

        /// <summary>Gets or sets the minimum wait time in minutes between running schedules.</summary>
        /// <value>The minimum wait time in minutes.</value>
        public int MinimumWaitTimeInMinutes
        {
            get
            {
                return this.minimumWaitTimeInMinutes;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                this.minimumWaitTimeInMinutes = value;
            }
        }

        public event EventHandler<AuctionHouseFilesDownloadedEventArgs> AuctionHouseFilesSaved;

        protected virtual void OnAuctionHouseFilesSaved(AuctionHouseFilesDownloadedEventArgs e)
        {
            var handler = this.AuctionHouseFilesSaved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DownloadProgressEventArgs> DownloadProgress;

        protected virtual void OnDownloadProgress(DownloadProgressEventArgs e)
        {
            var handler = this.DownloadProgress;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>Occurs when [stopped].</summary>
        public event EventHandler Stopped;

        public event EventHandler<RealmTrackingModelEventArgs> LastAuctionHouseUpdated;

        protected virtual void OnLastAuctionHouseUpdated(RealmTrackingModel e)
        {
            var handler = this.LastAuctionHouseUpdated;
            if (handler != null)
            {
                handler(this, new RealmTrackingModelEventArgs(e));
            }
        }

        protected virtual void OnStopped()
        {
            var handler = this.Stopped;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>Cancels this instance.</summary>
        public void Cancel()
        {
            var cancel = this.backgroundCancel;
            if (cancel != null)
            {
                cancel.Cancel();
            }
        }

        /// <summary>Starts the download process.</summary>
        public void Run()
        {
            if (string.IsNullOrWhiteSpace(this.DataFolder))
            {
                throw new NullReferenceException("DataFolder has not been set!");
            }

            if (this.DataFolder.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                throw new IOException("DataFolder contains invalid characters!");
            }

            if (!Directory.Exists(this.DataFolder))
            {
                throw new DirectoryNotFoundException("DataFolder does not exist!");
            }

            if (this.isDownloading)
            {
                return;
            }

            this.isDownloading = true;
            this.backgroundCancel = new CancellationTokenSource();
            Task.Factory.StartNew(
                () =>
                {
                    // run until canceled
                    while (!this.backgroundCancel.IsCancellationRequested)
                    {
                        this.StartDownloadingAuctionHouseData();

                        // sleep for a time to prevent 100% cpu usage
                        // and restrict to minimum of 10 minute intervals
                        var startedSleeping = DateTime.Now;

                        this.LogInfo(string.Format("Next update starts at {0}", new DateTime(startedSleeping.Ticks + TimeSpan.FromMinutes(this.minimumWaitTimeInMinutes).Ticks)));
                        while (!this.backgroundCancel.IsCancellationRequested && DateTime.Now < startedSleeping + TimeSpan.FromMinutes(this.minimumWaitTimeInMinutes))
                        {
                            this.StartDownloadingItemIdInformation();
                            Thread.Sleep(50);
                        }
                    }

                    this.isDownloading = false;
                    this.backgroundCancel.Dispose();
                    this.backgroundCancel = null;
                    this.OnStopped();
                },
                this.backgroundCancel.Token);
        }

        private void StartDownloadingItemIdInformation()
        {
            if (this.itemIds.Count == 0)
            {
                //    this.LogInfo("There are no item id's to download.");
                return;
            }

            this.LogInfo(string.Format("{0} Item Id's still left in the queue.", this.itemIds.Count));

            // process each realm in the queue
            var started = DateTime.Now;
            while (this.itemIds.Count > 0 && !this.backgroundCancel.IsCancellationRequested && DateTime.Now < started + TimeSpan.FromSeconds(45))
            {
                // attempt to spawn up to MaxProcessingTasks tasks at a time
                var count = this.itemIds.Count < this.MaxProcessingTasks ? this.itemIds.Count : this.MaxProcessingTasks;
                var tasks = new Task[count];
                for (var i = 0; i < count; i++)
                {
                    // start a task
                    tasks[i] = Task.Factory.StartNew(this.DownloadItemIdData, this.backgroundCancel.Token);
                }

                //    this.LogInfo(string.Format("Allocated {0} item id processing tasks. Waiting for them to complete.", count));

                // wait for all tasks to complete
                Task.WaitAll(tasks);

                //  this.LogInfo("All item id processing tasks completed.");
                this.LogInfo(string.Format("{0} Item Id's still left in the queue.", this.itemIds.Count));
            }
        }

        private void DownloadItemIdData()
        {
            ItemIdRegionModel model;
            lock (this.itemIdLock)
            {
                if (this.itemIds.Count == 0)
                {
                    return;
                }

                model = this.itemIds[0];
                this.itemIds.RemoveAt(0);
            }

            long taskId = -1;
            var fileName = string.Empty;
            try
            {
                var urlPath = string.Format("/api/wow/item/{0}", model.ItemId);
                var url = new Uri(
                    new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + model.Region + ".battle.net", UriKind.RelativeOrAbsolute),
                    urlPath);
                fileName = Path.Combine(this.DataFolder, model.Region, "ItemIds", model.ItemId + ".json");

                // check for existing id file this may occur if multiple ah data files are downloaded & processed
                // concurrently independently of one another
                if (File.Exists(fileName))
                {
                    this.LogInfo(string.Format("Item id file {0} already exists.", model.ItemId));
                    return;
                }

                taskId = this.taskIdManager.NewId();
                //  this.LogInfo(string.Format("{0}: Starting Item id file {1} download...", taskId, model.ItemId));
                var result = this.DownloadFile(
                    fileName,
                    url,
                    false,
                    (DateTime.Now - TimeSpan.FromDays(365)).ToUniversalTime(),
                    taskId,
                    string.Empty);

                // check result                                          
                if (result.Successful)
                {
                    //    this.LogInfo(string.Format("{0}: Item id file {1} download complete.", taskId, model.ItemId));
                    this.OnItemSaved(new ItemDownloadedEventArgs(model.ItemId, model.Region, fileName));
                }
                else
                {
                    this.LogError(
                        string.Format(
                            "{0}: Item id file {1} download unsuccessful. {2}",
                            taskId,
                            model.ItemId,
                            result.Error != null ? result.Error.Message : string.Empty));
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                        this.LogError(string.Format("{0}: Item id {1} file deleted successfully! \"{2}\"", taskId, model.ItemId, fileName));
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError(string.Format("{0}: Unhandled exception occurred! {1} ItemId: {2}", taskId, ex.Message, model.ItemId));
                try
                {
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                        this.LogError(string.Format("{0}: Item id {1} file deleted successfully! \"{2}\"", taskId, model.ItemId, fileName));
                    }
                }
                catch (Exception exx)
                {
                    this.LogError(
                        string.Format("{0}: May have been a problem deleting Item id file! {1} ItemId: {2}", taskId, exx.Message, model.ItemId));
                }
            }
        }

        private void StartDownloadingAuctionHouseData()
        {
            // grab a copy of the realm tracking list to allow the UI to continue to work with realm schedules
            Queue<RealmTrackingModel> realmQueue;
            lock (this.Realms)
            {
                realmQueue = new Queue<RealmTrackingModel>(this.Realms);
            }

            this.LogInfo(string.Format("{0} Realms were queued.", realmQueue.Count));

            // process each realm in the queue
            while (realmQueue.Count > 0 && !this.backgroundCancel.IsCancellationRequested)
            {
                try
                {
                    this.SpawnProcessingTasks(realmQueue, this.DataFolder);
                }
                catch (Exception ex)
                {
                    this.LogError("Unhandled exception occurred! " + ex.Message);
                }
            }
        }

        private void SpawnProcessingTasks(Queue<RealmTrackingModel> realms, string dataFolderPath)
        {
            // attempt to spawn up to MaxProcessingTasks tasks at a time
            var count = realms.Count < this.MaxProcessingTasks ? realms.Count : this.MaxProcessingTasks;
            var tasks = new Task[count];
            for (var i = 0; i < count; i++)
            {
                // start a task
                tasks[i] = Task.Factory.StartNew(
                    x => this.AuctionHouseDownloadTask(dataFolderPath, (RealmTrackingModel)x),
                    realms.Dequeue(),
                    this.backgroundCancel.Token);
            }

            this.LogInfo(string.Format("Allocated {0} processing tasks. Waiting for them to complete.", count));

            // wait for all tasks to complete
            Task.WaitAll(tasks);

            this.LogInfo("All processing tasks completed.");
        }

        private void AuctionHouseDownloadTask(string dataFolderPath, RealmTrackingModel realm)
        {
            // create a unique task id for this method call
            var taskID = this.taskIdManager.NewId();
            try
            {
                this.LogInfo(string.Format("{0}: Processing task for {1}-{2} has started.", taskID, realm.Name, realm.Region));

                // check it it's time for the realms auction house to be downloaded
                if (realm.TrackAuctionHouse && DateTime.Now > realm.LastAuctionHouseUpdate + TimeSpan.FromTicks(realm.AuctionHouseTrackingInterval))
                {
                    this.LogInfo(string.Format("{0}: An update is needed. Downloading AH info file.", taskID));

                    // get auction house url
                    var url = realm.BuildAuctionHouseUrl();

                    // download AH info file                                                                       
                    var filePath = Path.Combine(dataFolderPath, realm.Region, realm.Name, "AuctionHouseInfo.json");

                    // var ifModifiedSince = File.Exists(filePath) ? File.GetLastWriteTime(filePath).ToUniversalTime() : DateTime.Now.Subtract(TimeSpan.FromDays(30));
                    var ifModifiedSince = realm.LastAuctionHouseUpdate.ToUniversalTime();

                    // attempt to download the info file
                    var result = this.DownloadFile(filePath, url, false, ifModifiedSince, taskID, realm.Name);

                    // check result                                          
                    if (result.Successful)
                    {
                        this.LogInfo(string.Format("{0}: File download complete.", taskID));

                        // process the file data
                        if (!this.ProcessAuctionHouseInfoFile(dataFolderPath, realm, filePath, taskID))
                        {
                            this.LogError(string.Format("{0}: Task unsuccessful. Processing auction house files failed.", taskID));
                        }
                    }
                    else
                    {
                        this.LogError(string.Format("{0}: File download unsuccessful. {1}", taskID, result.Error != null ? result.Error.Message : string.Empty));
                    }
                }
                else
                {
                    this.LogInfo(string.Format("{0}: No update required.", taskID));
                }

                this.LogInfo(string.Format("{0}: Processing task completed.", taskID));
            }
            catch (Exception ex)
            {
                this.LogError(string.Format("{0}: Unhandled exception occurred! " + ex.Message, taskID));
            }
        }

        private CommandResult<string> DownloadFile(string filePath, Uri url, bool allowCancel, DateTime IfModifiedSince, long taskId, string realmName)
        {
            var path = Path.GetDirectoryName(filePath);
            if (!string.IsNullOrWhiteSpace(path))
            {
                Directory.CreateDirectory(path);
            }

            CommandResult<string> result;
            using (var stream = new FileStream(filePath, File.Exists(filePath) ? FileMode.Open : FileMode.Create, FileAccess.Write, FileShare.None))
            //   using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                var parameters = new Dictionary<string, object>
                                     {
                                         { "Url", url }, 
                                         { "OutputStream", stream }, 
                                         { "IfModifiedSince", IfModifiedSince },
                                         { "BufferSize", this.BufferSize }
                                     };

                if (allowCancel)
                {
                    var progressCallback = new Func<float, int, bool>(
                        (progress, bytesPerSecond) =>
                        {
                            this.OnDownloadProgress(new DownloadProgressEventArgs(taskId, progress, realmName, bytesPerSecond));
                            return !this.backgroundCancel.Token.IsCancellationRequested;
                        });

                    parameters.Add("ProgressCallback", progressCallback);
                }

                // download auction house info file
                result = DownloadFileCommand.DownloadFile(parameters);

                stream.Close();
            }

            return result;
        }

        public int BufferSize
        {
            get
            {
                return this.bufferSize;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value", "Buffer size value must be greater then 0.");
                }

                this.bufferSize = value;
            }
        }

        private bool ProcessAuctionHouseInfoFile(string dataFolderPath, RealmTrackingModel realm, string filePath, long taskID)
        {
            AuctionData info;
            try
            {
                // read json info and compare last modified to last auction house file downloaded
                info = JsonConvert.DeserializeObject<AuctionData>(File.ReadAllText(filePath));
            }
            catch (Exception ex)
            {
                this.LogError(string.Format("{0}: Unknown error occurred deserializing json! " + ex.Message, taskID));
                if (File.Exists(filePath))
                {
                    this.LogError(string.Format("{0}: Deleting file " + filePath, taskID));
                    File.Delete(filePath);
                }

                return false;
            }

            // get path to auction house folder and ensure that it exists
            var path = Path.Combine(dataFolderPath, realm.Region, realm.Name, "AuctionHouse");
            Directory.CreateDirectory(path);

            // ge last file data for the ah data if any
            var lastFileDate = this.GetLastFileDate(path);

            // get a list of urls to auction house files
            var unixStartDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            // determine what files to download
            var filesToDownload = (from dataFile in info.files
                                   where lastFileDate < unixStartDate.AddMilliseconds(dataFile.lastModified)
                                   select dataFile.url).ToList();

            this.LogInfo(string.Format("{0}: Processing task has {1} auction house files to download.", taskID, filesToDownload.Count));

            var generateFileName = new Func<string>(
                () =>
                {
                    var dateTime = DateTime.Now;
                    return Path.Combine(
                          dataFolderPath,
                          realm.Region,
                          realm.Name,
                          "AuctionHouse",
                          dateTime.Year.ToString(),
                          dateTime.Month.ToString(),
                          dateTime.ToString(@"M-d-yyyy hh-mm-ss tt") + ".json");
                });

            // download new auction house files if necessary
            var savedFiles = new List<string>();
            for (var index = 0; index < filesToDownload.Count; index++)
            {
                // get file url and build path for filename
                var fileUrl = new Uri(filesToDownload[index], UriKind.RelativeOrAbsolute);
                var outputPath = string.Empty;

                // while loop prevents super fast file downloads from over writing the last download.
                // Assume the json contained 2 or more ah files to download. If you had a fast enough 
                // internet connection you could theoretically download the first file in under one second
                // then proceed to the next file within that same second and start downloading the next file
                // but the next file will have the same time stamp filename as the previous file because
                // file names are only time stamped with a one second resolution. The while loop will halt execution
                // until the next second ticks by
                while (File.Exists(outputPath = generateFileName()))
                {
                    // sleep half second to prevent massive amount of file exist calls.
                    Thread.Sleep(500);
                }

                this.LogInfo(string.Format("{0}: Processing task started {1}/{2} downloads.", taskID, index + 1, filesToDownload.Count));

                // attempt to download auction house file
                savedFiles.Add(outputPath);
                var downloadResult = this.DownloadFile(outputPath, fileUrl, true, lastFileDate, taskID, realm.Name);

                // check if successful
                if (downloadResult.Successful)
                {
                    this.LogInfo(string.Format("{0}: File download {1}/{2} succeeded. {3}", taskID, index + 1, filesToDownload.Count, outputPath));
                }
                else
                {
                    this.LogError(string.Format("{0}: Processing task failed. {1}", taskID, downloadResult.Error != null ? downloadResult.Error.Message : string.Empty));
                    this.LogError(string.Format("{0}: Removing {1} previously saved files.", taskID, savedFiles.Count));

                    // cleanup saved files
                    for (var i = 0; i < savedFiles.Count; i++)
                    {
                        var file = savedFiles[i];
                        File.Delete(file);
                        this.LogError(string.Format("{0}: Removed {1}/{2} {3}", taskID, i + 1, savedFiles.Count, file));
                    }

                    this.LogError(string.Format("{0}: Removal of previous saved files complete.", taskID));

                    return false;
                }
            }


            // set last success full AH update time, update the repository, and notify changes via event
            realm.LastAuctionHouseUpdate = DateTime.Now;
            this.OnLastAuctionHouseUpdated(realm);
            this.OnAuctionHouseFilesSaved(new AuctionHouseFilesDownloadedEventArgs(savedFiles.ToArray(), realm.Name, realm.Region, AuctionHouseFile.DataFile));

            return true;
        }

        private DateTime GetLastFileDate(string path)
        {
            // get date folders
            var dateFolders = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly);
            IEnumerable<string> files = new string[0];
            if (dateFolders.Length != 0)
            {
                var lastDateFolder = dateFolders.OrderByDescending(x => x).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(lastDateFolder))
                {
                    // get month folders
                    var monthFolders = Directory.GetDirectories(lastDateFolder, "*.*", SearchOption.TopDirectoryOnly);
                    var lastMonthFolder = monthFolders.OrderByDescending(x => x).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(lastMonthFolder))
                    {
                        // get files if any
                        files = Directory.GetFiles(lastMonthFolder, "*.json", SearchOption.TopDirectoryOnly);
                        files = files.Union(Directory.GetFiles(lastMonthFolder, "*.zip", SearchOption.TopDirectoryOnly));
                        files = files.Distinct();
                    }
                }
            }

            // get last recorded auction file based on file creation date
            var last =
                files.OrderBy(x => DateTime.ParseExact(Path.GetFileNameWithoutExtension(x), @"M-d-yyyy hh-mm-ss tt", null).ToUniversalTime().Ticks)
                    .LastOrDefault();

            // get last saved file date
            var lastFileDate = string.IsNullOrWhiteSpace(last)
                                   ? DateTime.MinValue
                                   : DateTime.ParseExact(Path.GetFileNameWithoutExtension(last), @"M-d-yyyy hh-mm-ss tt", null).ToUniversalTime();
            return lastFileDate;
        }

        private void LogError(string message)
        {
            Logging.Log(LogEntryType.Error, message, "Downloader");
        }

        private void LogInfo(string message)
        {
            Logging.Log(LogEntryType.Information, message, "Downloader");
        }

        private void LogEvent(string message)
        {
            Logging.Log(LogEntryType.Event, message, "Downloader");
        }

        public bool IsDownloading
        {
            get
            {
                return this.isDownloading;
            }
        }

        public void EnqueueItemId(ItemIdRegionModel id)
        {
            lock (this.itemIdLock)
            {
                if (this.itemIds.All(x => x.ItemId != id.ItemId))
                {
                    this.itemIds.Add(id);
                }
            }
        }
    }
}
