﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TimeIntervalControl
{
    /* This code was written by ycouriel@gmail.com
     * You can use it in any way you want
     * 
     * For more information about the attributes:
     * http://msdn.microsoft.com/en-us/library/system.componentmodel.defaulteventattribute.aspx
     * 
     */
    [DefaultEvent("IntervalChanged")]
    [DefaultProperty("CurrentTimeSpan")]
    public partial class TimeInterval : UserControl
    {
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        public event EventHandler IntervalChanged;

        private bool suspendChangeNotifications = false;

        public TimeInterval()
        {
            InitializeComponent();
        }

        public int Days
        {
            get { return int.Parse(txtDays.Text); }
            set { txtDays.Text = value.ToString(); }
        }

        public int Hours
        {
            get { return int.Parse(txtHours.Text); }
            set { txtHours.Text = value.ToString(); }
        }

        public int Minutes
        {
            get { return int.Parse(txtMinutes.Text); }
            set { txtMinutes.Text = value.ToString(); }
        }

        public int Seconds
        {
            get { return int.Parse(txtSeconds.Text); }
            set { txtSeconds.Text = value.ToString(); }
        }

        public long CurrentTicks
        {
            get { return CurrentTimeSpan.Ticks; }
            set { CurrentTimeSpan = new TimeSpan(value); }
        }

        public TimeSpan CurrentTimeSpan
        {
            get { return new TimeSpan(Days, Hours, Minutes, Seconds); }
            set
            {
                suspendChangeNotifications = true;
                Days = value.Days;
                Hours = value.Hours;
                Minutes = value.Minutes;
                Seconds = value.Seconds;
                // this way the user will be notified only once
                suspendChangeNotifications = false;
                IntervalChangedNotifyUser();
            }
        }

        protected virtual void IntervalChangedNotifyUser()
        {
            var onIntervalChanged = this.IntervalChanged;
            if (onIntervalChanged != null && !suspendChangeNotifications)
            {
                onIntervalChanged(this, null);
            }
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            var textBox = (sender as TextBox);
            if (!IsNumeric(textBox.Text)) textBox.Text = "0";
            IntervalChangedNotifyUser();
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allow only numbers
            // note that this solution does not protect from copy-paste of a non-number
            // that's why there is another check in TextChanged
            if (!IsNumeric(e.KeyChar.ToString())) e.Handled = true;
        }

        private bool IsNumeric(string text)
        {
            // limit the number to 7 digits
            return Regex.IsMatch(text, @"^\d{1,7}$");
        }

        private void TimeInterval_Load(object sender, EventArgs e)
        {

        }
    }
}
