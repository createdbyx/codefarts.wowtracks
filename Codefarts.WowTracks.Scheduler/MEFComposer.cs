﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.Repository;
    using Codefarts.Logging;
    using Codefarts.WowTracks.SchedulerApi;

    public class MEFComposer
    {
        [ImportMany(typeof(IRepository))]
        public List<IRepository> Repositories;

        [ImportMany(typeof(ILogging))]
        public List<ILogging> LoggingRepositories;

        [ImportMany(typeof(IPlugin))]
        public List<IPlugin> Plugins;
    }
}