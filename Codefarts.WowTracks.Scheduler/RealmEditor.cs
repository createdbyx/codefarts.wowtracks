﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System.Windows.Forms;

    using Codefarts.WowTracks.DataModels;

    public partial class RealmEditor : Form
    {
        private RealmTrackingModel realm;

        public RealmEditor()
        {
            InitializeComponent();
        }

        public bool TrackAuctionHouse
        {
            get
            {
                return this.chkTrackAH.Checked;
            }
        }

        public long Interval
        {
            get
            {
                return this.tiInterval.CurrentTicks;
            }
        }

        public long AuctionHouseInterval
        {
            get
            {
                return this.tiAHInterval.CurrentTicks;
            }
        }

        public RealmTrackingModel Realm
        {
            get
            {
                return this.realm;
            }
            set
            {
                this.realm = value;

                if (value != null)
                {
                    this.tiInterval.CurrentTicks = value.TrackingInterval;
                    this.tiAHInterval.CurrentTicks = value.AuctionHouseTrackingInterval;
                    this.chkTrackAH.Checked = value.TrackAuctionHouse;
                }
            }
        }
    }
}
