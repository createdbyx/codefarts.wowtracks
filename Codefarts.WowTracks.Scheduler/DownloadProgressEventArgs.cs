﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;

    /// <summary>
    /// Provides event args for file download progress.
    /// </summary>
    public class DownloadProgressEventArgs : EventArgs
    {
        /// <summary>Initializes a new instance of the <see cref="DownloadProgressEventArgs"/> class.</summary>
        public DownloadProgressEventArgs()
        {
        }

        /// <summary>Initializes a new instance of the <see cref="DownloadProgressEventArgs"/> class.</summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="progress">The progress.</param>
        /// <param name="name">The realm name.</param>
        /// <param name="bytesPerSecond"></param>
        public DownloadProgressEventArgs(long taskId, float progress, string name, int bytesPerSecond)
        {
            this.TaskId = taskId;
            this.Progress = progress;
            this.RealmName = name;
            this.BytesPerSecond = bytesPerSecond;
        }

        /// <summary>Gets or sets the task identifier.</summary>
        /// <value>The task identifier.</value>
        public long TaskId { get; set; }

        /// <summary>Gets or sets the progress.</summary>
        /// <value>The progress from 0 to 1.</value>
        public float Progress { get; set; }

        /// <summary>Gets or sets the bytes per second.</summary>
        /// <value>The bytes per second value.</value>
        public int BytesPerSecond { get; set; }
        
        /// <summary>Gets or sets the name of the realm.</summary>
        /// <value>The name of the realm.</value>
        public string RealmName { get; set; }

    }
}