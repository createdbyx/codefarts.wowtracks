﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.Data.Entity;

    using System.Windows;

    using FirstFloor.ModernUI.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DbConfiguration.SetConfiguration(new TrackerDbConfiguration());
            Database.SetInitializer(new CreateDatabaseIfNotExists<TrackerContext>());
        }      
    }
}
