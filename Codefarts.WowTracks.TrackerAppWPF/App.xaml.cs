﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.IO;

    using Codefarts.Settings;
    using Codefarts.Settings.Xml;
    using System;
    using System.Windows;


    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // setup settings
            var settingsFile = Path.Combine(Environment.CurrentDirectory, "settings.xml");
            SettingsManager.Instance.Values = new XmlDocumentLinqValues(settingsFile, true);

            base.OnStartup(e);
        }
    }
}
