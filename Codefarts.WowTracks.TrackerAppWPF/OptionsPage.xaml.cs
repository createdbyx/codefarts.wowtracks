﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.Data.Entity;
    using System.Windows.Forms;

    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Codefarts.Settings;
    using Codefarts.WowTracks.TrackerAppWPF.Models;

    using FirstFloor.ModernUI.Windows;
    using FirstFloor.ModernUI.Windows.Navigation;

    /// <summary>
    /// Interaction logic for OptionsPage.xaml
    /// </summary>
    public partial class OptionsPage : Page, IContent
    {
        private int _noOfErrorsOnScreen = 0;

        private OptionsModel optionsModel;

        public OptionsPage()
        {
            InitializeComponent();
        }

        private void btnSelectFolder_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.optionsModel.DataFolder = dialog.SelectedPath;
                }
            }
        }     

        private void Validation_Error(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
            {
                _noOfErrorsOnScreen++;
            }
            else
            {
                _noOfErrorsOnScreen--;
            }
        }

        private void SaveSettings_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _noOfErrorsOnScreen == 0;
            e.Handled = true;
        }

        private void SaveSettings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SettingsManager.Instance.SetValue(SettingConstants.DataFolder, this.optionsModel.DataFolder.Trim());
            DbConfiguration.SetConfiguration(new TrackerDbConfiguration());
            e.Handled = true;
        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(NavigationEventArgs e)
        {
            this.optionsModel = new OptionsModel() { DataFolder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty) };
            this.DataContext = this.optionsModel;
        }

        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
    }
}
