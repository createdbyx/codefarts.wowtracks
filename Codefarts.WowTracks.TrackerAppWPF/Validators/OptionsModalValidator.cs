﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF.Validators
{
    using System.IO;

    using Codefarts.WowTracks.TrackerAppWPF.Models;

    using FluentValidation;
                  
    public class OptionsModalValidator : AbstractValidator<OptionsModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsModalValidator"/> class.
        /// </summary>
        public OptionsModalValidator()
        {
            RuleFor<string>(x => x.DataFolder).NotEmpty().WithMessage("{PropertyName} can not be empty.");
            RuleFor<string>(x => x.DataFolder).Must(Directory.Exists).WithMessage("{PropertyName} does not exist.");
        }
    }
}
