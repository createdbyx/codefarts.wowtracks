﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF.Models
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.TrackerAppWPF.Annotations;
    using Codefarts.WowTracks.TrackerAppWPF.Validators;

    public class OptionsModel : IDataErrorInfo, INotifyDataErrorInfo   ,INotifyPropertyChanged
    {
        public string DataFolder
        {
            get
            {
                return this.dataFolder;
            }
            set
            {
                this.dataFolder = value;
                this.OnPropertyChanged("DataFolder");
            }
        }

        private string lastError;

        private string dataFolder;

        public string this[string columnName]
        {
            get
            {
                var validator = new OptionsModalValidator();
                var results = validator.Validate(this);
                return !results.IsValid ? results.Errors.First().ErrorMessage : string.Empty;
            }
        }

        public string Error
        {
            get
            {
                var validator = new OptionsModalValidator();
                var results = validator.Validate(this);
                return !results.IsValid ? results.Errors.First().ErrorMessage : string.Empty;
            }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            var validator = new OptionsModalValidator();
            var results = validator.Validate(this);
            if (!results.IsValid && this.ErrorsChanged != null)
            {
                this.ErrorsChanged(this, new DataErrorsChangedEventArgs(results.Errors.First().PropertyName));
            }
            return results.Errors;
        }

        public bool HasErrors
        {
            get
            {
                var validator = new OptionsModalValidator();
                var results = validator.Validate(this);
                return results.IsValid;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
