﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF.Models
{
    using System.Globalization;

    using WOWSharp.Community.Wow;

    public class Realm
    {
        public Realm()
        {
        }

        public Realm(WOWSharp.Community.Wow.Realm x)
        {
            this.BattleGroupName = x.BattleGroupName;
            this.Locale = x.Locale;
            this.Name = x.Name;
            this.Slug = x.Slug;
            this.TimeZone = x.TimeZone;
            this.TolBarad = x.TolBarad;
            this.WinterGrasp = x.WinterGrasp;
        }

        public bool IsSelected { get; set; }

        public string BattleGroupName { get; internal set; }

        public string Locale { get; internal set; }

        public string Name { get; internal set; }

        public RealmPopulation Population { get; internal set; }

        public bool Queue { get; internal set; }

        public string Slug { get; internal set; }

        public bool Status { get; internal set; }

        public string TimeZone { get; internal set; }

        public RealmTypes RealmType { get; internal set; }

        public PvpZone TolBarad { get; internal set; }

        public PvpZone WinterGrasp { get; internal set; }

        public bool IsPvp
        {
            get
            {
                return (this.RealmType & RealmTypes.Pvp) != 0;
            }
        }

        public bool IsRP
        {
            get
            {
                return (this.RealmType & RealmTypes.RP) != 0;
            }
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "Realm = {0}, Type= {1}, Status = {2}", this.Name,
                this.RealmType, this.Status);
        }
    }
}