﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF.Models
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.TrackerAppWPF.Annotations;

    using WOWSharp.Community;

    public class SelectedRegion : INotifyPropertyChanged
    {
        private string name;

        private string host;

        private ObservableCollection<string> supportedLocales;

        private string defaultLocale;

        private bool isSelected;

        public SelectedRegion(Region item)
        {
            this.name = item.Name;
            this.host = item.Host;
            this.supportedLocales = new ObservableCollection<string>(item.SupportedLocales);
            this.defaultLocale = item.DefaultLocale;
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        public string DefaultLocale
        {
            get
            {
                return this.defaultLocale;
            }
            set
            {
                this.defaultLocale = value;
                this.OnPropertyChanged("DefaultLocale");
            }
        }

        public ObservableCollection<string> SupportedLocales
        {
            get
            {
                return this.supportedLocales;
            }
            set
            {
                this.supportedLocales = value;
                this.OnPropertyChanged("SupportedLocales");
            }
        }

        public string Host
        {
            get
            {
                return this.host;
            }
            set
            {
                this.host = value;
                this.OnPropertyChanged("Host");
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
