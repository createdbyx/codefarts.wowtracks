﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF.Models
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.TrackerAppWPF.Annotations;

    using WOWSharp.Community;

    public class AddRealmModel : INotifyPropertyChanged
    {
        private string filter;

        // private Region selectedRegion;

        private Realm[] realms;

        private string[] supportedLocales;

        private SelectedRegion[] regions;

        public SelectedRegion[] Regions
        {
            get
            {
                return this.regions;
            }
            set
            {
                this.regions = value;
                this.OnPropertyChanged("Regions");
            }
        }

        public string[] SupportedLocales
        {
            get
            {
                return this.supportedLocales;
            }
            set
            {
                this.supportedLocales = value;
                this.OnPropertyChanged("SupportedLocales");
            }
        }

        public Realm[] Realms
        {
            get
            {
                return this.realms;
            }
            set
            {
                this.realms = value;
                this.OnPropertyChanged("Realms");
                this.OnPropertyChanged("FilteredRealms");
            }
        }

        public SelectedRegion SelectedRegion
        {
            get
            {
                return this.regions.FirstOrDefault(x => x.IsSelected);
            }
        }

        public string Filter
        {
            get
            {
                return this.filter;
            }
            set
            {
                if (this.filter == value)
                {
                    return;
                }

                this.filter = value;
                this.OnPropertyChanged("Filter");
                this.OnPropertyChanged("FilteredRealms");
            }
        }

        public Realm[] FilteredRealms
        {
            get
            {
                return this.Realms == null ? null : this.Realms.Where(x => 
                    string.IsNullOrWhiteSpace(this.Filter) || x.Name.IndexOf(this.Filter, StringComparison.OrdinalIgnoreCase) != -1).ToArray();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}