﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.TrackerAppWPF.Models;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    /// <summary>
    /// Interaction logic for AddRealm.xaml
    /// </summary>
    public partial class AddRealm : Page
    {
        private AddRealmModel addRealmModel;

        public AddRealm()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.addRealmModel = new AddRealmModel() { Regions = Region.AllRegions.Select(x => new SelectedRegion(x)).ToArray() };
            var us = this.addRealmModel.Regions.FirstOrDefault(x => x.Name == Region.US.Name);
            if (us != null)
            {
                us.IsSelected = true;
            }

            this.DataContext = this.addRealmModel;
            this.RadioButton_Checked(this, e);
        }

        private async Task PopulateRealmList(Region selectedItem)
        {
            var client = new WowClient(selectedItem, null, null, null);

            // Get realms
            var results = await client.GetRealmStatusAsync();
            this.addRealmModel.Realms = results.Realms.Select(x => new Models.Realm(x)).ToArray();
            this.LoadingAnimation.Visibility = Visibility.Hidden;
        }

        private async void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.LoadingAnimation.Visibility = Visibility.Visible;
            var selectedRegion = this.addRealmModel.Regions.FirstOrDefault(x => x.IsSelected);
            await this.PopulateRealmList(Region.AllRegions.FirstOrDefault(x => x.Name == selectedRegion.Name));
        }

        private async void btnAddRealms_Click(object sender, RoutedEventArgs e)
        {
            var addList = from item in this.addRealmModel.Realms
                          where item.IsSelected
                          select new RealmTrackingModel()
                                     {
                                         Name = item.Name,
                                         Region = this.addRealmModel.SelectedRegion.Name
                                     };

            using (var db = new TrackerContext())
            {
                addList = from item in addList
                          where !db.TrackingRealms.Any(x => x.Name == item.Name && x.Region == item.Region)
                          select item;

                db.TrackingRealms.AddRange(addList);
                var savedCount = await db.SaveChangesAsync();

                // did we save all the records
                if (addList.Count() != savedCount)
                {
                    // error ?
                }
            }
        }

        private void ClearFilter_OnClick(object sender, RoutedEventArgs e)
        {
            this.addRealmModel.Filter = null;
        }
    }

    //private async Task AddTrackingRealmsToDatabase()
    //{
    //    Task.Factory.StartNew(
    //      async () =>
    //       {

    //       });
    //}
    // }
}
