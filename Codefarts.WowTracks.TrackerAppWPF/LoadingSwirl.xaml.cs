﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LoadingSwirl.xaml
    /// </summary>
    public partial class LoadingSwirl : Grid
    {
        public LoadingSwirl()
        {
            InitializeComponent();
        }        
    }
}
