﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.Data.Entity;

    using Codefarts.WowTracks.DataModels;

    public class TrackerContext : DbContext
    {
        public TrackerContext( )
            : base("Tracker.sdf")
        {
        }

        public DbSet<DiscoveredRealms> DiscoveredRealms { get; set; }
        public DbSet<RealmTrackingModel> TrackingRealms { get; set; }
    }
}