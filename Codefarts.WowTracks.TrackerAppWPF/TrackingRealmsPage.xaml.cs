﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerAppWPF
{
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;

    using Codefarts.Settings;

    /// <summary>
    /// Interaction logic for TrackingRealmsPage.xaml
    /// </summary>
    public partial class TrackingRealmsPage : Page
    {
        public TrackingRealmsPage()
        {
            InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var path = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
            if (string.IsNullOrWhiteSpace(path) || !Directory.Exists(path))
            {
                return;
            }

            Task.Factory.StartNew(
                () =>
                {
                    using (var db = new TrackerContext())
                    {
                        var results = db.TrackingRealms.ToList();
                        this.Dispatcher.InvokeAsync(
                            () =>
                            {
                                this.LoadingAnimation.Visibility = Visibility.Hidden;
                                this.RealmItems.ItemsSource = results;
                            });
                    }
                });
        }

        private void RealmButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RealmPage());  
        }
    }
}
