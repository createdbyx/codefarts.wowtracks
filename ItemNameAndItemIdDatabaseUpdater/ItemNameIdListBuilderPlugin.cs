﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace ItemNameAndItemIdDatabaseUpdater
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.SchedulerApi;

    using Newtonsoft.Json;

    public class ItemNameIdListBuilderPlugin : IPlugin
    {
        private Application app;
        private Dictionary<string, List<NameIdModel>> items = new Dictionary<string, List<NameIdModel>>();

        private bool connected;

        public void Connect(Application app)
        {
            if (this.connected)
            {
                return;
            }

            this.connected = true;
            this.app = app;
            this.app.ItemSaved += this.app_ItemSaved;
        }

        private void app_ItemSaved(object sender, ItemDownloadedEventArgs e)
        {
            var fileName = Path.Combine(this.app.DataFolder, e.Region, "ItemIdNameTable.json");

            // ensure key exists
            if (!this.items.ContainsKey(e.Region))
            {
                this.items.Add(e.Region, new List<NameIdModel>());

                // check if there is an existing file and if so read it and cache it's data
                if (File.Exists(fileName))
                {
                    var dataSet = JsonConvert.DeserializeObject<NameIdModel[]>(File.ReadAllText(fileName));
                    this.items[e.Region].AddRange(dataSet);
                }
            }

            // read the new data file and add it to the list if it does not yet exist
            var newData = JsonConvert.DeserializeObject<ItemInfo>(File.ReadAllText(e.FileName));

            // add item to the list if it has not already been added
            var list = this.items[e.Region];
            if (list.All(x => x.Id != newData.id))
            {
                list.Add(new NameIdModel(newData.id, newData.name));
            }

            // ensure directory exists before saving updated name id table file
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            File.WriteAllText(fileName, JsonConvert.SerializeObject(list.ToArray(), Formatting.Indented));
        }

        public void Disconnect()
        {
            if (!this.connected)
            {
                return;
            }

            this.app.ItemSaved -= this.app_ItemSaved;
            this.app = null;
            this.connected = false;
        }
    }
}