﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace AuctionHouseItemIdDatabaseBuilderPlugin
{
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;

    using Codefarts.Logging;
    using Codefarts.WowTracks.SchedulerApi;

    using Newtonsoft.Json;

    using WOWSharp.Community.Wow;

    [Export(typeof(IPlugin))]
    public class ItemIdDatabaseGenerator : IPlugin
    {
        private Application app;

        public void Connect(Application app)
        {
            this.app = app;
            this.app.AuctionHouseFilesSaved += this.app_AuctionHouseFilesSaved;
        }

        private void LogError(string message)
        {
            Logging.Log(LogEntryType.Error, message, "ItemDownloader");
        }

        private void LogInfo(string message)
        {
            Logging.Log(LogEntryType.Information, message, "ItemDownloader");
        }

        private void LogEvent(string message)
        {
            Logging.Log(LogEntryType.Event, message, "ItemDownloader");
        }

        private void app_AuctionHouseFilesSaved(object sender, AuctionHouseFilesDownloadedEventArgs e)
        {
            if (e.FileType != AuctionHouseFile.DataFile)
            {
                return;
            }

            string[] itemIdFiles;
            foreach (var file in e.Files)
            {
                var data = JsonConvert.DeserializeObject<AuctionDump>(File.ReadAllText(file));
                // get all item id's

                var alliance = data.Alliance.Auctions.Select(x => x.ItemId).Distinct().AsParallel();
                var horde = data.Horde.Auctions.Select(x => x.ItemId).Distinct().AsParallel();
                var neutral = data.Neutral.Auctions.Select(x => x.ItemId).Distinct().AsParallel();

                var list = alliance.Union(horde).Union(neutral).Distinct().AsParallel();

                // get list of existing item ids
                var itemIdFolder = Path.Combine(this.app.DataFolder, e.Region, "ItemIds");

                try
                {
                    // ensure directory exists
                    Directory.CreateDirectory(itemIdFolder);
                }
                catch (System.Exception ex)
                {
                    this.LogError("Error ensuring ItemIds folder exists. " + ex.Message);
                    continue;
                }

                try
                {
                    itemIdFiles = Directory.GetFiles(itemIdFolder, "*.json", SearchOption.TopDirectoryOnly);
                }
                catch (System.Exception ex)
                {
                    this.LogError("Error retrieving list of existing ItemId data files. " + ex.Message);
                    continue;
                }

                var existingIds = itemIdFiles.Where(
                    x =>
                    {
                        var value = 0;
                        return int.TryParse(Path.GetFileNameWithoutExtension(x), out value);
                    }).Select(x => int.Parse(Path.GetFileNameWithoutExtension(x))).AsParallel();

                // get list of newly discovered item ids
                var newIds = list.Except(existingIds).AsParallel();

                this.app.DownloadItemData(newIds, e.Region);

                //// start downloading new item ids
                //var client = new WOWSharp.Community.Wow.WowClient(Region.Default);
                //foreach (var id in newIds)
                //{
                //this.app.DownloadItemData();
                //    var idData = await client.GetItemAsync(id);
                //    File.WriteAllText(Path.Combine(itemIdFolder, id + ".json"), JsonConvert.SerializeObject(idData));
                //}
            }
        }

        public void Disconnect()
        {
            this.app.AuctionHouseFilesSaved -= this.app_AuctionHouseFilesSaved;
            this.app = null;
        }
    }
}
