﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System;
    using System.IO;
    using System.Windows.Forms;

    using Codefarts.Settings;
    using Codefarts.Settings.Xml;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // setup settings
            var settingsFile = Path.Combine(Environment.CurrentDirectory, "settings.xml");
            SettingsManager.Instance.Values = new XmlDocumentLinqValues(settingsFile, true);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
