﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Windows.Forms;

    using Codefarts.WowTracks.DataModels;

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new OptionsDialog())
            {
                form.ShowDialog(this);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            using (var dialog = new AddRealmDialog())
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    var realm = dialog.GetSelectedRealm();
                    if (realm != null)
                    {
                        using (var db = new TrackerContext("Tracker.sdf"))
                        {
                            var listViewItem = new ListViewItem(realm.Name);
                            var region = WOWSharp.Community.Region.AllRegions.FirstOrDefault(x => x.SupportedLocales.Contains(realm.Locale.Replace("_", "-")));
                            if (region != null)
                            {
                                // check if already tracking
                                if (db.TrackingRealms.Any(x => x.Region == region.Name && x.Name == realm.Name))
                                {
                                    return;
                                }

                                listViewItem.SubItems.Add(region.Name);
                                this.lvRealms.Items.Add(listViewItem);

                                var tracker = new RealmTrackingModel() { Name = realm.Name, Region = region.Name };
                                db.TrackingRealms.Add(tracker);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DbConfiguration.SetConfiguration(new TrackerDbConfiguration());
            Database.SetInitializer(new CreateDatabaseIfNotExists<TrackerContext>());

            var callback = new Func<RealmTrackingModel, ListViewItem>(
                cb =>
                {
                    var item = new ListViewItem(cb.Name);
                    item.SubItems.Add(cb.Region);
                    return item;
                });

            using (var db = new TrackerContext("Tracker.sdf"))
            {
                var results = db.TrackingRealms.ToArray();
                var items = from r in results select callback(r);
                this.lvRealms.Items.AddRange(items.ToArray());
            }
        }
    }
}
