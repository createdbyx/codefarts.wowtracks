﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    using Action = Microsoft.Win32.TaskScheduler.Action;

    public partial class AddRealmDialog : Form
    {
        private int counter;

        public AddRealmDialog()
        {
            InitializeComponent();
        }

        private void AddRealmDialog_Load(object sender, EventArgs e)
        {
            this.cboRegion.DataSource = WOWSharp.Community.Region.AllRegions;
        }

        private void DoneLoading()
        {
            this.tmrLoading.Stop();
            this.SetText(string.Empty);
        }

        private void SetText(string text)
        {
            if (this.lblLoading.InvokeRequired)
            {
                this.Invoke(new Action<string>(this.SetText), new object[] { text });
            }
            else
            {
                this.lblLoading.Text = text;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.counter++;
            this.SetText("Loading " + new string('.', this.counter % 4));
        }

        private async void cboRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Init client
            var selectedItem = this.cboRegion.SelectedItem as Region;
            this.cboLocale.DataSource = selectedItem.SupportedLocales;

            await this.PopulateRealmList(selectedItem);
        }

        private async Task PopulateRealmList(Region selectedItem)
        {
            var client = new WowClient(selectedItem, null, (string)this.cboLocale.SelectedValue, null);

            // Get realms
            this.tmrLoading.Start();
            var results = await client.GetRealmStatusAsync();
            this.cboRealm.DataSource = results.Realms;
            this.DoneLoading();
        }

        public Realm GetSelectedRealm()
        {
            return this.cboRealm.SelectedItem as Realm;
        }

        private async void cboLocale_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = this.cboRegion.SelectedItem as Region;
            await this.PopulateRealmList(selectedItem);
        }
    }
}
