﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System;
    using System.Data.Entity;
    using System.IO;
    using System.Windows.Forms;

    using Codefarts.Settings;

    public partial class OptionsDialog : Form
    {
        private bool dataFolderChanged;

        public OptionsDialog()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.txtDataFolder.Text = dialog.SelectedPath;
                }
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (dataFolderChanged)
            {
                SettingsManager.Instance.SetValue(SettingConstants.DataFolder, this.txtDataFolder.Text.Trim());
                DbConfiguration.SetConfiguration(new TrackerDbConfiguration());
            }
        }

        private void txtDataFolder_TextChanged(object sender, EventArgs e)
        {
            this.dataFolderChanged = true;
        }

        private void OptionsDialog_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = string.IsNullOrWhiteSpace(this.txtDataFolder.Text) || Directory.Exists(this.txtDataFolder.Text);
        }

        private void OptionsDialog_Load(object sender, EventArgs e)
        {
            this.txtDataFolder.Text = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
        }
    }
}
