﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace AuctionHouseDataFileCompressorPlugin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.IdManager;
    using Codefarts.Logging;
    using Codefarts.WowTracks.SchedulerApi;

    [Export(typeof(IPlugin))]
    public class AuctionHouseDataFileCompressor : IPlugin
    {
        private Application app;

        private IdManager idManager = new IdManager();
        private Dictionary<DateTime, KeyValuePair<long, string>> filesToDelete = new Dictionary<DateTime, KeyValuePair<long, string>>();
        private CancellationTokenSource backgroundCancel;

        private Task deleteTask;

        private bool connected;

        public void Connect(Application app)
        {
            if (this.connected)
            {
                return;
            }

            this.app = app;
            this.app.AuctionHouseFilesSaved += this.app_AuctionHouseFilesSaved;

            this.backgroundCancel = new CancellationTokenSource();
            this.deleteTask = Task.Factory.StartNew(
                () =>
                {
                    while (this.connected && !this.backgroundCancel.IsCancellationRequested)
                    {
                        this.DeleteFiles(false);
                        Thread.Sleep(1000);
                    }

                    this.DeleteFiles(true);

                }, this.backgroundCancel.Token);

            this.connected = true;
        }

        private async void app_AuctionHouseFilesSaved(object sender, AuctionHouseFilesDownloadedEventArgs e)
        {
            if (e.FileType != AuctionHouseFile.DataFile)
            {
                return;
            }

            var taskId = this.idManager.NewId();
            var files = e.Files.Where(x => Path.GetExtension(x).Equals(".json", StringComparison.OrdinalIgnoreCase)).ToArray();
            var index = 1;
            this.Log(taskId, string.Format("Need to compress {0} files", index));
            foreach (var file in files)
            {
                if (!File.Exists(file))
                {
                    this.LogError(taskId, string.Format("Missing file! \"{0}\"", file));
                    continue;
                }

                this.Log(taskId, string.Format("Compressing {0}/{1}", index, files.Length));
                var zipFile = Path.ChangeExtension(file, ".zip");

                try
                {
                    // log the time for performance metrics
                    var startTime = DateTime.Now.Ticks;

                    // read json and compress to zip with the same name
                    using (var jsonStream = File.OpenWrite(zipFile))
                    {
                        using (var writer = new ZipArchive(jsonStream, ZipArchiveMode.Create, false))
                        {
                            var entry = writer.CreateEntry(Path.GetFileName(file), CompressionLevel.Optimal);

                            using (var streamWriter = new StreamWriter(entry.Open()))
                            {
                                await streamWriter.WriteAsync(File.ReadAllText(file));
                            }
                        }
                    }

                    var totalSeconds = TimeSpan.FromTicks(DateTime.Now.Ticks - startTime).TotalSeconds;
                    this.Log(taskId, string.Format("Done compressing {0}/{1} Time: {2} seconds", index, files.Length, totalSeconds));

                    // delete original json file
                    this.filesToDelete.Add(DateTime.Now, new KeyValuePair<long, string>(taskId, file));
                }
                catch (Exception ex)
                {
                    this.LogError(taskId, ex.Message);
                    try
                    {
                        if (File.Exists(file) && File.Exists(zipFile))
                        {
                            File.Delete(zipFile);
                            this.Log(taskId, string.Format("Deleted zip file \"{0}\"", zipFile));
                        }
                    }
                    catch (Exception exx)
                    {
                        this.LogError(taskId, string.Format("Problem deleting zip file. {0} \"{1}\"", exx.Message, zipFile));
                    }
                }

                index++;
            }

            this.Log(taskId, "Compression completed");
        }

        private void Log(long taskId, string message)
        {
            Logging.Log(LogEntryType.Information, string.Format("{0}: {1}", taskId, message), "AHDataFileCompressPlugin");
        }

        private void LogError(long taskId, string message)
        {
            Logging.Log(LogEntryType.Error, string.Format("{0}: {1}", taskId, message), "AHDataFileCompressPlugin");
        }

        ~AuctionHouseDataFileCompressor()
        {
            if (!this.connected)
            {
                return;
            }
                                     
            this.DeleteFiles(true);
        }

        public void Disconnect()
        {
            if (!this.connected)
            {
                return;
            }

            this.backgroundCancel.Cancel();
            this.DeleteFiles(true);
            this.app = null;
            this.deleteTask = null;
            this.connected = false;
        }

        private void DeleteFiles(bool force)
        {
            var keys = new List<DateTime>();

            foreach (var pair in this.filesToDelete)
            {
                if (force || DateTime.Now > pair.Key + TimeSpan.FromMinutes(5))  // keep file for 5 minutes to allow other plugins to do work
                {
                    this.Log(pair.Value.Key, string.Format("Deleting original json \"{0}\"", pair.Value.Value));
                    File.Delete(pair.Value.Value);
                    keys.Add(pair.Key);
                }
            }

            foreach (var key in keys)
            {
                this.filesToDelete.Remove(key);
            }
        }
    }
}
