﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public class NameIdModel
    {
        public NameIdModel(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public NameIdModel()
        {
            
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
