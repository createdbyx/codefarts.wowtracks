﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public class ItemSource
    {
        public int sourceId { get; set; }
        public string sourceType { get; set; }
    } 
}