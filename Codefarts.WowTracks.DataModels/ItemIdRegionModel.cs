﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public class ItemIdRegionModel
    {
        public ItemIdRegionModel(int itemId, string region)
        {
            this.ItemId = itemId;
            this.Region = region;
        }

        public ItemIdRegionModel()
        {
        }

        public int ItemId { get; set; }
        public string Region { get; set; }
    }
}