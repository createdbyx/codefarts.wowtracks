﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public class AuctionData
    {
        public AuctionDataFile[] files { get; set; }
    }

    public class AuctionDataFile
    {
        public long lastModified { get; set; }
        public string url { get; set; }
    }                                      
}
