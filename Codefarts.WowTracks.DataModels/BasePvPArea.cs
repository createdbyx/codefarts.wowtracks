﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public abstract class BasePvPArea
    {
        public int Id { get; set; }
        public int Area { get; set; }
        public int ControllingFaction { get; set; }
        public long Next { get; set; }
        public int Status { get; set; }
    }
}