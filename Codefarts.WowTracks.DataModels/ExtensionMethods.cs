﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public static class ExtensionMethods
    {

        public static RealmTrackingModel Clone(this RealmTrackingModel model)
        {
            return new RealmTrackingModel()
            {
                AuctionHouseTrackingInterval = model.AuctionHouseTrackingInterval,
                LastAuctionHouseUpdate = model.LastAuctionHouseUpdate,
                Id = model.Id,
                LastUpdate = model.LastUpdate,
                Name = model.Name,
                Region = model.Region,
                StartedTracking = model.StartedTracking,
                StartedTrackingAuctionHouse = model.StartedTrackingAuctionHouse,
                TrackAuctionHouse = model.TrackAuctionHouse,
                TrackingInterval = model.TrackingInterval
            };
        }
    }
}
