﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System;
    using System.Globalization;

    public class AuctionDataModel
    {
        /// <summary>
        ///   Gets or sets the auction id
        /// </summary>
        public long AuctionId
        {
            get;
              set;
        }

        /// <summary>
        ///   gets or sets the item id
        /// </summary>
        public int ItemId
        {
            get;
               set;
        }

        /// <summary>
        ///   Gets or sets the owner name
        /// </summary>
        public string OwnerName
        {
            get;
              set;
        }

        /// <summary>
        ///   Gets or sets the bid value in copper
        /// </summary>
        public long CurrentBidValue
        {
            get;
              set;
        }

        /// <summary>
        ///   gets or sets the buyout value in copper
        /// </summary>
        public long? BuyoutValue
        {
            get;
              set;
        }

        /// <summary>
        ///   gets or sets the quantity
        /// </summary>
        public int Quantity
        {
            get;
              set;
        }

        /// <summary>
        ///  gets or sets the owner's realm for the auction
        /// </summary>
        public string OwnerRealm
        {
            get;
              set;
        }

        /// <summary>
        /// Gets or sets the time left for the auction to expire
        /// </summary>
        public AuctionTimeLeft TimeLeft
        {
            get;
              set;
        }

        /// <summary>
        /// No clue what this value is about. rand property for auction returned by Blizzard's API
        /// </summary>
        public long Random
        {
            get;
              set;
        }

        /// <summary>
        /// No clue what this value is about. seed property for auction returned by Blizzard's API
        /// </summary>
        public long Seed
        {
            get;
              set;
        }

        /// <summary>
        /// Pet species Id (in case the auctioned item is a battle pet)
        /// </summary>
        public int PetSpeciesId
        {
            get;
              set;
        }

        /// <summary>
        /// Pet breed Id (in case the auctioned item is a battle pet)
        /// </summary>
        public int PetBreedId
        {
            get;
              set;
        }

        /// <summary>
        /// Pet level (in case the auctioned item is a battle pet)
        /// </summary>
        public int PetLevel
        {
            get;
              set;
        }

        /// <summary>
        /// Pet quality (in case the auctioned item is a battle pet)
        /// </summary>
        public ItemQuality PetQuality
        {
            get;
              set;
        }

        /// <summary>
        ///   Gets string representation (for debugging purposes)
        /// </summary>
        /// <returns> Gets string representation (for debugging purposes) </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture,
                                 "Auction {0}, Item Id {1}, Owner {2}, Bid {3}, Buyout {4}, Quantity {5} Faction {6} CaptureDate {7}",
                                 AuctionId, ItemId, OwnerName, CurrentBidValue, BuyoutValue, Quantity, Faction, CaptureDate);
        }




        public DateTime CaptureDate { get; set; }
        public Faction Faction { get; set; }
    }
}
