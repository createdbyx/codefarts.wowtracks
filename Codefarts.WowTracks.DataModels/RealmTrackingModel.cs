﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    public class RealmTrackingModel : TrackingEntry
    {
        private bool trackAuctionHouse;

        private DateTime startedTrackingAuctionHouse;

        private DateTime lastAuctionHouseUpdate;

        private long auctionHouseTrackingInterval;

        //[XmlIgnore]
        //public DateTime LastAuctionHouseUpdateDate
        //{
        //    get
        //    {
        //        return this.lastAuctionHouseUpdate.Date;
        //    }

        //    set
        //    {
        //        this.LastAuctionHouseUpdate = value.Date + this.lastAuctionHouseUpdate.TimeOfDay;
        //        this.OnPropertyChanged("LastAuctionHouseUpdateDate");
        //    }
        //}

        //[XmlIgnore]
        //public TimeSpan LastAuctionHouseUpdateTime
        //{
        //    get
        //    {
        //        return this.lastAuctionHouseUpdate.TimeOfDay;
        //    }

        //    set
        //    {
        //        this.LastAuctionHouseUpdate = this.lastAuctionHouseUpdate + value;
        //        this.OnPropertyChanged("LastAuctionHouseUpdateTime");
        //    }
        //}

        //[XmlIgnore]
        //public DateTime StartedTrackingAuctionHouseDate
        //{
        //    get
        //    {
        //        return this.startedTrackingAuctionHouse.Date;
        //    }

        //    set
        //    {
        //        this.StartedTrackingAuctionHouse = value.Date + this.startedTrackingAuctionHouse.TimeOfDay;
        //        this.OnPropertyChanged("StartedTrackingAuctionHouseDate");
        //    }
        //}

        //[XmlIgnore]
        //public TimeSpan StartedTrackingAuctionHouseTime
        //{
        //    get
        //    {
        //        return this.startedTrackingAuctionHouse.TimeOfDay;
        //    }

        //    set
        //    {
        //        this.StartedTrackingAuctionHouse = this.startedTrackingAuctionHouse.Date + value;
        //        this.OnPropertyChanged("StartedTrackingAuctionHouseTime");
        //    }
        //}

        public bool TrackAuctionHouse
        {
            get
            {
                return this.trackAuctionHouse;
            }

            set
            {
                if (this.trackAuctionHouse == value)
                {
                    return;
                }

                this.trackAuctionHouse = value;
                this.OnPropertyChanged("TrackAuctionHouse");
            }
        }

        public DateTime StartedTrackingAuctionHouse
        {
            get
            {
                return this.startedTrackingAuctionHouse;
            }

            set
            {
                if (this.startedTrackingAuctionHouse == value)
                {
                    return;
                }

                this.startedTrackingAuctionHouse = value;
                this.OnPropertyChanged("StartedTrackingAuctionHouse");
            }
        }

        public DateTime LastAuctionHouseUpdate
        {
            get
            {
                return this.lastAuctionHouseUpdate;
            }

            set
            {
                if (this.lastAuctionHouseUpdate == value)
                {
                    return;
                }

                this.lastAuctionHouseUpdate = value;
                this.OnPropertyChanged("LastAuctionHouseUpdate");
            }
        }

        public long AuctionHouseTrackingInterval
        {
            get
            {
                return this.auctionHouseTrackingInterval;
            }

            set
            {
                if (this.auctionHouseTrackingInterval == value)
                {
                    return;
                }

                this.auctionHouseTrackingInterval = value;
                this.OnPropertyChanged("AuctionHouseTrackingInterval");
            }
        }
    }
}