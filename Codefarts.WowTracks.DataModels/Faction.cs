﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System.Runtime.Serialization;

    public enum Faction
    {
        /// <summary>
        ///   Neutral
        /// </summary>
        [EnumMember(Value = "neutral")]
        Neutral = -1,

        /// <summary>
        ///   Alliance
        /// </summary>
        [EnumMember(Value = "alliance")]
        Alliance = 0,

        /// <summary>
        ///   Horde (Loktar Ogar! Victory or Death! For the Horde!)
        /// </summary>
        [EnumMember(Value = "horde")]
        Horde = 1,
    }
}