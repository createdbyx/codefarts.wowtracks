﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System;
    using System.Collections.Generic;

    public class RegionModel
    {
        public string FriendlyName { get; set; }
        public Uri Host { get; set; }
        public IList<string> Locales { get; set; }
        public string Name { get; set; }
    }
}