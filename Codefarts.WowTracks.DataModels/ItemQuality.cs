﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    /// <summary>
    ///   An enumeration representing item quality
    /// </summary>
    public enum ItemQuality
    {
        /// <summary>
        ///   Poor (Grey)
        /// </summary>
        Poor = 0,

        /// <summary>
        ///   Common (White)
        /// </summary>
        Common = 1,

        /// <summary>
        ///   Uncommon (Green)
        /// </summary>
        Uncommon = 2,

        /// <summary>
        ///   Rare (Blue)
        /// </summary>
        Rare = 3,

        /// <summary>
        ///   Epic (Purple)
        /// </summary>
        Epic = 4,

        /// <summary>
        ///   Legendary (Orange)
        /// </summary>
        Legendary = 5,

        /// <summary>
        ///   Artifact (Golden)
        /// </summary>
        Artifact = 6,

        /// <summary>
        ///   Heirloom (Golden)
        /// </summary>
        Heirloom = 7
    }
}