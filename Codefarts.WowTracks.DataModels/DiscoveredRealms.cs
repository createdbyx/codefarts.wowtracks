﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System;

    public class DiscoveredRealms
    {
        public int Id { get; set; }
        public string RealmName { get; set; }
        public DateTime Discovered { get; set; }
        public DateTime LastChecked { get; set; }
    }
}
