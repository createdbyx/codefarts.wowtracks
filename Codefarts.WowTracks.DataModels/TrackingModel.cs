﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    using Codefarts.WowTracks.DataModels.Annotations;

    public class TrackingModel : INotifyPropertyChanged 
    {                                      
        private ObservableCollection<RealmTrackingModel> realms;   

        public ObservableCollection<RealmTrackingModel> Realms
        {
            get
            {
                return this.realms;
            }

            set
            {
                this.realms = value;
                this.OnPropertyChanged("Realms");
            }
        }

        public TrackingModel()
        {
            this.Realms = new ObservableCollection<RealmTrackingModel>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}