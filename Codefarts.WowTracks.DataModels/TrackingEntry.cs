﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    using System;
    using System.ComponentModel;                  
    using System.Xml.Serialization;

    using Codefarts.WowTracks.DataModels.Annotations;

    public abstract class TrackingEntry : INotifyPropertyChanged  
    {
        private long id;

        private string name;

        private string region;

        private DateTime startedTracking;

        private DateTime lastUpdate;
                      
        private long trackingInterval;              

        //[XmlIgnore]
        //public DateTime LastUpdateDate
        //{
        //    get
        //    {
        //        return this.lastUpdate.Date;
        //    }

        //    set
        //    {
        //        this.LastUpdate = value.Date + this.lastUpdate.TimeOfDay;
        //        this.OnPropertyChanged("LastUpdateDate");
        //    }
        //}

        //[XmlIgnore]
        //public TimeSpan LastUpdateTime
        //{
        //    get
        //    {
        //        return this.lastUpdate.TimeOfDay;
        //    }

        //    set
        //    {
        //        this.LastUpdate = this.lastUpdate + value;
        //        this.OnPropertyChanged("LastUpdateTime");
        //    }
        //}

        //[XmlIgnore]
        //public DateTime StartedTrackingDate
        //{
        //    get
        //    {
        //        return this.startedTracking.Date;
        //    }

        //    set
        //    {
        //        this.StartedTracking = value.Date + this.startedTracking.TimeOfDay;
        //        this.OnPropertyChanged("StartedTrackingDate");
        //    }
        //}

        //[XmlIgnore]
        //public TimeSpan StartedTrackingTime
        //{
        //    get
        //    {
        //        return this.startedTracking.TimeOfDay;
        //    }

        //    set
        //    {
        //        this.StartedTracking = this.startedTracking.Date + value;
        //        this.OnPropertyChanged("StartedTrackingTime");
        //    }
        //}

        public long Id
        {
            get
            {
                return this.id;
            }
            set
            {
                if (this.id == value)
                {
                    return;
                }

                this.id = value;
                this.OnPropertyChanged("Id");
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (this.name == value)
                {
                    return;
                }

                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        public string Region
        {
            get
            {
                return this.region;
            }
            set
            {
                if (this.region == value)
                {
                    return;
                }

                this.region = value;
                this.OnPropertyChanged("Region");
            }
        }

        public DateTime StartedTracking
        {
            get
            {
                return this.startedTracking;
            }
            set
            {
                if (this.startedTracking == value)
                {
                    return;
                }

                this.startedTracking = value;
                this.OnPropertyChanged("StartedTracking");
            }
        }

        public DateTime LastUpdate
        {
            get
            {
                return this.lastUpdate;
            }
            set
            {
                if (this.lastUpdate == value)
                {
                    return;
                }

                this.lastUpdate = value;
                this.OnPropertyChanged("LastUpdate");
            }
        }

        public long TrackingInterval
        {
            get
            {
                return this.trackingInterval;
            }

            set
            {
                if (this.trackingInterval == value)
                {
                    return;
                }

                this.trackingInterval = value;
                this.OnPropertyChanged("TrackingInterval");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

