﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataModels
{
    public class Realm
    {
        public int Id { get; set; }
        public string Battlegroup { get; set; }
        public string Locale { get; set; }
        public string Name { get; set; }
        public string Population { get; set; }
        public bool Queue { get; set; }
        public string Slug { get; set; }
        public bool Status { get; set; }
        public string Timezone { get; set; }
        public TolBarad Tolbarad { get; set; }
        public string Type { get; set; }
        public Wintergrasp Wintergrasp { get; set; }
    }


}