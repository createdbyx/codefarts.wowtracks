﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.CommandLibrary
{
    using System.IO;
    using System.Net;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a commend for downloading files.
    /// </summary>
    public class DownloadFileCommand
    {
        public static CommandResult<string> DownloadFile(IDictionary<string, object> parameters)
        {
            if (parameters == null)
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentNullException("parameters") };
            }

            Uri uri;
            if (!parameters.TryGetParameter("Url", out uri))
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentException("Could not acquire 'Url' parameter.", "Url") };
            }

            Stream outputStream;
            if (!parameters.TryGetParameter("OutputStream", out outputStream) || outputStream == null)
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentException("Could not acquire 'OutputStream' parameter.", "OutputStream") };
            }

            var bufferSize = 8192;
            if (parameters.TryGetParameter("BufferSize", out bufferSize, bufferSize) && bufferSize < 1)
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentException("Invalid 'BufferSize' parameter.", "BufferSize") };
            }


            Action<string> callback;
            Func<float, int, bool> progressCallback;
            parameters.TryGetParameter("Callback", out callback);
            parameters.TryGetParameter("ProgressCallback", out progressCallback);

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(uri);

                if (callback != null)
                {
                    callback(uri.ToString());
                }

                DateTime ifModifiedSince;
                if (parameters.TryGetParameter("IfModifiedSince", out ifModifiedSince))
                {
                    request.IfModifiedSince = ifModifiedSince;
                }

                using (var httpResponse = (HttpWebResponse)request.GetResponse())
                {
                    var contentLength = httpResponse.ContentLength;
                    if (callback != null)
                    {
                        callback(string.Format("{0} bytes to read.", contentLength));
                    }

                    using (var writer = new BinaryWriter(outputStream))
                    {
                        using (var reader = new BinaryReader(httpResponse.GetResponseStream()))
                        {
                            var readCount = 0;
                            var chunk = reader.ReadBytes(bufferSize);
                            var startTime = DateTime.Now.Ticks;
                            while (chunk.Length > 0)
                            {
                                writer.Write(chunk, 0, chunk.Length);
                                readCount += chunk.Length;

                                // Displays the operation identifier, and the transfer progress.
                                if (callback != null)
                                {
                                    callback(
                                        string.Format(
                                            "downloaded {0} of {1} bytes. {2} % complete...",
                                            readCount,
                                            contentLength,
                                            Math.Min((readCount / contentLength) * 100, 100)));
                                }

                                // (readCount) bytes per second divided by ( (startTime) divided by ticks per second to get number of seconds elapsed )
                                var bytesPerSecond = readCount / TimeSpan.FromTicks(DateTime.Now.Ticks - startTime).TotalSeconds;
                                if (progressCallback != null && !progressCallback(Math.Min(((float)readCount / contentLength) * 100f, 100f), (int)bytesPerSecond))
                                {
                                    return new CommandResult<string>()
                                               {
                                                   Successful = false,
                                                   ReturnValues = new Dictionary<string, object> { { "Canceled", true }, { "ExitCode", -6 } }
                                               };
                                }

                                chunk = reader.ReadBytes(bufferSize);
                            }

                            reader.Close();
                        }

                        writer.Flush();
                    }

                    if (callback != null)
                    {
                        callback("Success.");
                    }

                }
            }
            catch (WebException we)
            {
                var httpResponse = we.Response as HttpWebResponse;
                if (httpResponse == null)
                {
                    return BuildErrorResponse(callback, string.Format("Error downloading! Status: {0}", we.Status), -5);
                }

                switch (httpResponse.StatusCode)
                {
                    case HttpStatusCode.NotModified:
                        return BuildErrorResponse(callback, "StatusCode: 304 Not modified no need to download!", -3);

                    case HttpStatusCode.NotFound:
                        return BuildErrorResponse(callback, string.Format("StatusCode: 404 Status: {0} Not found!", we.Status), -7);

                    default:
                        return BuildErrorResponse(callback, string.Format("StatusCode: {0}  Status: {1} ", httpResponse.StatusCode, we.Status) + we.Message, -4);
                }
            }
            catch (Exception ex)
            {
                return BuildErrorResponse(callback, "Unknown Error downloading", -4);
            }

            return new CommandResult<string>() { Successful = true };
        }

        private static CommandResult<string> BuildErrorResponse(Action<string> callback, string message, int exitCode)
        {
            if (callback != null)
            {
                callback(message);
            }

            return new CommandResult<string>()
                       {
                           Successful = false,
                           Error = new Exception(message),
                           ReturnValues = new Dictionary<string, object> { { "ExitCode", exitCode } }
                       };
        }
    }
}
