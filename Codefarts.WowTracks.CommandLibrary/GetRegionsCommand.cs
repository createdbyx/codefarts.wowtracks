﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.CommandLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Codefarts.WowTracks.DataModels;

    public class GetRegionsCommand
    {
        public static CommandResult<string> GetRegionInformation(IDictionary<string, object> parameters)
        {
            var region = string.Empty;
            if (!parameters.TryGetParameter("Region", out region))
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentException("Missing 'Region' parameter.") };
            }

            var allRegions = GetAllRegionInformation(null);
            if (string.IsNullOrWhiteSpace(region))
            {
                return allRegions;
            }

            RegionModel[] regions;
            allRegions.ReturnValues.TryGetParameter("Regions", out regions);
            var item = regions.FirstOrDefault(x => string.Compare(x.FriendlyName, region, StringComparison.OrdinalIgnoreCase) == 0 ||
                                                   string.Compare(x.Name, region, StringComparison.OrdinalIgnoreCase) == 0);
            if (item == null)
            {
                return new CommandResult<string>() { Successful = false, Error = new ArgumentException("Unknown region specified.") };
            }

            regions = new[] { item };

            return new CommandResult<string>() { Successful = true, ReturnValues = new Dictionary<string, object> { { "Regions", regions } } };
        }

        public static CommandResult<string> GetAllRegionInformation(IDictionary<string, object> parameters)
        {
            var regions = new[]
                          {
                              new RegionModel()
                                  {
                                      FriendlyName = "United States",
                                      Name = "US",
                                      Host = new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + "us.battle.net"), 
                                      Locales = new List<string>(new[]{"en_US","es_MX","pt_BR"}) 
                                  },
                              new RegionModel()
                                  {
                                      FriendlyName = "Europe",
                                      Name = "EU",
                                      Host = new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + "eu.battle.net"), 
                                      Locales = new List<string>(new[]{"en_GB","es_ES","fr_FR","ru_RU","de_DE","pt_PT","it_IT"}) 
                                  }  ,
                              new RegionModel()
                                  {
                                      FriendlyName = "Korea",
                                      Name = "KR",
                                      Host = new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + "kr.battle.net"), 
                                      Locales = new List<string>(new[]{"ko_KR"}) 
                                  }   ,
                              new RegionModel()
                                  {
                                      FriendlyName = "Taiwan",
                                      Name = "TW",
                                      Host = new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + "tw.battle.net"), 
                                      Locales = new List<string>(new[]{"zh_TW"}) 
                                  }  ,
                              new RegionModel()
                                  {
                                      FriendlyName = "China",
                                      Name = "CN",
                                      Host = new Uri(Uri.UriSchemeHttp + Uri.SchemeDelimiter + "www.battlenet.com.cn"), 
                                      Locales = new List<string>(new[]{"zh_CN"}) 
                                  }
                          };

            return new CommandResult<string>() { Successful = true, ReturnValues = new Dictionary<string, object> { { "Regions", regions } } };
        }
    }
}