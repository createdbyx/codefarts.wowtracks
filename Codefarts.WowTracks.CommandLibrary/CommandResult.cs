﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.CommandLibrary
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a defined class for command return types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommandResult<T>
    {
        public bool Successful { get; set; }
        public Exception Error { get; set; }
        public IDictionary<T, object> ReturnValues { get; set; }
    }
}