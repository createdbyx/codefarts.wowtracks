﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.CommandLibrary
{
    using System;
    using System.Collections.Generic;

    public static class DictionaryExtensionMethods
    {
        public static bool TryGetParameter<T>(this IDictionary<string, object> dictionary, string name, out T value)
        {
            return TryGetParameter(dictionary, name, out value, default(T));
        }

        public static bool TryGetParameter<T>(this IDictionary<string, object> dictionary, string name, out T value, T defaultValue)
        {
            try
            {
                value = (T)dictionary[name];
                return true;
            }
            catch (Exception)
            {
                value = defaultValue;
                return false;
            }
        }
    }
}