﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class SettingsModel:INotifyPropertyChanged
    {
        private string dataFolder;
        private string logFolder;

        private bool automaticallyRunSchedulesOnStartup;
        private bool autoScrollLogs;

        private TimeSpan minimumWaitTimeBetweenSchedules;

        public bool AutoScrollLogs
        {
            get
            {
                return this.autoScrollLogs;
            }

            set
            {
                if (this.autoScrollLogs == value)
                {
                    return;
                }

                this.autoScrollLogs = value;
                this.OnPropertyChanged("AutoScrollLogs");
            }
        }

        public int MaxProcessingTasks
        {
            get
            {
                return this.maxProcessingTasks;
            }

            set
            {
                if (this.maxProcessingTasks == value)
                {
                    return;
                }

                this.maxProcessingTasks = value;
                this.OnPropertyChanged("MaxProcessingTasks");  
            }
        }

        private int maxProcessingTasks;

        public TimeSpan MinimumWaitTimeBetweenSchedules
        {
            get
            {
                return this.minimumWaitTimeBetweenSchedules;
            }

            set
            {
                if (this.minimumWaitTimeBetweenSchedules == value)
                {
                    return;
                } 
                
                this.minimumWaitTimeBetweenSchedules = value;
                this.OnPropertyChanged("MinimumWaitTimeBetweenSchedules");
            }
        }

        public bool AutomaticallyRunSchedulesOnStartup
        {
            get
            {
                return this.automaticallyRunSchedulesOnStartup;
            }

            set
            {
                if (this.automaticallyRunSchedulesOnStartup == value)
                {
                    return;
                }

                this.automaticallyRunSchedulesOnStartup = value;
                this.OnPropertyChanged("AutomaticallyRunSchedulesOnStartup");
            }
        }

        public string DataFolder
        {
            get
            {
                return this.dataFolder;
            }

            set
            {
                if (this.dataFolder == value)
                {
                    return;
                }

                this.dataFolder = value;
                this.OnPropertyChanged("DataFolder");
            }
        }

        public string LogFolder
        {
            get
            {
                return this.logFolder;
            }

            set
            {
                if (this.logFolder == value)
                {
                    return;
                }

                this.logFolder = value;
                this.OnPropertyChanged("LogFolder");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}