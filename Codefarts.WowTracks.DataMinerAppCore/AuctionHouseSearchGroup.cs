﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore
{
    using System.Collections.ObjectModel;

    public class AuctionHouseSearchGroup:SearchGroup     
    {
        public ObservableCollection<IAuctionHouseSearchFilter> Filters { get; set; }
    }
}