﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using System;
    using System.Collections.Generic;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataModels;

    public class MainAuctionHouseFilterModel : MainFilterModel, IAuctionHouseSearchFilter
    {
        private string[] regions;

        private RealmTrackingModel[] realms;

        private int selectedRegionIndex = -1;

        private int selectedRealmIndex = -1;

        private bool enabled=true;

        public int SelectedRealmIndex
        {
            get
            {
                return this.selectedRealmIndex;
            }

            set
            {
                this.selectedRealmIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged("SelectedRealm");
                this.OnPropertyChanged("StartDate");
                this.OnPropertyChanged("EndDate");
                this.OnPropertyChanged("FirstCaptureDate");
                this.OnPropertyChanged("LastCaptureDate");
            }
        }

        public int SelectedRegionIndex
        {
            get
            {
                return this.selectedRegionIndex;
            }

            set
            {
                if (value == this.selectedRegionIndex)
                {
                    return;
                }

                this.selectedRegionIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged("SelectedRegion");
            }
        }

        public string[] Regions
        {
            get
            {
                return this.regions;
            }

            set
            {
                if (Equals(value, this.regions))
                {
                    return;
                }

                this.regions = value;
                this.OnPropertyChanged();
            }
        }

        public RealmTrackingModel[] Realms
        {
            get
            {
                return this.realms;
            }

            set
            {
                if (Equals(value, this.realms))
                {
                    return;
                }

                this.realms = value;
                this.OnPropertyChanged();
                this.SelectedRealmIndex = this.realms != null && realms.Length == 0 ? 0 : -1;
            }
        }      
 
        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items;
        }

        public string Name
        {
            get
            {
                return "Search";
            }
        }

        public string Control
        {
            get
            {
                return "Codefarts.WowTracks.DataMinerWPF.Controls.MainAuctionHouseFilterControl";
            }
        }

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value == this.enabled)
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        public IAuctionHouseSearchFilter Clone()
        {
            return new MainAuctionHouseFilterModel()
                        {
                            EndDate = this.EndDate,
                            Realms = this.Realms,
                            Regions = this.Regions,
                            StartDate = this.StartDate
                        };
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {
        }

        public override string SelectedRegion
        {
            get
            {
                return this.regions[this.selectedRegionIndex];
            }

            set
            {
                // do nothing
            }
        }

        public override string SelectedRealm
        {
            get
            {
                try
                {
                    return this.realms[this.selectedRealmIndex].Name;
                }
                catch
                {
                    return null;
                }
            }

            set
            {
                // do nothing
            }
        }     

        public RealmTrackingModel SelectedRealmModel
        {
            get
            {
                try
                {
                    return this.realms[this.selectedRealmIndex];
                }
                catch
                {
                    return null;
                }
            }
        }     
    }
}