﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore.Models
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;
                 
    public class MainFilterModel : INotifyPropertyChanged
    {
        private DateTime startDate;

        private DateTime endDate;

        private string selectedRegion;

        private string selectedRealm;

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                if (value.Equals(this.startDate))
                {
                    return;
                }

                this.startDate = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime EndDate
        {
            get
            {
                return this.endDate;
            }

            set
            {
                if (value.Equals(this.endDate))
                {
                    return;
                }

                this.endDate = value;
                this.OnPropertyChanged();
            }
        }

        public virtual string SelectedRegion
        {
            get
            {
                return this.selectedRegion;
            }

            set
            {
                if (value == this.selectedRegion)
                {
                    return;
                }

                this.selectedRegion = value;
                this.OnPropertyChanged();
            }
        }

        public virtual string SelectedRealm
        {
            get
            {
                return this.selectedRealm;
            }

            set
            {
                if (value == this.selectedRealm)
                {
                    return;
                }

                this.selectedRealm = value;
                this.OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}