﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore
{
    using System;
    using System.Collections.Generic;

    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataModels;

    public interface IAuctionHouseSearchFilter
    {
        AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items);

        string Name { get; }

        string Control { get; }

        bool Enabled { get; set; }

        IAuctionHouseSearchFilter Clone();

        void Connect(Application app);

        void Disconnect();
    }
}