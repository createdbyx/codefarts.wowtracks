﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore.Models
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;
    using Codefarts.WowTracks.DataModels;

    public class ItemDisplayModel : INotifyPropertyChanged
    {
        private string iconUrl;

        private string region;

        private string name;

        private int itemId;

        private bool visible = true;

        public bool Visible
        {
            get
            {
                return this.visible;
            }

            set
            {
                if (value.Equals(this.visible))
                {
                    return;
                }

                this.visible = value;
                this.OnPropertyChanged();
            }
        }

        public ItemDisplayModel()
        {
        }

        public ItemDisplayModel(string region, ItemInfo item)
        {
            this.SetValue(region, item);
        }

        private void SetValue(string region, ItemInfo item)
        {
            this.name = item.name;
            this.region = region;
            this.itemId = item.id;
            this.iconUrl = string.Format("{0}{1}media.blizzard.com/wow/icons/56/{2}.jpg", Uri.UriSchemeHttp, Uri.SchemeDelimiter, item.icon);
        }

        public ItemDisplayModel(string region, AuctionDataModel item)
        {
            var contentManager = ContentManager.ContentManager<string>.Instance;
            var filePath = Path.Combine(contentManager.RootDirectory, region, "ItemIds", item.ItemId + ".json");
            if (File.Exists(filePath))
            {
                var itemData = contentManager.Load<ItemInfo>(filePath);
                this.SetValue(region, itemData);
            }
        }

        public int ItemId
        {
            get
            {
                return this.itemId;
            }

            set
            {
                if (value == this.itemId)
                {
                    return;
                }

                this.itemId = value;
                this.OnPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (value == this.name)
                {
                    return;
                }

                this.name = value;
                this.OnPropertyChanged();
            }
        }

        public string Region
        {
            get
            {
                return this.region;
            }

            set
            {
                if (value == this.region)
                {
                    return;
                }

                this.region = value;
                this.OnPropertyChanged();
            }
        }

        public string IconUrl
        {
            get
            {
                return this.iconUrl;
            }

            set
            {
                if (value == this.iconUrl)
                {
                    return;
                }

                this.iconUrl = value;
                this.OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
