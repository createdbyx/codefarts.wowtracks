﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Windows.Forms;

    public class TimeSpanColumn : DataGridViewColumn
    {
        public TimeSpanColumn()
            : base(new TimeSpanCell())
        {
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                // Ensure that the cell used for the template is a CalendarCell. 
                if (value != null && !value.GetType().IsAssignableFrom(typeof(TimeSpanCell)))
                {
                    throw new InvalidCastException("Must be a TimeSpanCell");
                }

                base.CellTemplate = value;
            }
        }
    }
}