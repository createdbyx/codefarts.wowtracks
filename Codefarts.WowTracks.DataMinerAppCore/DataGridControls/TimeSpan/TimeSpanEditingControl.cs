// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Windows.Forms;

    class TimeSpanEditingControl : TimeIntervalControl.TimeInterval, IDataGridViewEditingControl
    {
        DataGridView dataGridView;
        private bool valueChanged;
        int rowIndex;

        public TimeSpanEditingControl()
        {
            // this.Format = DateTimePickerFormat.Short;
        }

        // Implements the IDataGridViewEditingControl.EditingControlFormattedValue property. 
        public object EditingControlFormattedValue
        {
            get
            {
                return this.CurrentTimeSpan.ToString();
            }

            set
            {
                if (!(value is string))
                {
                    return;
                }

                try
                {
                    // This will throw an exception of the string is  
                    // null, empty, or not in the format of a date. 
                    this.CurrentTimeSpan = TimeSpan.Parse((string)value);
                }
                catch
                {
                    // In the case of an exception, just use the  
                    // default value so we're not left with a null 
                    // value. 
                    //this.CurrentTimeSpan = TimeSpan.TicksPerDay;
                }
            }
        }

        // Implements the  
        // IDataGridViewEditingControl.GetEditingControlFormattedValue method. 
        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        // Implements the  
        // IDataGridViewEditingControl.ApplyCellStyleToEditingControl method. 
        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;

            //this.Days = dataGridViewCellStyle.ForeColor;
            // this.CalendarMonthBackground = dataGridViewCellStyle.BackColor;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex  
        // property. 
        public int EditingControlRowIndex
        {
            get
            {
                return this.rowIndex;
            }

            set
            {
                this.rowIndex = value;
            }
        }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey  
        // method. 
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed. 
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return !dataGridViewWantsInputKey;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit  
        // method. 
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            // No preparation needs to be done.
            this.dataGridView.CurrentCell.OwningRow.Height = this.Size.Height;
            this.dataGridView.CurrentCell.OwningColumn.Width = this.Size.Width;
        }

        // Implements the IDataGridViewEditingControl.RepositionEditingControlOnValueChange property. 
        public bool RepositionEditingControlOnValueChange
        {
            get
            {
                return false;
            }
        }

        // Implements the IDataGridViewEditingControl.EditingControlDataGridView property. 
        public DataGridView EditingControlDataGridView
        {
            get
            {
                return this.dataGridView;
            }

            set
            {
                this.dataGridView = value;
            }
        }

        // Implements the IDataGridViewEditingControl.EditingControlValueChanged property. 
        public bool EditingControlValueChanged
        {
            get
            {
                return this.valueChanged;
            }

            set
            {
                this.valueChanged = value;
            }
        }

        // Implements the IDataGridViewEditingControl.EditingPanelCursor property. 
        public Cursor EditingPanelCursor
        {
            get
            {
                return base.Cursor;
            }
        }

        protected override void IntervalChangedNotifyUser()
        {
            // Notify the DataGridView that the contents of the cell have changed.
            this.valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.IntervalChangedNotifyUser();
        }
    }
}