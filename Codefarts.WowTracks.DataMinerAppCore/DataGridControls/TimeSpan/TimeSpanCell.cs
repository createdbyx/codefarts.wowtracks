// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Scheduler
{
    using System;
    using System.Windows.Forms;

    public class TimeSpanCell : DataGridViewTextBoxCell
    {
       
        public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value. 
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            var control = this.DataGridView.EditingControl as TimeSpanEditingControl;

            // Use the default row value when Value property is null. 
            if (this.Value == null)
            {
                control.CurrentTimeSpan = (TimeSpan)this.DefaultNewRowValue;
            }
            else
            {
                control.CurrentTimeSpan = TimeSpan.FromTicks((long)this.Value);
            }
        }

        public override Type EditType
        {
            get
            {
                // Return the type of the editing control that CalendarCell uses. 
                return typeof(TimeSpanEditingControl);
            }
        }

        public override Type ValueType
        {
            get
            {
                // Return the type of the value that CalendarCell contains.  
                return typeof(DateTime);
            }
        }

        public override object DefaultNewRowValue
        {
            get
            {
                // Use the current date and time as the default value. 
                return DateTime.Now;
            }
        }
    }
}