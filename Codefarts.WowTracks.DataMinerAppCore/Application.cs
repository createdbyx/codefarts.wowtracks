﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataModels;

    public class Application
    {
        protected int taskCount = 4;
       // private List<ItemDisplayModel> displayItemModels;

        public ObservableCollection<IAuctionHouseSearchFilter> RegisteredAuctionHouseFilters { get; set; }
        public ObservableCollection<IResultsPlugin> RegisteredResultPlugins { get; set; }
        public ObservableCollection<IAuctionHouseSearchFilter> AuctionHouseSearches { get; set; }
        // public ObservableCollection<AuctionHouseSearchGroup> AuctionHouseSearches { get; set; }
        // public ObservableDictionary<Type, IAuctionHouseFilterControl> AuctionHouseFilterControls { get; set; }
       // protected Dictionary<string, List<NameIdModel>> itemsTable = new Dictionary<string, List<NameIdModel>>();

        private bool isBuildingDisplayItems;
        private CancellationTokenSource backgroundCancel;

        private bool isAvailible;

        protected object isLocked = new object();

        public bool DisplayItemModelsAvailible()
        {
            return this.isAvailible;
        }

        public virtual void BrowseTo(string url)
        {
            
        }

        protected MainAuctionHouseFilterModel mainAuctionHouseFilterModel;

        public virtual MainAuctionHouseFilterModel MainAuctionHouseFilterModel
        {
            get
            {
                return this.mainAuctionHouseFilterModel;
            }

            set
            {
                this.mainAuctionHouseFilterModel = value;
            }
        }
        //public virtual List<ItemDisplayModel> DisplayItemModels
        //{
        //    get
        //    {
        //        //lock (this.isLocked)
        //        //{
        //        //    if (this.isAvailible)
        //        //    {
        //        return this.displayItemModels;//.ToArray();
        //        //}
        //        //}
        //    }
        //}

        //public virtual IDictionary<string, List<NameIdModel>> ItemNames
        //{
        //    get
        //    {
        //        return this.itemsTable;//.ToDictionary(x => x.Key, y => y.Value.ToList());
        //    }
        //}

        //public ReadOnlyCollection<ItemDisplayModel> DisplayItemModels
        //{
        //    get
        //    {
        //        lock (this.displayItemModels)
        //        {
        //            return new ReadOnlyCollection<ItemDisplayModel>(this.displayItemModels); 
        //        }
        //    }
        //}

        //public ReadOnlyDictionary<string, List<NameIdModel>> ItemNames
        //{
        //    get
        //    {
        //        lock (this.itemsTable)
        //        {
        //            return new ReadOnlyDictionary<string, List<NameIdModel>>(this.itemsTable); 
        //        }
        //    }
        //}

        public Application()
        {
            this.RegisteredAuctionHouseFilters = new ObservableCollection<IAuctionHouseSearchFilter>();
            this.AuctionHouseSearches = new ObservableCollection<IAuctionHouseSearchFilter>();
            this.mainAuctionHouseFilterModel = new MainAuctionHouseFilterModel();
            this.RegisteredResultPlugins=new ObservableCollection<IResultsPlugin>();
            //    this.displayItemModels = new List<ItemDisplayModel>();
            //   this.RegisteredAuctionHouseFilters.CollectionChanged += RegisteredAuctionHouseFilters_CollectionChanged;
        }

        /// <summary>Initializes this instance.</summary>
        public async virtual void Initialize()
        {
            // this.AuctionHouseFilterControls = new ObservableDictionary<Type, IAuctionHouseFilterControl>();

            // build list of item display models

           // await this.BuildDisplayItems();
        }

        protected virtual void CancelBuildDisplayItems()
        {
            var cancellationTokenSource = this.backgroundCancel;
            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();
            }
        }

        //protected async virtual Task BuildDisplayItems()
        //{
        //    await Task.Factory.StartNew(
        //        () =>
        //        {
        //            var names = this.ItemNames;
        //            if (names == null || this.isBuildingDisplayItems)
        //            {
        //                return;
        //            }

        //            this.isBuildingDisplayItems = true;
        //            this.backgroundCancel = new CancellationTokenSource();
        //            foreach (var region in names.Keys)
        //            {
        //                var tasks = new List<Task>();

        //                foreach (var listItem in names[region])
        //                {
        //                    tasks.Add(
        //                          Task.Factory.StartNew(
        //                            async x =>
        //                            {
        //                                var data = x as RegionNameIdModel;
        //                                await this.BuildDisplayItem(data.Model, data.Region);
        //                            },
        //                            new RegionNameIdModel() { Model = listItem, Region = region },
        //                            this.backgroundCancel.Token));

        //                    if (tasks.Count == this.taskCount)
        //                    {
        //                        Task.WaitAll(tasks.ToArray());
        //                        tasks.Clear();
        //                    }

        //                    if (this.backgroundCancel.IsCancellationRequested)
        //                    {
        //                        break;
        //                    }
        //                }

        //                Task.WaitAll(tasks.ToArray());

        //                if (this.backgroundCancel.IsCancellationRequested)
        //                {
        //                    break;
        //                }
        //            }

        //            //var oldList = new List<ItemDisplayModel>(this.displayItemModels);
        //            //this.displayItemModels.Clear();
        //            //this.displayItemModels.AddRange(oldList.Distinct(EqualityComparerCallback<ItemDisplayModel>.Compare((a, b) => a.Name == b.Name)));
        //            this.backgroundCancel = null;
        //            this.isBuildingDisplayItems = false;
        //        });
        //}

        //private class RegionNameIdModel
        //{
        //    public string Region { get; set; }
        //    public NameIdModel Model { get; set; }
        //}

        //private async Task BuildDisplayItem(NameIdModel model, string region)
        //{
        //    await Task.Factory.StartNew(
        //      () =>
        //      {
        //          // get item data 
        //          try
        //          {
        //              var itemData = ContentManager<string>.Instance.Load<ItemInfo>(Path.Combine(region, "ItemIds", model.Id + ".json"));
        //              var item = new ItemDisplayModel();
        //              item.ItemId = model.Id;
        //              item.Name = model.Name;
        //              item.Region = region;
        //              item.IconUrl = string.Format("{0}{1}media.blizzard.com/wow/icons/56/{2}.jpg", Uri.UriSchemeHttp, Uri.SchemeDelimiter, itemData.icon);
        //              //lock (this.displayItemModels)
        //              //{
        //              this.displayItemModels.Add(item);
        //              //}
        //          }
        //          catch (Exception)
        //          {
        //          }
        //      });
        //}

        //void RegisteredAuctionHouseFilters_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    switch (e.Action)
        //    {
        //        case NotifyCollectionChangedAction.Add:
        //            foreach (var item in e.NewItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                item.Connect(this);
        //            }

        //            break;

        //        case NotifyCollectionChangedAction.Remove:
        //            foreach (var item in e.OldItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                item.Disconnect();
        //            }

        //            break;

        //        case NotifyCollectionChangedAction.Replace:
        //            foreach (var item in e.OldItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                item.Disconnect();
        //            }

        //            foreach (var item in e.NewItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                item.Connect(this);
        //            }

        //            break;
        //    }
        //}
    }
}
