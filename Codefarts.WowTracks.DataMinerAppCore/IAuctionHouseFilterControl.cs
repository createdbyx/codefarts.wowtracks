﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore
{
    public interface IAuctionHouseFilterControl
    {
        void SetModel(IAuctionHouseSearchFilter model);

        void SetApplication(Application app);
    }
}