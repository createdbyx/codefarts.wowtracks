﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerAppCore
{
    using System;

    using Codefarts.WowTracks.DataModels;

    public interface IResultsPlugin
    {
        void UpdateData(AuctionDataModel[] dataModels, IResultsControl control);

        Guid UniqueId { get; }

        string Control { get; }
        
        string Name { get; }

        void Connect(Application app);

        void Disconnect();

        bool IsConnected { get; }
    }
}
