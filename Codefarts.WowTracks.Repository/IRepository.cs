﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.WowTracks.DataModels;

    public interface IRepository
    {
        Task TrackRealm(RealmTrackingModel model);

        Task StopTrackingRealm(RealmTrackingModel model);

        Task<bool> IsTrackingRealm(RealmTrackingModel model);

        Task UpdateRealm(RealmTrackingModel model);

        Task<IEnumerable<RealmTrackingModel>> GetRealmsBeingTracked(int page, int itemsPerPage);

        Task<int> GetNumberOfRealmsBeingTracked();

        Task<AuctionDataModel[]> GetAuctionHouseData(string region, string realmName, DateTime startDate, DateTime endDate, Action<float, string> callback, CancellationToken cancelToken);
    }
}
