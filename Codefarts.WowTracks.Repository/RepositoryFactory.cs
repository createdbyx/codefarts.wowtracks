﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Codefarts.WowTracks.DataModels;

    public class RepositoryFactory
    {
        private static RepositoryFactory instance;

        public IRepository Repository { get; set; }


        public static RepositoryFactory Instance
        {
            get
            {
                return instance ?? (instance = new RepositoryFactory());
            }
        }

        public static void UpdateRealm(RealmTrackingModel model)
        {
            Instance.Repository.UpdateRealm(model);
        }

        public static Task<IEnumerable<RealmTrackingModel>> GetRealmsBeingTracked(int page, int itemsPerPage)
        {
            return Instance.Repository.GetRealmsBeingTracked(page, itemsPerPage);
        }

        public static Task TrackRealm(RealmTrackingModel model)
        {
            return Instance.Repository.TrackRealm(model);
        }

        public static Task StopTrackingRealm(RealmTrackingModel model)
        {
            return Instance.Repository.StopTrackingRealm(model);
        }
    }
}