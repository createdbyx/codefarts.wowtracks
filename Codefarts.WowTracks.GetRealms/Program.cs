﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Realm
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml.Linq;

    using Codefarts.WowTracks.CommandLibrary;
    using Codefarts.WowTracks.DataModels;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    class Program
    {
        private static bool verbose = true;
        private static bool xmlOutput = false;

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Write("Expected at least 1 argument!");
                Environment.ExitCode = -1;
                return;
            }

            var parameters = new List<string>(args.Skip(1));
            GetSwitches(parameters);

            var command = args[0];
            switch (command.Trim().ToLower())
            {
                case "list":
                    ListCommand(parameters);
                    break;

                case "tracking":
                    ListTrackedRealms();
                    break;
            }
        }

        private static void ListTrackedRealms()
        {
            
        }

        private static void ListCommand(List<string> parameters)
        {
            if (parameters.Count < 1)
            {
                Write("Expected at least 1 argument!");
                Environment.ExitCode = -2;
                return;
            }

            if (string.Compare(parameters[0], "regions", StringComparison.OrdinalIgnoreCase) == 0)
            {
                var result = GetRegionsCommand.GetRegionInformation(new Dictionary<string, object> { { "Region", string.Empty } });
                RegionModel[] regions;
                result.ReturnValues.TryGetParameter("Regions", out regions);
                var lengths = new[]
                                  {
                                      regions.Max(x => x.Name.Length),
                                      regions.Max(x => x.FriendlyName.Length) + 1,
                                      regions.Max(x => x.Host.ToString().Length) + 1,
                                  };
                foreach (var model in regions)
                {
                    Write("{0," + lengths[0] + "}{1," + lengths[1] + "}{2," + lengths[2] + "}",
                        model.Name,
                        model.FriendlyName,
                        model.Host);
                }

                return;
            }
                
            try
            {
                var region = Region.AllRegions.FirstOrDefault(x => string.Compare(x.Name, parameters[0], StringComparison.OrdinalIgnoreCase) == 0);
                if (region == null)
                {
                    Write("Unknown Region!");
                    Environment.ExitCode = -2;
                    return;
                }

                // Init client
                var client = new WowClient(region, null, null, null);

                // Get realms
                var result = client.GetRealmStatusAsync().Result;

                if (xmlOutput)
                {
                    var realms = new XElement("realms", result.Realms.Select(r =>
                        new XElement("realm",
                            new XAttribute("name", r.Name),
                            new XAttribute("battlegroupname", r.BattleGroupName),
                            new XAttribute("ispvp", r.IsPvp),
                            new XAttribute("isrp", r.IsRP),
                            new XAttribute("locale", r.Locale),
                            new XAttribute("population", r.Population),
                            new XAttribute("queue", r.Queue),
                            new XAttribute("realmtype", r.RealmType),
                            new XAttribute("slug", r.Slug),
                            new XAttribute("status", r.Status),
                            new XAttribute("timezone", r.TimeZone),
                            new XElement("tolbarad",
                                new XAttribute("areaid", r.TolBarad.AreaId),
                                new XAttribute("controllingfaction", r.TolBarad.ControllingFaction),
                                new XAttribute("nextbattletimeutc", r.TolBarad.NextBattleTimeUtc),
                                new XAttribute("status", r.TolBarad.Status)),
                            new XElement("wintergrasp",
                                new XAttribute("areaid", r.TolBarad.AreaId),
                                new XAttribute("controllingfaction", r.TolBarad.ControllingFaction),
                                new XAttribute("nextbattletimeutc", r.TolBarad.NextBattleTimeUtc),
                                new XAttribute("status", r.TolBarad.Status))
                           )));
                    var doc = new XDocument(new XDeclaration("1.0", "UTF-8", "true"), realms);
                    doc.Save(Console.Out);
                }
                else
                {
                    var lengths = new[]
                                  {
                                      result.Realms.Max(x => Math.Max(x.Name.Length, 4)),
                                      result.Realms.Max(x => Math.Max(x.Status.ToString().Length, 6)) + 1,
                                      result.Realms.Max(x => Math.Max(x.Population.ToString().Length, 10) + 1) 
                                  };

                    var format = string.Empty;
                    for (int index = 0; index < lengths.Length; index++)
                    {
                        format += "{" + string.Format("{0},{1}", index, lengths[index]) + "}";
                    }

                    Write(format,
                        "Name",
                        "Status",
                        "Population");
                    Write(new string('-', 50));

                    foreach (var model in result.Realms)
                    {
                        Write(format,
                            model.Name,
                            model.Status,
                            model.Population);
                    }

                    Write("{0} realms listed.", result.Realms.Count);
                }
            }
            catch (ApiException ex)
            {
                Write(ex.Message);
                Environment.ExitCode = -3;
            }
        }

        private static void GetSwitches(List<string> parameters)
        {
            var index = parameters.IndexOf("-v");
            if (index != -1)
            {
                verbose = false;
                parameters.RemoveAt(index);
            }

            index = parameters.IndexOf("-xml");
            if (index != -1)
            {
                xmlOutput = true;
                parameters.RemoveAt(index);
            }
        }

        private static void Write(string text, params object[] args)
        {
            if (verbose)
            {
                Console.WriteLine(text, args);
            }
        }
    }
}
