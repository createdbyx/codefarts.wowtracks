﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMiner
{
    using UnityEditor;

    using UnityEngine;

    public interface IFilter
    {

    }

    public class MainGui : EditorWindow
    {

        [MenuItem("Window/Codefarts/Data Miner")]
        public static void Show()
        {
            var window = GetWindow<MainGui>();
            ((EditorWindow)window).Show();
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            GUILayout.BeginHorizontal();

            this.DrawFilters();

            this.DrawResults();

            GUILayout.EndHorizontal();
        }

        private void DrawResults()
        {
            if (GUILayout.Button("test"))
            {
            }
        }


        private void DrawFilters()
        {

        }
    }

}