﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServerCompact;

    using Codefarts.Settings;

    public class TrackerDbConfiguration : DbConfiguration
    {
        public TrackerDbConfiguration()
        {
            var folder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
            
            var sqlCeConnectionFactory = new SqlCeConnectionFactory(
                SqlCeProviderServices.ProviderInvariantName,
                folder,
                String.Empty);

            this.SetDefaultConnectionFactory(sqlCeConnectionFactory);
            this.SetProviderServices(SqlCeProviderServices.ProviderInvariantName, SqlCeProviderServices.Instance);
        }
    }
}