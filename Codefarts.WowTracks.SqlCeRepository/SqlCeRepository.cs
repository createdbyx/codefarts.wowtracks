﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SqlLiteRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.Settings;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.Repository;
    using Codefarts.WowTracks.TrackerApp;

    [Export(typeof(IRepository))]
    public class SqlCeRepository : IRepository
    {
        private string trackerFileName;

        public SqlCeRepository()
        {
            var folder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
            this.trackerFileName = Path.Combine(folder, "Trackers.sdf");
            //  this.trackerFileName = "Trackers.sdf";
        }

        public async Task TrackRealm(RealmTrackingModel model)
        {
            var context = new TrackerContext(trackerFileName);
            context.TrackingRealms.Add(model);
            await context.SaveChangesAsync();
        }

        public async Task StopTrackingRealm(RealmTrackingModel model)
        {
            var context = new TrackerContext(trackerFileName);
            var realm = await context.TrackingRealms.SingleOrDefaultAsync(x => x.Id == model.Id);
            if (realm != null)
            {
                context.TrackingRealms.Remove(realm);
                await context.SaveChangesAsync();
            }
        }

        public async Task<bool> IsTrackingRealm(RealmTrackingModel model)
        {
            var context = new TrackerContext(trackerFileName);
            return await context.TrackingRealms.AnyAsync(x => x.Id == model.Id || (x.Region == model.Region && x.Name == model.Name));
        }

        public async Task UpdateRealm(RealmTrackingModel model)
        {
            var context = new TrackerContext(trackerFileName);
            context.Entry(model).State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<RealmTrackingModel>> GetRealmsBeingTracked(int page, int itemsPerPage)
        {
            //return await Task<IEnumerable<RealmTrackingModel>>.Factory.StartNew(
            //    () =>
            //        {
            var context = new TrackerContext(trackerFileName);
            itemsPerPage = itemsPerPage < 1 ? 1 : itemsPerPage;
            itemsPerPage = itemsPerPage > 100 ? 100 : itemsPerPage;
            return await context.TrackingRealms.OrderBy(x => x.Id).Skip(page * itemsPerPage).Take(itemsPerPage).ToArrayAsync();
            // });
        }

        public async Task<int> GetNumberOfRealmsBeingTracked()
        {
            var context = new TrackerContext(trackerFileName);
            return await context.TrackingRealms.CountAsync();
        }

        public Task<AuctionDataModel[]> GetAuctionHouseData(string region, string realmName, DateTime startDate, DateTime endDate, Action<float, string> callback, CancellationToken cancelToken)
        {
            throw new NotImplementedException();
        }
    }
}
