﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.TrackerApp
{
    using System.Data.Entity;

    using Codefarts.WowTracks.DataModels;

    [DbConfigurationType(typeof(TrackerDbConfiguration))]
    public class TrackerContext : DbContext
    {
        public TrackerContext(string filename)
            : base(filename)
        {
        }

        public DbSet<RealmTrackingModel> TrackingRealms { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.LastAuctionHouseUpdateDate);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.LastAuctionHouseUpdateTime);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.LastUpdateDate);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.LastUpdateTime);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.StartedTrackingAuctionHouseDate);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.StartedTrackingAuctionHouseTime);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.StartedTrackingDate);
        //    modelBuilder.Entity<RealmTrackingModel>().Ignore(t => t.StartedTrackingTime);

        //    base.OnModelCreating(modelBuilder);
        //}
    }
}