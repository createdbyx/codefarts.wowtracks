﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.ConsoleTester
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServerCompact;

    using Codefarts.WowTracks.DataModels;

    public class RealmsTest
    {
        public class RealmsContext : DbContext
        {
            public RealmsContext(string filename)
                : base(filename)
            {
            }

            public DbSet<DiscoveredRealms> Realms { get; set; }
        }

        public class MyConfiguration : DbConfiguration
        {
            public MyConfiguration()
            {
                var sqlCeConnectionFactory = new SqlCeConnectionFactory(
                    SqlCeProviderServices.ProviderInvariantName,
                    Environment.CurrentDirectory,
                    string.Empty);
                this.SetDefaultConnectionFactory(sqlCeConnectionFactory);
                this.SetProviderServices(SqlCeProviderServices.ProviderInvariantName, SqlCeProviderServices.Instance);
            }
        }

        public static void Run(string[] args)
        {
            DbConfiguration.SetConfiguration(new MyConfiguration());
            Database.SetInitializer(new CreateDatabaseIfNotExists<RealmsContext>());

            var file = "test.sdf";
            using (var db = new RealmsContext(file))
            {
                //// Create and save a new Blog
                //Console.Write("Enter a name for a new person: ");
                //var name = Console.ReadLine();
                //Console.Write("Enter a name age: ");
                //var age = int.Parse(Console.ReadLine().Trim());

                //var item = new Person { Name = name, Age = age };
                //db.People.Add(item);
                //db.SaveChanges();

                //// Display all Blogs from the database
                //var query = from b in db.People
                //            orderby b.Name
                //            select b;

                //Console.WriteLine("All people in the database:");
                //foreach (var q in query)
                //{
                //    Console.WriteLine("Name: {0}   Age: {1}", q.Name, q.Age);
                //}

                //Console.WriteLine("Press any key to exit...");
                //Console.ReadKey();
            }
        }
    }
}