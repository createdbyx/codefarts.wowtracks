﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>                                

namespace Codefarts.WowTracks.ConsoleTester
{
    using System;
    using System.IO;

    using Codefarts.Settings;
    using Codefarts.Settings.Xml;

    class Program
    {
        static void Main(string[] args)
        {
            // setup settings
            var settingsFile = Path.Combine(Environment.CurrentDirectory, "settings.xml");
            SettingsManager.Instance.Values = new XmlDocumentLinqValues(settingsFile);


            RealmsTest.Run(args);
            //SQLCeCodeFirstTest.Run(args);
        }
    }
}
