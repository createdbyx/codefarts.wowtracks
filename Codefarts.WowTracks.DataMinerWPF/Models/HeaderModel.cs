// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;

    public class HeaderModel   :INotifyPropertyChanged
    {
        private string text;

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                if (value == this.text)
                {
                    return;
                }
                this.text = value;
                this.OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}