﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataModels;

    public class AuctionDataGridModel : AuctionDataModel
    {
        public AuctionDataGridModel()
        {
        }

        public AuctionDataGridModel(AuctionDataModel model, ItemDisplayModel displayModel)
        {
            this.AuctionId = model.AuctionId;
            this.BuyoutValue = model.BuyoutValue;
            this.CaptureDate = model.CaptureDate;
            this.CurrentBidValue = model.CurrentBidValue;
            this.Faction = model.Faction;
            this.ItemId = model.ItemId;
            this.OwnerName = model.OwnerName;
            this.OwnerRealm = model.OwnerRealm;
            this.PetBreedId = model.PetBreedId;
            this.PetLevel = model.PetLevel;
            this.PetQuality = model.PetQuality;
            this.PetSpeciesId = model.PetSpeciesId;
            this.Quantity = model.Quantity;
            this.Random = model.Random;
            this.Seed = model.Seed;
            this.TimeLeft = model.TimeLeft;
            this.DisplayModel = displayModel;
        }

        public ItemDisplayModel DisplayModel { get; set; }
    }
}
