﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    public enum Equivalency
    {
        EqualTo,
        GreaterThan,
        GreaterThenOrEqualTo,
        LessThan,
        LessThanOrEqualTo,
    }
}