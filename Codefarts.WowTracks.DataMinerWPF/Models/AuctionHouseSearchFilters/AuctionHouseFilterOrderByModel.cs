﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;
    using System.Linq;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterOrderByModel : AuctionHouseBaseFilterModel
    {
        private bool itemId = true;

        private bool timeLeft;

        private bool buyout;

        private bool currentBid;

        private bool owner;

        private bool auctionId;

        private bool captureDate;

        private bool faction;

        public bool Quantity
        {
            get
            {
                return this.quantity;
            }

            set
            {
                if (value.Equals(this.quantity))
                {
                    return;
                }

                this.quantity = value;
                this.OnPropertyChanged();
            }
        }

        private bool descending;

        private bool quantity;

        public bool Descending
        {
            get
            {
                return this.descending;
            }

            set
            {
                if (value.Equals(this.descending))
                {
                    return;
                }

                this.descending = value;
                this.OnPropertyChanged();
            }
        }

        public bool Faction
        {
            get
            {
                return this.faction;
            }

            set
            {
                if (value.Equals(this.faction))
                {
                    return;
                }

                this.faction = value;
                this.OnPropertyChanged();
            }
        }

        public bool CaptureDate
        {
            get
            {
                return this.captureDate;
            }

            set
            {
                if (value.Equals(this.captureDate))
                {
                    return;
                }

                this.captureDate = value;
                this.OnPropertyChanged();
            }
        }

        public bool AuctionId
        {
            get
            {
                return this.auctionId;
            }

            set
            {
                if (value.Equals(this.auctionId))
                {
                    return;
                }

                this.auctionId = value;
                this.OnPropertyChanged();
            }
        }

        public bool Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                if (value.Equals(this.owner))
                {
                    return;
                }

                this.owner = value;
                this.OnPropertyChanged();
            }
        }

        public bool CurrentBid
        {
            get
            {
                return this.currentBid;
            }

            set
            {
                if (value.Equals(this.currentBid))
                {
                    return;
                }

                this.currentBid = value;
                this.OnPropertyChanged();
            }
        }

        public bool Buyout
        {
            get
            {
                return this.buyout;
            }

            set
            {
                if (value.Equals(this.buyout))
                {
                    return;
                }

                this.buyout = value;
                this.OnPropertyChanged();
            }
        }

        public bool TimeLeft
        {
            get
            {
                return this.timeLeft;
            }

            set
            {
                if (value.Equals(this.timeLeft))
                {
                    return;
                }

                this.timeLeft = value;
                this.OnPropertyChanged();
            }
        }

        public bool ItemId
        {
            get
            {
                return this.itemId;
            }

            set
            {
                if (value.Equals(this.itemId))
                {
                    return;
                }

                this.itemId = value;
                this.OnPropertyChanged();
            }
        }

        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterOrderByModel()
                       {
                           CaptureDate = this.CaptureDate,
                           AuctionId = this.AuctionId,
                           Owner = this.Owner,
                           CurrentBid = this.CurrentBid,
                           Buyout = this.Buyout,
                           TimeLeft = this.TimeLeft,
                           ItemId = this.ItemId,
                       };
        }


        public override AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            if (this.CaptureDate)
            {
                return (this.descending ? items.OrderByDescending(x => x.CaptureDate) : items.OrderBy(x => x.CaptureDate)).AsParallel().ToArray();
            }

            if (this.AuctionId)
            {
                return (this.descending ? items.OrderByDescending(x => x.AuctionId) : items.OrderBy(x => x.AuctionId)).AsParallel().ToArray();
            }

            if (this.Owner)
            {
                return (this.descending ? items.OrderByDescending(x => x.OwnerName) : items.OrderBy(x => x.OwnerName)).AsParallel().ToArray();
            }

            if (this.CurrentBid)
            {
                return (this.descending ? items.OrderByDescending(x => x.CurrentBidValue) : items.OrderBy(x => x.CurrentBidValue)).AsParallel().ToArray();
            }

            if (this.Buyout)
            {
                return (this.descending ? items.OrderByDescending(x => x.BuyoutValue) : items.OrderBy(x => x.BuyoutValue)).AsParallel().ToArray();
            }

            if (this.TimeLeft)
            {
                return (this.descending ? items.OrderByDescending(x => x.TimeLeft) : items.OrderBy(x => x.TimeLeft)).AsParallel().ToArray();
            }

            if (this.ItemId)
            {
                return (this.descending ? items.OrderByDescending(x => x.ItemId) : items.OrderBy(x => x.ItemId)).AsParallel().ToArray();
            }

            if (this.Faction)
            {
                return (this.descending ? items.OrderByDescending(x => x.Faction) : items.OrderBy(x => x.Faction)).AsParallel().ToArray();
            }

            if (this.quantity)
            {
                return (this.descending ? items.OrderByDescending(x => x.Quantity) : items.OrderBy(x => x.Quantity)).AsParallel().ToArray();
            }

            return items;
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseOrderByControl).FullName;
            }
        }

        public override string Name
        {
            get
            {
                return "Order By";
            }
        }
    }
}