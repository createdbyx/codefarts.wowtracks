﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataModels;

    public class AuctionHouseBaseFilterModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        public virtual string Name { get; protected set; }

        public virtual string Control { get; protected set; }

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        public virtual IAuctionHouseSearchFilter Clone()
        {
            return null;
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {                                             
        }

        public virtual AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}