﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterTimeLeftModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private bool veryLong;

        private bool _long;

        private bool medium;

        private bool _short;

        private bool none;

        private bool enabled=true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }
        public bool None
        {
            get
            {
                return this.none;
            }
            set
            {
                if (value.Equals(this.none))
                {
                    return;
                }
                this.none = value;
                this.OnPropertyChanged();
            }
        }

        public bool Short
        {
            get
            {
                return this._short;
            }
            set
            {
                if (value.Equals(this._short))
                {
                    return;
                }
                this._short = value;
                this.OnPropertyChanged();
            }
        }

        public bool Medium
        {
            get
            {
                return this.medium;
            }
            set
            {
                if (value.Equals(this.medium))
                {
                    return;
                }
                this.medium = value;
                this.OnPropertyChanged();
            }
        }

        public bool Long
        {
            get
            {
                return this._long;
            }
            set
            {
                if (value.Equals(this._long))
                {
                    return;
                }
                this._long = value;
                this.OnPropertyChanged();
            }
        }

        public bool VeryLong
        {
            get
            {
                return this.veryLong;
            }
            set
            {
                if (value.Equals(this.veryLong))
                {
                    return;
                }
                this.veryLong = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                x =>
                {
                    if (x != null)
                    {
                        switch (x.TimeLeft)
                        {
                            case AuctionTimeLeft.None:
                                return this.none;

                            case AuctionTimeLeft.Short:
                                return this._short;

                            case AuctionTimeLeft.Medium:
                                return this.medium;

                            case AuctionTimeLeft.Long:
                                return this._long;

                            case AuctionTimeLeft.VeryLong:
                                return this.veryLong;
                        }
                    }

                    return false;
                }).AsParallel().ToArray();
        }

        public string Name
        {
            get
            {
                return "Time Left";
            }
        }

        public string Control
        {
            get
            {
                return typeof(AuctionHouseTimeLeftFilterControl).FullName;
            }
        }

      

        public IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterTimeLeftModel()
                        {
                            None = this.None,
                            Long = this.Long,
                            Medium = this.Medium,
                            Short = this.Short,
                            VeryLong = this.VeryLong
                        };
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
