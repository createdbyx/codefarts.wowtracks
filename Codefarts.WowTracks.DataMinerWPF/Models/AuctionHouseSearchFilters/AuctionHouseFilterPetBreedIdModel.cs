﻿namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterPetBreedIdModel : AuctionHouseFilterValueModel
    {
        public AuctionHouseFilterPetBreedIdModel()
        {
            this.ValueCallback = model => model.PetBreedId;
            this.Comparer = Equivalency.EqualTo;
        }

        public override string Name
        {
            get
            {
                return "Pet Breed Id";
            }
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseValueControl).FullName;
            }
        }
    }
}