﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterSeedModel : AuctionHouseFilterValueModel
    {
        public AuctionHouseFilterSeedModel()
        {
            this.ValueCallback = model => model.Seed;
            this.Comparer = Equivalency.EqualTo;
        }

        public override string Name
        {
            get
            {
                return "Seed";
            }
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseValueControl).FullName;
            }
        }
    }
}