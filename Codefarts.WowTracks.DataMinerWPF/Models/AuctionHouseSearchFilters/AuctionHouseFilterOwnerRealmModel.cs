﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterOwnerRealmModel : AuctionHouseFilterStringModel
    {
        public AuctionHouseFilterOwnerRealmModel()
        {
            this.ValueCallback = model => model.OwnerRealm;
        }

        public override string Name
        {
            get
            {
                return "Owner Realm";
            }
        }
       
        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterOwnerRealmModel()
            {
                Value = this.Value,
                ValueCallback = this.ValueCallback
            };
        }    
    }
}