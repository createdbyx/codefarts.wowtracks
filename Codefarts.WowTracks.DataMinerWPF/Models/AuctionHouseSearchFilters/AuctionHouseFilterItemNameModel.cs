﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.Logging;
    using Codefarts.Settings;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterItemNameModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private string value;

        private bool connected;

        private Application app;

        private ObservableCollection<ItemDisplayModel> displayItems = new ObservableCollection<ItemDisplayModel>();
        private CancellationTokenSource backgroundCancel;

        private bool buildingDisplayItems;

        private readonly object lockObject = new object();

        private ItemDisplayModel displayValue;

        internal class ItemEntry
        {
            public ItemInfo Item { get; set; }
            public ItemDisplayModel Display { get; set; }
        }

        private ObservableDictionary<string, List<ItemEntry>> itemData;

        private bool itemsLoaded;

        private bool itemsCurrentlyBeingLoaded;

        public bool ExactMatch { get; set; }
        public bool CaseSensitivity { get; set; }

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        //       private bool Handled { get; set; }
        public ItemDisplayModel DisplayValue
        {
            get
            {
                return this.displayValue;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                this.Value = value.Name;
                this.displayValue = value;
                this.OnPropertyChanged();
            }
        }

        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                //if (this.Handled)
                //{
                //    return;
                //}

                if (value == this.value)
                {
                    return;
                }

                this.value = value;
                //   this.Handled = true;
                this.OnPropertyChanged();
                var cancellationTokenSource = this.backgroundCancel;
                if (cancellationTokenSource != null)
                {
                    cancellationTokenSource.Cancel();
                }

                //   this.BuildDisplayItems();
                //  this.Handled = false;     
            }
        }

        //public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        //{
        //    if (!this.connected || string.IsNullOrWhiteSpace(this.value))
        //    {
        //        return items;
        //    }

        //    // get item id from it's name
        //    var list = this.app.ItemNames[model.SelectedRegion];
        //    var idMatches = list.Where(x => this.CompareName(x.Name)).Select(y => y.Id).AsParallel().ToArray();

        //    return items.Where(x => idMatches.Contains(x.ItemId)).AsParallel();
        //    //            return items.Where(x => idMatches.AsParallel().Any(y => y == x.ItemId));
        //}

        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            if (!this.connected || string.IsNullOrWhiteSpace(this.value))
            {
                return items;
            }

            // wait for items to be loaded
            var startTime = DateTime.Now;
            while (this.itemsCurrentlyBeingLoaded)
            {
                Thread.Sleep(1);
                if (DateTime.Now > startTime + TimeSpan.FromSeconds(30))
                {
                    throw new Exception("Timed out waiting for item table to finish being built.");
                }
            }

            this.BuildItemNamesTable();
            if (this.itemData.ContainsKey(model.SelectedRegion))
            {
                var list = this.itemData[model.SelectedRegion];
                var idMatches = list.Where(x => this.CompareName(x.Item.name)).Select(y => y.Item.id).AsParallel().ToArray();
                return items.Where(x => idMatches.Contains(x.ItemId)).AsParallel().ToArray();
            }

            return items;
        }

        private async Task BuildItemNamesTable()
        {
            if (!this.connected || this.itemsLoaded || this.itemsCurrentlyBeingLoaded)
            {
                return;
            }

            this.itemsCurrentlyBeingLoaded = true;
            await Task.Factory.StartNew(
                 () =>
                 {
                     var settings = SettingsManager.Instance;
                     var dataFolder = settings.GetSetting(SettingConstants.DataFolder, string.Empty);
                     this.itemData = new ObservableDictionary<string, List<ItemEntry>>();
                     foreach (var region in WOWSharp.Community.Region.AllRegions)
                     {
                         var itemPath = Path.Combine(dataFolder, region.Name, "ItemIds");
                         if (!Directory.Exists(itemPath))
                         {
                             continue;
                         }

                         var files = Directory.GetFiles(itemPath, "*.json");

                         this.itemData.Add(region.Name, new List<ItemEntry>());

                         var startTime = DateTime.Now;
                         for (var index = 0; index < files.Length; index++)
                         {
                             var file = files[index];
                             try
                             {
                                 var item = ContentManager.ContentManager<string>.Instance.Load<ItemInfo>(file);
                                 this.itemData[region.Name].Add(new ItemEntry() { Item = item, Display = new ItemDisplayModel(region.Name, item) });
                             }
                             catch (Exception ex)
                             {
                                 Logging.Log(LogEntryType.Error, ex.Message, "LoadingItemError");
                             }

                             if (DateTime.Now <= startTime + TimeSpan.FromSeconds(2))
                             {
                                 continue;
                             }

                             Logging.Log(LogEntryType.Information, string.Format("Region {0} {1}/{2} completed", region.Name, index, files.Length), "RebuildItemsTable");
                             startTime = DateTime.Now;
                         }

                         this.itemData[region.Name] = this.itemData[region.Name].OrderBy(x => x.Item.name).ToList();

                         Logging.Log(
                             LogEntryType.Information,
                             string.Format("Loaded {0} item data set into items table for region {1}", itemData[region.Name].Count, region.Name),
                             "RebuildItemsTable");
                     }


                     this.itemsLoaded = true;
                     this.itemsCurrentlyBeingLoaded = false;
                     this.UpdateDisplayItems();
                 });
        }

        private void UpdateDisplayItems()
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                () =>
                {
                    var selectedRegion = this.app.MainAuctionHouseFilterModel.SelectedRegion;
                    this.displayItems.Clear();
                    //var models =
                    //    this.itemData[selectedRegion].AsParallel()
                    //        .Select(x => new ItemDisplayModel(selectedRegion, x.Item))
                    //        .ToArray();
                    foreach (var model in this.itemData[selectedRegion])
                    {
                        this.displayItems.Add(model.Display);
                    }
                });

            //this.OnPropertyChanged("ItemDisplayModels");
            // this.BuildDisplayItems();
        }

        private bool CompareName(string name)
        {
            if (this.ExactMatch)
            {
                return string.Compare(name, this.value, this.CaseSensitivity ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) == 0;
            }

            return name.IndexOf(this.value, this.CaseSensitivity ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) != -1;
        }

        public ObservableCollection<ItemDisplayModel> ItemDisplayModels
        {
            get
            {
                return this.displayItems;
            }
        }

        public async void BuildDisplayItems()
        {
            if (this.buildingDisplayItems || this.itemsCurrentlyBeingLoaded)
            {
                return;
            }

            //if (!this.connected)
            //{
            //    this.displayItems = null;
            //}

            var cancellationTokenSource = this.backgroundCancel;
            if (cancellationTokenSource != null && cancellationTokenSource.IsCancellationRequested)
            {
                if (!this.itemsCurrentlyBeingLoaded)
                {
                    this.backgroundCancel = null;
                }
                else
                {
                    return;
                }
            }

            await this.BuildItemNamesTable();

            //  return this.GetDisplayItems();     
            this.buildingDisplayItems = true;
            this.backgroundCancel = new CancellationTokenSource();
            //  this.OnPropertyChanged("ItemDisplayModels");
            await Task.Factory.StartNew(
                () =>
                {
                    var regionName = this.app.MainAuctionHouseFilterModel.SelectedRegion;

                    //  var originalItems = this.displayItems;
                    //System.Windows.Application.Current.Dispatcher.InvokeAsync(
                    //    () =>
                    //    {
                    //        this.displayItems.Clear();
                    //        this.displayItems.Add(new ItemDisplayModel() { Name = "Rebuilding list..." });
                    //    });
                    //  this.OnPropertyChanged("ItemDisplayModels");
                    //  var correctedSearchValue = string.IsNullOrWhiteSpace(this.value) ? string.Empty : this.value.Trim();
                    //lock (this.lockObject)
                    //{
                    //lock (this.app.DisplayItemModels)
                    //{
                    // if (this.itemData.ContainsKey(regionName))
                    // {
                    //    System.Windows.Application.Current.Dispatcher.InvokeAsync(
                    //        () =>
                    //        {
                    //            this.displayItems.Clear();
                    //            this.displayItems.Add(new ItemDisplayModel() { Name = "No items for region..." });
                    //        });
                    //}
                    //else
                    //{
                    List<ItemEntry> list;
                    if (this.itemData.TryGetValue(regionName, out list))
                    {
                        //System.Windows.Application.Current.Dispatcher.InvokeAsync(
                        //    () =>
                        //    {

                        var correctedSearchValue = string.IsNullOrWhiteSpace(this.value) ? string.Empty : this.value.Trim();
                        var visibleCount = 0;
                        foreach (var item in list)
                        {
                            item.Display.Visible = visibleCount < 100 && (string.IsNullOrWhiteSpace(correctedSearchValue) ||
                                                                          (!string.IsNullOrWhiteSpace(item.Item.name) &&
                                                                           this.CompareName(item.Item.name)));
                            visibleCount += item.Display.Visible ? 1 : 0;
                        }

                        // });
                    }

                    this.buildingDisplayItems = false;
                    // this.OnPropertyChanged("ItemDisplayModels");
                },
                this.backgroundCancel.Token);
        }


        //public async void BuildDisplayItemsOld()
        //{
        //    if (this.buildingDisplayItems || this.itemsCurrentlyBeingLoaded)
        //    {
        //        return;
        //    }

        //    //if (!this.connected)
        //    //{
        //    //    this.displayItems = null;
        //    //}

        //    var cancellationTokenSource = this.backgroundCancel;
        //    if (cancellationTokenSource != null && cancellationTokenSource.IsCancellationRequested)
        //    {
        //        if (!this.itemsCurrentlyBeingLoaded)
        //        {
        //            this.backgroundCancel = null;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }

        //    await this.BuildItemNamesTable();
        //    //task.Wait((int)TimeSpan.FromMinutes(2).TotalMilliseconds);


        //    //  return this.GetDisplayItems();     
        //    this.buildingDisplayItems = true;
        //    this.backgroundCancel = new CancellationTokenSource();
        //    this.OnPropertyChanged("ItemDisplayModels");
        //    await Task.Factory.StartNew(
        //        () =>
        //        {
        //            //  var originalItems = this.displayItems;
        //            System.Windows.Application.Current.Dispatcher.InvokeAsync(
        //                () =>
        //                {
        //                    this.displayItems.Clear();
        //                    this.displayItems.Add(new ItemDisplayModel() { Name = "Rebuilding list..." });
        //                });
        //            this.OnPropertyChanged("ItemDisplayModels");
        //            var correctedSearchValue = string.IsNullOrWhiteSpace(this.value) ? string.Empty : this.value.Trim();
        //            //lock (this.lockObject)
        //            //{
        //            //lock (this.app.DisplayItemModels)
        //            //{
        //            var regionName = this.app.MainAuctionHouseFilterModel.SelectedRegion;
        //            if (!this.itemData.ContainsKey(regionName))
        //            {
        //                System.Windows.Application.Current.Dispatcher.InvokeAsync(
        //                    () =>
        //                    {
        //                        this.displayItems.Clear();
        //                        this.displayItems.Add(new ItemDisplayModel() { Name = "No items for region..." });
        //                    });
        //            }
        //            else
        //            {
        //                var list = this.itemData[regionName];
        //                System.Windows.Application.Current.Dispatcher.InvokeAsync(
        //                    () =>
        //                    {
        //                        this.displayItems.Clear();
        //                        var data =
        //                            (string.IsNullOrWhiteSpace(correctedSearchValue)
        //                                 ? list
        //                                 : list.Where(x => !string.IsNullOrWhiteSpace(x.name) && this.CompareName(x.name)))
        //                            // .Distinct(Settings.Xml.EqualityComparerCallback<ItemDisplayModel>.Compare((a, b) => a.Name == b.Name))
        //                                .AsParallel().Take(100).Select(x => new ItemDisplayModel(regionName, x)).ToArray();
        //                        foreach (var model in data)
        //                        {
        //                            this.displayItems.Add(model);
        //                        }
        //                    });
        //            }

        //            //if (string.IsNullOrWhiteSpace(this.value))
        //            //{
        //            //    this.displayItems = this.app.DisplayItemModels.Take(25).AsParallel().ToArray();
        //            //}
        //            //else
        //            //{
        //            //    var list = new List<ItemDisplayModel>();
        //            //    foreach (var model in this.app.DisplayItemModels.AsParallel())
        //            //    {
        //            //        if (model.Name.ToLowerInvariant().Contains(correctedSearchValue))
        //            //        {
        //            //            list.Add(model);
        //            //        }

        //            //        if (this.backgroundCancel.IsCancellationRequested)
        //            //        {
        //            //            this.buildingDisplayItems = false;
        //            //            this.displayItems = originalItems;
        //            //            this.OnPropertyChanged("ItemDisplayModels");
        //            //            return;
        //            //        }

        //            //        if (list.Count == 25)
        //            //        {
        //            //            break;
        //            //        }
        //            //    }

        //            //    this.displayItems = list.ToArray();
        //            //}

        //            //}

        //            this.buildingDisplayItems = false;
        //            this.OnPropertyChanged("ItemDisplayModels");
        //        },
        //        this.backgroundCancel.Token);
        //}

        public string Name
        {
            get
            {
                return "Item Name";
            }
        }

        public string Control
        {
            get
            {
                return typeof(AuctionHouseItemNameControl).FullName;
            }
        }

        public virtual IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterItemNameModel()
                       {
                           Value = this.Value
                       };
        }

        public void Connect(Application app)
        {
            if (this.connected)
            {
                return;
            }

            this.app = app;
            app.MainAuctionHouseFilterModel.PropertyChanged += this.OnMainAuctionHouseFilterModelOnPropertyChanged;
            this.displayItems.Clear();
            this.displayItems.Add(new ItemDisplayModel() { Name = "Building list..." });
            this.OnPropertyChanged("ItemDisplayModels");
            //this.BuildDisplayItems();
            this.connected = true;
            this.BuildItemNamesTable();
        }

        private void OnMainAuctionHouseFilterModelOnPropertyChanged(object s, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedRegion")
            {
                this.UpdateDisplayItems();
            }
        }

        public void Disconnect()
        {
            if (!this.connected)
            {
                return;
            }

            this.app.MainAuctionHouseFilterModel.PropertyChanged -= this.OnMainAuctionHouseFilterModelOnPropertyChanged;
            this.app = null;
            this.connected = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}