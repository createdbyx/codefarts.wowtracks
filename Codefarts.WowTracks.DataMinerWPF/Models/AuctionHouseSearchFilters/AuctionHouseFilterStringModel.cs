﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    public class AuctionHouseFilterStringModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private string value;

        private bool enabled=true;

        public bool ExactMatch { get; set; }
        public bool CaseSensitivity { get; set; }

        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (value == this.value)
                {
                    return;
                }
                this.value = value;
                this.OnPropertyChanged();
            }
        }

        public virtual Func<AuctionDataModel, string> ValueCallback { get; set; }


        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(x => this.value != null && this.ValueCallback != null && this.CompareName(this.ValueCallback(x))).AsParallel().ToArray();
        }

        private bool CompareName(string name)

        {
            if (this.ExactMatch)
            {
                return string.Compare(name, this.value, this.CaseSensitivity ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) == 0;
            }

            return name.IndexOf(this.value, this.CaseSensitivity ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) != -1;
        }

        public virtual string Name { get; private set; }

        public virtual string Control
        {
            get
            {
                return typeof(AuctionHouseStringControl).FullName;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        public virtual IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterStringModel()
                       {
                           Value = this.Value,
                           ValueCallback = this.ValueCallback
                       };
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}