﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataModels;

    public class AuctionHouseFilterValueModel : AuctionHouseBaseFilterModel
    {
        private bool rangeCheck;

        private Equivalency comparer;

        private long maxValue;

        private long minValue;

        private long value;

        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterValueModel()
                        {
                            Comparer = this.Comparer,
                            MaxValue = this.MaxValue,
                            MinValue = this.MinValue,
                            RangeCheck = this.RangeCheck,
                            Value = this.Value,
                            ValueCallback = this.ValueCallback,
                            Control = this.Control,
                            Name = this.Name
                        };
        }

        public virtual long Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (value == this.value)
                {
                    return;
                }

                this.value = value;
                this.OnPropertyChanged();
            }
        }

        public virtual long MinValue
        {
            get
            {
                return this.minValue;
            }
            set
            {
                if (value == this.minValue)
                {
                    return;
                }
                this.minValue = value;
                this.OnPropertyChanged();
            }
        }

        public virtual long MaxValue
        {
            get
            {
                return this.maxValue;
            }
            set
            {
                if (value == this.maxValue)
                {
                    return;
                }
                this.maxValue = value;
                this.OnPropertyChanged();
            }
        }

        public virtual Equivalency Comparer
        {
            get
            {
                return this.comparer;
            }
            set
            {
                if (value == this.comparer)
                {
                    return;
                }
                this.comparer = value;
                this.OnPropertyChanged();
            }
        }

        public virtual Func<AuctionDataModel, long> ValueCallback { get; set; }

        public virtual bool RangeCheck
        {
            get
            {
                return this.rangeCheck;
            }
            set
            {
                if (value.Equals(this.rangeCheck))
                {
                    return;
                }
                this.rangeCheck = value;
                this.OnPropertyChanged();
            }
        }

        public override AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                x =>
                {
                    if (this.RangeCheck)
                    {
                        var tempValue = this.ValueCallback(x);
                        return tempValue >= this.MinValue && tempValue <= this.MaxValue;
                    }

                    switch (this.Comparer)
                    {
                        case Equivalency.EqualTo:
                            return this.ValueCallback(x) == this.Value;

                        case Equivalency.GreaterThan:
                            return this.ValueCallback(x) > this.Value;

                        case Equivalency.GreaterThenOrEqualTo:
                            return this.ValueCallback(x) >= this.Value;

                        case Equivalency.LessThan:
                            return this.ValueCallback(x) < this.Value;

                        case Equivalency.LessThanOrEqualTo:
                            return this.ValueCallback(x) <= this.Value;
                    }

                    return false;
                }).AsParallel().ToArray();
        }


    }
}