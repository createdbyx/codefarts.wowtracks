﻿namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterPetLevelModel : AuctionHouseFilterValueModel
    {
        public AuctionHouseFilterPetLevelModel()
        {
            this.ValueCallback = model => model.PetLevel;
            this.Comparer = Equivalency.EqualTo;
        }

        public override string Name
        {
            get
            {
                return "Pet Level";
            }
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseValueControl).FullName;
            }
        }
    }
}