﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System.ComponentModel.Composition;
    using System.Linq;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterDistinctAuctionsModel : AuctionHouseBaseFilterModel
    {
        private bool lastInstance;

        public bool LastInstance
        {
            get
            {
                return this.lastInstance;
            }

            set
            {
                if (value.Equals(this.lastInstance))
                {
                    return;
                }

                this.lastInstance = value;
                this.OnPropertyChanged();
            }
        }

        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterDistinctAuctionsModel()
                       {
                           LastInstance = this.LastInstance
                       };
        }

        public override AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.GroupBy(a => a.AuctionId).Select(g => this.LastInstance ? g.Last() : g.First()).ToArray();
          //  return items.GroupBy(a => a.AuctionId).Select(g => this.LastInstance ? g.Last() : g.First()).GroupBy(a => a.CaptureDate).Select(x => x.First()).ToArray();
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseDistinctAuctionsControl).FullName;
            }
        }

        public override string Name
        {
            get
            {
                return "Distinct Auctions";
            }
        }
    }
}