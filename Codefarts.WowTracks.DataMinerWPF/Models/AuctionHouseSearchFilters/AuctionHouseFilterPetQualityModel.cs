﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterPetQualityModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private bool heirloom;

        private bool artifact;

        private bool legendary;

        private bool epic;

        private bool rare;

        private bool uncommon;

        private bool common;

        private bool poor;

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool Poor
        {
            get
            {
                return this.poor;
            }
            set
            {
                if (value.Equals(this.poor))
                {
                    return;
                }
                this.poor = value;
                this.OnPropertyChanged();
            }
        }

        public bool Common
        {
            get
            {
                return this.common;
            }
            set
            {
                if (value.Equals(this.common))
                {
                    return;
                }
                this.common = value;
                this.OnPropertyChanged();
            }
        }

        public bool Uncommon
        {
            get
            {
                return this.uncommon;
            }
            set
            {
                if (value.Equals(this.uncommon))
                {
                    return;
                }
                this.uncommon = value;
                this.OnPropertyChanged();
            }
        }

        public bool Rare
        {
            get
            {
                return this.rare;
            }
            set
            {
                if (value.Equals(this.rare))
                {
                    return;
                }
                this.rare = value;
                this.OnPropertyChanged();
            }
        }

        public bool Epic
        {
            get
            {
                return this.epic;
            }
            set
            {
                if (value.Equals(this.epic))
                {
                    return;
                }
                this.epic = value;
                this.OnPropertyChanged();
            }
        }

        public bool Legendary
        {
            get
            {
                return this.legendary;
            }
            set
            {
                if (value.Equals(this.legendary))
                {
                    return;
                }
                this.legendary = value;
                this.OnPropertyChanged();
            }
        }

        public bool Artifact
        {
            get
            {
                return this.artifact;
            }
            set
            {
                if (value.Equals(this.artifact))
                {
                    return;
                }
                this.artifact = value;
                this.OnPropertyChanged();
            }
        }

        public bool Heirloom
        {
            get
            {
                return this.heirloom;
            }
            set
            {
                if (value.Equals(this.heirloom))
                {
                    return;
                }
                this.heirloom = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                x =>
                {
                    if (x != null)
                    {
                        switch (x.PetQuality)
                        {
                            case ItemQuality.Poor:
                                return this.poor;

                            case ItemQuality.Common:
                                return this.common;

                            case ItemQuality.Uncommon:
                                return this.uncommon;

                            case ItemQuality.Rare:
                                return this.rare;

                            case ItemQuality.Epic:
                                return this.epic;

                            case ItemQuality.Legendary:
                                return this.legendary;

                            case ItemQuality.Artifact:
                                return this.artifact;

                            case ItemQuality.Heirloom:
                                return this.heirloom;

                        }
                    }

                    return false;
                }).AsParallel().ToArray();
        }

        public string Name
        {
            get
            {
                return "Pet Quality";
            }
        }

        public string Control
        {
            get
            {
                return typeof(AuctionHouseQualityControl).FullName;
            }
        }

        public IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterPetQualityModel()
                        {
                            Artifact = this.Artifact,
                            Common = this.Common,
                            Epic = this.Epic,
                            Heirloom = this.Heirloom,
                            Legendary = this.Legendary,
                            Poor = this.Poor,
                            Rare = this.Rare,
                            Uncommon = this.Uncommon
                        };
        }

        public void Connect(Application app)
        {
             }

        public void Disconnect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}