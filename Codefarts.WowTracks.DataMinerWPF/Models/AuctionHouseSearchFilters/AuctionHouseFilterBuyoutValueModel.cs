﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;
    using System.Linq;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterBuyoutValueModel : AuctionHouseFilterValueModel
    {
        private bool hasBuyout;

        public bool HasBuyout
        {
            get
            {
                return this.hasBuyout;
            }

            set
            {
                if (value.Equals(this.hasBuyout))
                {
                    return;
                }

                this.hasBuyout = value;
                this.OnPropertyChanged();
            }
        }

        public override AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                x =>
                {
                    if (!this.HasBuyout && !x.BuyoutValue.HasValue)
                    {
                        return false;
                    }

                    if (this.RangeCheck)
                    {
                        var tempValue = this.ValueCallback(x);
                        return tempValue >= this.MinValue && tempValue <= this.MaxValue;
                    }

                    switch (this.Comparer)
                    {
                        case Equivalency.EqualTo:
                            return this.ValueCallback(x) == this.Value;

                        case Equivalency.GreaterThan:
                            return this.ValueCallback(x) > this.Value;

                        case Equivalency.GreaterThenOrEqualTo:
                            return this.ValueCallback(x) >= this.Value;

                        case Equivalency.LessThan:
                            return this.ValueCallback(x) < this.Value;

                        case Equivalency.LessThanOrEqualTo:
                            return this.ValueCallback(x) <= this.Value;
                    }

                    return false;
                }).AsParallel().ToArray();
        }

        public AuctionHouseFilterBuyoutValueModel()
        {
            // ERROR: the following line of code is probably in error
            this.ValueCallback = model => model.BuyoutValue.HasValue ? model.BuyoutValue.Value : 0;
            this.Comparer = Equivalency.EqualTo;
        }

        public override string Name
        {
            get
            {
                return "Buyout Value";
            }
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseBuyoutControl).FullName;
            }
        }
    }
}