﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System.ComponentModel.Composition;
    using System.Linq;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterTakeModel : AuctionHouseBaseFilterModel
    {
        private bool fromEnd;

        private int count;

        public int Count
        {
            get
            {
                return this.count;
            }

            set
            {
                if (value == this.count)
                {
                    return;
                }

                this.count = value;
                this.OnPropertyChanged();
            }
        }

        public bool FromEnd
        {
            get
            {
                return this.fromEnd;
            }

            set
            {
                if (value.Equals(this.fromEnd))
                {
                    return;
                }

                this.fromEnd = value;
                this.OnPropertyChanged();
            }
        }

        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterTakeModel()
                       {
                           Count = this.Count,
                           FromEnd = this.FromEnd
                       };
        }


        public override AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            if (this.FromEnd)
            {
                var listCount = items.Count();
                var index = listCount - this.count;

                return (index < 0 ? items : items.Skip(index)).Take(this.Count).ToArray();
            }

            return items.Take(this.Count).ToArray();
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseTakeControl).FullName;
            }
        }

        public override string Name
        {
            get
            {
                return "Take";
            }
        }
    }
}