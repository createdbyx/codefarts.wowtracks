﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterQuantityModel : AuctionHouseFilterValueModel
    {
        public AuctionHouseFilterQuantityModel()
        {
            this.ValueCallback = model => model.Quantity;
            this.Comparer = Equivalency.EqualTo;
        }

        public override string Name
        {
            get
            {
                return "Quantity";
            }
        }

        public override string Control
        {
            get
            {
                return typeof(AuctionHouseValueControl).FullName;
            }
        }
    }
}