﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.ComponentModel.Composition;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterOwnerNameModel : AuctionHouseFilterStringModel
    {
        public AuctionHouseFilterOwnerNameModel()
        {
            this.ValueCallback = model => model.OwnerName;
        }
        
        public override string Name
        {
            get
            {
                return "Owner Name";
            }
        }
        public override IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterOwnerNameModel()
            {
                Value = this.Value,
                ValueCallback = this.ValueCallback
            };
        }      
    }
}