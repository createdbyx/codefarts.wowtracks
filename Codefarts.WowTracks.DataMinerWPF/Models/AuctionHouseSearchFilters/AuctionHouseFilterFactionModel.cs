﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterFactionModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private bool neutral;

        private bool horde;

        private bool alliance;

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;                          
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        public bool Alliance
        {
            get
            {
                return this.alliance;
            }
            set
            {
                if (value.Equals(this.alliance))
                {
                    return;
                }
                this.alliance = value;
                this.OnPropertyChanged();
            }
        }

        public bool Horde
        {
            get
            {
                return this.horde;
            }
            set
            {
                if (value.Equals(this.horde))
                {
                    return;
                }
                this.horde = value;
                this.OnPropertyChanged();
            }
        }

        public bool Neutral
        {
            get
            {
                return this.neutral;
            }
            set
            {
                if (value.Equals(this.neutral))
                {
                    return;
                }
                this.neutral = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                    x =>
                    {
                        if (x != null)
                        {
                            switch (x.Faction)
                            {
                                case Faction.Neutral:
                                    return this.Neutral;

                                case Faction.Alliance:
                                    return this.alliance;

                                case Faction.Horde:
                                    return this.horde;
                            }
                        }

                        return false;
                    }).AsParallel().ToArray();
        }

        public string Name
        {
            get
            {
                return "Faction";
            }
        }

        public string Control
        {
            get
            {
                return typeof(AuctionHouseFactionControl).FullName;
            }
        }

        public IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterFactionModel()
                        {
                            Alliance = this.Alliance,
                            Horde = this.Horde,
                            Neutral = this.Neutral
                        };
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}