﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters
{
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.Logging;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Annotations;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataModels;

    [Export(typeof(IAuctionHouseSearchFilter))]
    public class AuctionHouseFilterSourceTypeModel : IAuctionHouseSearchFilter, INotifyPropertyChanged
    {
        private bool questReward;

        private bool none;

        private bool gameObjectDrop;

        private bool factionReward;

        private bool creatureDrop;

        private bool crafted;

        private bool achievement;

        private bool enabled = true;
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                if (value.Equals(this.enabled))
                {
                    return;
                }
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool Achievement
        {
            get
            {
                return this.achievement;
            }

            set
            {
                if (value.Equals(this.achievement))
                {
                    return;
                }

                this.achievement = value;
                this.OnPropertyChanged();
            }
        }

        public bool Crafted
        {
            get
            {
                return this.crafted;
            }

            set
            {
                if (value.Equals(this.crafted))
                {
                    return;
                }

                this.crafted = value;
                this.OnPropertyChanged();
            }
        }

        public bool CreatureDrop
        {
            get
            {
                return this.creatureDrop;
            }

            set
            {
                if (value.Equals(this.creatureDrop))
                {
                    return;
                }
                this.creatureDrop = value;
                this.OnPropertyChanged();
            }
        }

        public bool FactionReward
        {
            get
            {
                return this.factionReward;
            }

            set
            {
                if (value.Equals(this.factionReward))
                {
                    return;
                }

                this.factionReward = value;
                this.OnPropertyChanged();
            }
        }

        public bool GameObjectDrop
        {
            get
            {
                return this.gameObjectDrop;
            }

            set
            {
                if (value.Equals(this.gameObjectDrop))
                {
                    return;
                }

                this.gameObjectDrop = value;
                this.OnPropertyChanged();
            }
        }

        public bool None
        {
            get
            {
                return this.none;
            }

            set
            {
                if (value.Equals(this.none))
                {
                    return;
                }

                this.none = value;
                this.OnPropertyChanged();
            }
        }

        public bool QuestReward
        {
            get
            {
                return this.questReward;
            }

            set
            {
                if (value.Equals(this.questReward))
                {
                    return;
                }

                this.questReward = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] Filter(MainFilterModel model, AuctionDataModel[] items)
        {
            return items.Where(
                x =>
                {
                    try
                    {
                        var itemInfo = ContentManager.ContentManager<string>.Instance.Load<ItemInfo>(Path.Combine(model.SelectedRegion, "ItemIds", x.ItemId + ".json"));
                        switch (itemInfo.itemSource.sourceType)
                        {
                            case "NONE":
                                return this.None;

                            case "CREATURE_DROP":
                                return this.CreatureDrop;

                            case "GAME_OBJECT_DROP":
                                return this.GameObjectDrop;

                            case "REWARD_FOR_QUEST":
                                return this.QuestReward;

                            case "FACTION_REWARD":
                                return this.FactionReward;

                            case "CREATED_BY_SPELL":
                                return this.Crafted;

                            case "ACHIEVEMENT_REWARD":
                                return this.Achievement;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Logging.Log(LogEntryType.Error, string.Format("Itemid: {0}\r\n{1}", x.ItemId, ex.Message), "SourceTypeFilter");
                    }

                    return false;
                }).AsParallel().ToArray();
        }

        public string Name
        {
            get
            {
                return "Source Type";
            }
        }

        public string Control
        {
            get
            {
                return typeof(AuctionHouseSourceTypeControl).FullName;
            }
        }

        public IAuctionHouseSearchFilter Clone()
        {
            return new AuctionHouseFilterSourceTypeModel()
                       {
                          Achievement = this.Achievement,
                          Crafted = this.Crafted,
                          CreatureDrop = this.CreatureDrop,
                          FactionReward = this.FactionReward,
                          GameObjectDrop = this.GameObjectDrop,
                          None = this.None,
                          QuestReward = this.QuestReward
                       };
        }

        public void Connect(Application app)
        {
        }

        public void Disconnect()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}