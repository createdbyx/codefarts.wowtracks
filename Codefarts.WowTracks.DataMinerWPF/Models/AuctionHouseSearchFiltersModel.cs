﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using System.Collections.Generic;

    using Codefarts.WowTracks.DataMinerAppCore;

    public class AuctionHouseSearchFiltersModel
    {
        public List<IAuctionHouseSearchFilter> SearchFilters { get; set; }
    }
}
