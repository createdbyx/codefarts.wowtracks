﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;
    using Codefarts.WowTracks.DataModels;

    using OxyPlot;
    using OxyPlot.Axes;

    using LineSeries = OxyPlot.Series.LineSeries;

    public class AuctionHouseResultsChartModel : INotifyPropertyChanged
    {
        private PlotModel cachedModel;
        private List<DateValue> points = new List<DateValue>();
        public class DateValue
        {
            public DateTime Date { get; set; }
            public double Value { get; set; }
        }

        /// <summary>
        /// Gets the plot model.
        /// </summary>
        public PlotModel Model
        {
            get
            {
                return this.cachedModel;
            }
            private set
            {
                this.cachedModel = value;
                this.OnPropertyChanged();
            }
        }

        public List<DateValue> Points
        {
            get
            {
                return this.points;
            }
            private set
            {
                this.points = value;
                this.OnPropertyChanged();
            }
        }

        //public void GetBuyoutData(AuctionDataModel[] data)
        //{
        //    var buyoutData = from p in data
        //                     where p.ItemId == element
        //                     group p by p.CaptureDate into Group
        //                     select new
        //                     {
        //                         Date = Group.Key,
        //                         AverageBuyout = Group.Average(x => x.BuyoutValue),
        //                         MinBuyout = Group.Min(x => x.BuyoutValue),
        //                         MaxBuyout = Group.Max(x => x.BuyoutValue)
        //                     };

        //    var series1 = new LineSeries("Buyout") { MarkerType = MarkerType.Circle };

        //    var dates = buyoutData.Select(x => x.Date).OrderBy(x => x.Ticks);
        //    var dateIndexPairs = dates.Select((dt, i) => new KeyValuePair<DateTime, int>(dt, i));

        //    foreach (var dataModel in buyoutData)
        //    {
        //        series1.Points.Add(new DataPoint(dateIndexPairs.First(x => x.Key == dataModel.Date).Value, (int)dataModel.AverageBuyout));
        //    }

        //    tmp.Series.Add(series1);
        //}

        public void SetModel(AuctionDataModel[] data)
        {
            // Create the plot model
            var tmp = new PlotModel("Simple example", "using OxyPlot");
            tmp.Axes.Add(new DateTimeAxis(AxisPosition.Bottom));
            tmp.Axes.Add(new LinearAxis(AxisPosition.Left));

            // Create two line series (markers are hidden by default)

            //data = new[]
            //{
            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 1, 1, 0, 0), AuctionId = 1, ItemId = 6, BuyoutValue = 10, TimeLeft = AuctionTimeLeft.VeryLong }, 
            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 2, 1, 0, 0), AuctionId = 1, ItemId = 6, BuyoutValue = 10, TimeLeft = AuctionTimeLeft.Long },

            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 1, 1, 0, 0), AuctionId = 3, ItemId = 6, BuyoutValue = 6, TimeLeft = AuctionTimeLeft.VeryLong }, 
            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 2, 1, 0, 0), AuctionId = 3, ItemId = 6, BuyoutValue = 6, TimeLeft = AuctionTimeLeft.Long },

            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 1, 1, 0, 0), AuctionId = 3, ItemId = 6, BuyoutValue = 2, TimeLeft = AuctionTimeLeft.VeryLong }, 
            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 3, 1, 0, 0), AuctionId = 3, ItemId = 6, BuyoutValue = 2, TimeLeft = AuctionTimeLeft.Long },


            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 1, 1, 0, 0), AuctionId = 2, ItemId = 1, BuyoutValue = 25, TimeLeft = AuctionTimeLeft.VeryLong }, 
            //     new AuctionDataModel() { CaptureDate = new DateTime(2014, 1, 2, 1, 0, 0), AuctionId = 2, ItemId = 1, BuyoutValue = 25, TimeLeft = AuctionTimeLeft.Long },
            //};

           // var dates = data.Select(x => x.CaptureDate).AsParallel().OrderBy(x => x.Ticks);
          //  var dateIndexPairs = dates.Select((dt, i) => new KeyValuePair<DateTime, int>(dt, i)).AsParallel().ToArray();

          //  foreach (var itemId in data.Select(x => x.ItemId).AsParallel().Distinct())
           // {
                var buyoutData = from p in data
                               //  where p.ItemId == 6
                                 group p by p.CaptureDate into Group
                                 select new
                                 {
                                     Date = Group.Key,
                                     AverageBuyout = Group.AsParallel().Average(x => x.BuyoutValue).Value,
                                     MinBuyout = Group.AsParallel().Min(x => x.BuyoutValue),
                                     MaxBuyout = Group.AsParallel().Max(x => x.BuyoutValue)
                                 };

                // var series1 = new LineSeries(string.Format("ItemId: {0} Average Buyout", itemId)) { MarkerType = MarkerType.Circle };
                //var series1 = new LineSeries() { MarkerType = MarkerType.Circle };

                var points = new List<DateValue>();
                foreach (var dataModel in buyoutData.AsParallel().OrderBy(x => x.Date))
                {

                    points.Add(new DateValue { Date = dataModel.Date, Value = dataModel.AverageBuyout});
                    //points.Add(new DataPoint(dateIndexPairs.First(x => x.Key == dataModel.Date).Value, (int)dataModel.AverageBuyout));
                }

               // this.Points = points;
               
                // Create a line series
                var s1 = new LineSeries
                {
                    StrokeThickness = 1,
                    MarkerSize = 3,
                    ItemsSource = points,
                    DataFieldX = "Date",
                    DataFieldY = "Value",
                    MarkerStroke = OxyColors.ForestGreen,
                    MarkerType = MarkerType.Plus
                };

                tmp.Series.Add(s1);

                //series1.ItemsSource = points;
                //tmp.Series.Add(series1);

                //var series2 = new LineSeries(string.Format("ItemId: {0} Average Min Buyout", itemId)) { MarkerType = MarkerType.Circle };

                //foreach (var dataModel in buyoutData)
                //{
                //    series2.Points.Add(new DataPoint(dateIndexPairs.First(x => x.Key == dataModel.Date).Value, (int)dataModel.MaxBuyout));
                //}

                //tmp.Series.Add(series2);

                //var series3 = new LineSeries(string.Format("ItemId: {0} Average Max Buyout", itemId)) { MarkerType = MarkerType.Circle };

                //foreach (var dataModel in buyoutData)
                //{
                //    series3.Points.Add(new DataPoint(dateIndexPairs.First(x => x.Key == dataModel.Date).Value, (int)dataModel.MinBuyout));
                //}

                //tmp.Series.Add(series3);
           // }

            //foreach (var model in groups.AsParallel().Take(1))
            //{
            //    var series1 = new LineSeries("Time Left") { MarkerType = MarkerType.Circle };

            //    //init lineSeries
            //    //  var series1 = new BarSeries();

            //    foreach (var dataModel in model.data.GroupBy(x => x.AuctionId, y => y))
            //    {

            //        //   series1.Points.Add(new DataPoint(TimeSpan.FromTicks(dataModel.CaptureDate.Ticks).TotalMinutes, (int)dataModel.TimeLeft));
            //        //      series1.da.Points.Add(new DataPoint(TimeSpan.FromTicks(dataModel.CaptureDate.Ticks).TotalMinutes, (int)dataModel.TimeLeft));
            //    }

            //    tmp.Series.Add(series1);
            //}

            //series1.Points.Add(new DataPoint(0, 0));
            //series1.Points.Add(new DataPoint(10, 18));
            //series1.Points.Add(new DataPoint(20, 12));
            //series1.Points.Add(new DataPoint(30, 8));
            //series1.Points.Add(new DataPoint(40, 15));

            //var series2 = new LineSeries("Series 2") { MarkerType = MarkerType.Square };
            //series2.Points.Add(new DataPoint(0, 4));
            //series2.Points.Add(new DataPoint(10, 12));
            //series2.Points.Add(new DataPoint(20, 16));
            //series2.Points.Add(new DataPoint(30, 25));
            //series2.Points.Add(new DataPoint(40, 5));

            // Add the series to the plot model
            //tmp.Series.Add(series2);

            // Axes are created automatically if they are not defined

            // Set the Model property, the INotifyPropertyChanged event will make the WPF Plot control update its content
            this.Model = tmp;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
