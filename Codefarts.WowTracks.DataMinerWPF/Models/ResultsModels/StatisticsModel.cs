﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models.ResultsModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataModels;

    public class StatisticsModel : INotifyPropertyChanged
    {
        private ItemDisplayModel mostExpensiveItemBuyoutDisplay;

        private ItemDisplayModel cheapestItemBuyoutDisplay;

        private int totalNeutralItems;

        private int totalHordeItems;

        private int totalAllianceItems;

        private int totalItems;

        private int actualNeutralItems;

        private int actualHordeItems;

        private int actualAllianceItems;

        private int actualItems;

        private ItemDisplayModel mostExpensiveItemBidDisplay;

        private AuctionDataModel mostExpensiveItemBid;

        private AuctionDataModel mostExpensiveItemBuyout;

        private AuctionDataModel cheapestItemBid;

        private AuctionDataModel cheapestItemBuyout;

        private AuctionDataModel[] rarelyPostedItems;

        private AuctionDataModel[] mostFrequentlyPostedItems;

        private NameValueModel[] mostActiveToonsByAuctions;     

        private AuctionDataModel[] mostExpensiveItems;

        private AuctionDataModel[] cheapestItems;

        private NameValueModel[] mostActiveToonsByQuantity;

        private NameValueModel[] biggestMoneyMakersByBuyouts;

        private NameValueModel[] biggestMoneyMakersByBids;


        public NameValueModel[] BiggestMoneyMakersByBids
        {
            get
            {
                return this.biggestMoneyMakersByBids;
            }

            set
            {
                if (Equals(value, this.biggestMoneyMakersByBids))
                {
                    return;
                }

                this.biggestMoneyMakersByBids = value;
                this.OnPropertyChanged();
            }
        }

        public NameValueModel[] BiggestMoneyMakersByBuyouts
        {
            get
            {
                return this.biggestMoneyMakersByBuyouts;
            }

            set
            {
                if (Equals(value, this.biggestMoneyMakersByBuyouts))
                {
                    return;
                }

                this.biggestMoneyMakersByBuyouts = value;
                this.OnPropertyChanged();
            }
        }

        public NameValueModel[] MostActiveToonsByQuantity
        {
            get
            {
                return this.mostActiveToonsByQuantity;
            }

            set
            {
                if (Equals(value, this.mostActiveToonsByQuantity))
                {
                    return;
                }

                this.mostActiveToonsByQuantity = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] CheapestItems
        {
            get
            {
                return this.cheapestItems;
            }

            set
            {
                if (Equals(value, this.cheapestItems))
                {
                    return;
                }

                this.cheapestItems = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] MostExpensiveItems
        {
            get
            {
                return this.mostExpensiveItems;
            }
            set
            {
                if (Equals(value, this.mostExpensiveItems))
                {
                    return;
                }

                this.mostExpensiveItems = value;
                this.OnPropertyChanged();
            }
        }

        public NameValueModel[] MostActiveToonsByAuctions
        {
            get
            {
                return this.mostActiveToonsByAuctions;
            }

            set
            {
                if (Equals(value, this.mostActiveToonsByAuctions))
                {
                    return;
                }

                this.mostActiveToonsByAuctions = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] MostFrequentlyPostedItems
        {
            get
            {
                return this.mostFrequentlyPostedItems;
            }

            set
            {
                if (Equals(value, this.mostFrequentlyPostedItems))
                {
                    return;
                }

                this.mostFrequentlyPostedItems = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel[] RarelyPostedItems
        {
            get
            {
                return this.rarelyPostedItems;
            }

            set
            {
                if (Equals(value, this.rarelyPostedItems))
                {
                    return;
                }

                this.rarelyPostedItems = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel CheapestItemBuyout
        {
            get
            {
                return this.cheapestItemBuyout;
            }

            set
            {
                if (value == this.cheapestItemBuyout)
                {
                    return;
                }

                this.cheapestItemBuyout = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel CheapestItemBid
        {
            get
            {
                return this.cheapestItemBid;
            }

            set
            {
                if (value == this.cheapestItemBid)
                {
                    return;
                }

                this.cheapestItemBid = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel MostExpensiveItemBuyout
        {
            get
            {
                return this.mostExpensiveItemBuyout;
            }

            set
            {
                if (value == this.mostExpensiveItemBuyout)
                {
                    return;
                }

                this.mostExpensiveItemBuyout = value;
                this.OnPropertyChanged();
            }
        }

        public AuctionDataModel MostExpensiveItemBid
        {
            get
            {
                return this.mostExpensiveItemBid;
            }

            set
            {
                if (value == this.mostExpensiveItemBid)
                {
                    return;
                }

                this.mostExpensiveItemBid = value;
                this.OnPropertyChanged();
            }
        }

        public ItemDisplayModel CheapestItemBidDisplay { get; set; }

        public ItemDisplayModel MostExpensiveItemBidDisplay
        {
            get
            {
                return this.mostExpensiveItemBidDisplay;
            }

            set
            {
                if (Equals(value, this.mostExpensiveItemBidDisplay))
                {
                    return;
                }

                this.mostExpensiveItemBidDisplay = value;
                this.OnPropertyChanged();
            }
        }

        public int ActualItems
        {
            get
            {
                return this.actualItems;
            }

            set
            {
                if (value == this.actualItems)
                {
                    return;
                }

                this.actualItems = value;
                this.OnPropertyChanged();
            }
        }

        public int ActualAllianceItems
        {
            get
            {
                return this.actualAllianceItems;
            }

            set
            {
                if (value == this.actualAllianceItems)
                {
                    return;
                }

                this.actualAllianceItems = value;
                this.OnPropertyChanged();
            }
        }

        public int ActualHordeItems
        {
            get
            {
                return this.actualHordeItems;
            }

            set
            {
                if (value == this.actualHordeItems)
                {
                    return;
                }

                this.actualHordeItems = value;
                this.OnPropertyChanged();
            }
        }

        public int ActualNeutralItems
        {
            get
            {
                return this.actualNeutralItems;
            }

            set
            {
                if (value == this.actualNeutralItems)
                {
                    return;
                }

                this.actualNeutralItems = value;
                this.OnPropertyChanged();
            }
        }

        public int TotalItems
        {
            get
            {
                return this.totalItems;
            }

            set
            {
                if (value == this.totalItems)
                {
                    return;
                }

                this.totalItems = value;
                this.OnPropertyChanged();
            }
        }

        public int TotalAllianceItems
        {
            get
            {
                return this.totalAllianceItems;
            }

            set
            {
                if (value == this.totalAllianceItems)
                {
                    return;
                }

                this.totalAllianceItems = value;
                this.OnPropertyChanged();
            }
        }

        public int TotalHordeItems
        {
            get
            {
                return this.totalHordeItems;
            }

            set
            {
                if (value == this.totalHordeItems)
                {
                    return;
                }

                this.totalHordeItems = value;
                this.OnPropertyChanged();
            }
        }

        public int TotalNeutralItems
        {
            get
            {
                return this.totalNeutralItems;
            }

            set
            {
                if (value == this.totalNeutralItems)
                {
                    return;
                }

                this.totalNeutralItems = value;
                this.OnPropertyChanged();
            }
        }

        public ItemDisplayModel CheapestItemBuyoutDisplay
        {
            get
            {
                return this.cheapestItemBuyoutDisplay;
            }

            set
            {
                if (Equals(value, this.cheapestItemBuyoutDisplay))
                {
                    return;
                }

                this.cheapestItemBuyoutDisplay = value;
                this.OnPropertyChanged();
            }
        }

        public ItemDisplayModel MostExpensiveItemBuyoutDisplay
        {
            get
            {
                return this.mostExpensiveItemBuyoutDisplay;
            }

            set
            {
                if (Equals(value, this.mostExpensiveItemBuyoutDisplay))
                {
                    return;
                }

                this.mostExpensiveItemBuyoutDisplay = value;
                this.OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
