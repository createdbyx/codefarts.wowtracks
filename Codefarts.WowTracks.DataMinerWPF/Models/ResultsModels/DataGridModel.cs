﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Codefarts.WowTracks.DataMinerAppCore.Annotations;
    using Codefarts.WowTracks.DataModels;

    public class DataGridModel : INotifyPropertyChanged
    {
        private bool factionFieldVisible;

        private bool captureDateFieldVisible;

        private bool petQualityFieldVisible;

        private bool petLevelFieldVisible;

        private bool petBreedIdFieldVisible;

        private bool petSpeciesIdFieldVisible;

        private bool seedFieldVisible;

        private bool randomFieldVisible;

        private bool timeLeftFieldVisible;

        private bool ownerRealmFieldVisible;

        private bool quantityFieldVisible;

        private bool buyoutValueFieldVisible;

        private bool currentBidValueFieldVisible;

        private bool ownerNameFieldVisible;

        private bool itemIdFieldVisible;

        private bool auctionIdFieldVisible;

        private AuctionDataGridModel[] models;

        public AuctionDataGridModel[] Models
        {
            get
            {
                return this.models;
            }
            set
            {
                if (Equals(value, this.models))
                {
                    return;
                }

                this.models = value;
                this.OnPropertyChanged();
            }
        }

        public bool AuctionIdFieldVisible
        {
            get
            {
                return this.auctionIdFieldVisible;
            }
            set
            {
                if (value.Equals(this.auctionIdFieldVisible))
                {
                    return;
                }
                this.auctionIdFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool ItemIdFieldVisible
        {
            get
            {
                return this.itemIdFieldVisible;
            }
            set
            {
                if (value.Equals(this.itemIdFieldVisible))
                {
                    return;
                }
                this.itemIdFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool OwnerNameFieldVisible
        {
            get
            {
                return this.ownerNameFieldVisible;
            }
            set
            {
                if (value.Equals(this.ownerNameFieldVisible))
                {
                    return;
                }
                this.ownerNameFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool CurrentBidValueFieldVisible
        {
            get
            {
                return this.currentBidValueFieldVisible;
            }
            set
            {
                if (value.Equals(this.currentBidValueFieldVisible))
                {
                    return;
                }
                this.currentBidValueFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool BuyoutValueFieldVisible
        {
            get
            {
                return this.buyoutValueFieldVisible;
            }
            set
            {
                if (value.Equals(this.buyoutValueFieldVisible))
                {
                    return;
                }
                this.buyoutValueFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool QuantityFieldVisible
        {
            get
            {
                return this.quantityFieldVisible;
            }
            set
            {
                if (value.Equals(this.quantityFieldVisible))
                {
                    return;
                }
                this.quantityFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool OwnerRealmFieldVisible
        {
            get
            {
                return this.ownerRealmFieldVisible;
            }
            set
            {
                if (value.Equals(this.ownerRealmFieldVisible))
                {
                    return;
                }
                this.ownerRealmFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool TimeLeftFieldVisible
        {
            get
            {
                return this.timeLeftFieldVisible;
            }
            set
            {
                if (value.Equals(this.timeLeftFieldVisible))
                {
                    return;
                }
                this.timeLeftFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool RandomFieldVisible
        {
            get
            {
                return this.randomFieldVisible;
            }
            set
            {
                if (value.Equals(this.randomFieldVisible))
                {
                    return;
                }
                this.randomFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool SeedFieldVisible
        {
            get
            {
                return this.seedFieldVisible;
            }
            set
            {
                if (value.Equals(this.seedFieldVisible))
                {
                    return;
                }
                this.seedFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool PetSpeciesIdFieldVisible
        {
            get
            {
                return this.petSpeciesIdFieldVisible;
            }
            set
            {
                if (value.Equals(this.petSpeciesIdFieldVisible))
                {
                    return;
                }
                this.petSpeciesIdFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool PetBreedIdFieldVisible
        {
            get
            {
                return this.petBreedIdFieldVisible;
            }
            set
            {
                if (value.Equals(this.petBreedIdFieldVisible))
                {
                    return;
                }
                this.petBreedIdFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool PetLevelFieldVisible
        {
            get
            {
                return this.petLevelFieldVisible;
            }
            set
            {
                if (value.Equals(this.petLevelFieldVisible))
                {
                    return;
                }
                this.petLevelFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool PetQualityFieldVisible
        {
            get
            {
                return this.petQualityFieldVisible;
            }
            set
            {
                if (value.Equals(this.petQualityFieldVisible))
                {
                    return;
                }
                this.petQualityFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool CaptureDateFieldVisible
        {
            get
            {
                return this.captureDateFieldVisible;
            }
            set
            {
                if (value.Equals(this.captureDateFieldVisible))
                {
                    return;
                }
                this.captureDateFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public bool FactionFieldVisible
        {
            get
            {
                return this.factionFieldVisible;
            }
            set
            {
                if (value.Equals(this.factionFieldVisible))
                {
                    return;
                }
                this.factionFieldVisible = value;
                this.OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
