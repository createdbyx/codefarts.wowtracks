﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;

    using Codefarts.ContentManager;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.Repository;

    public class MEFComposer
    {
        [ImportMany(typeof(IRepository))]
        public List<IRepository> Repositories;

        [ImportMany(typeof(IAuctionHouseSearchFilter))]
        public List<IAuctionHouseSearchFilter> AuctionHouseSearchFilters;

        [ImportMany(typeof(IReader<string>))]
        public List<IReader<string>> StringContentReaders;

        [ImportMany(typeof(IResultsPlugin))]
        public List<IResultsPlugin> ResultPlugins;
    }
}