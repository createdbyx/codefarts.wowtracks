﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class TotalCopperConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var newValue = (long)value;
           // var totalGold = (newValue / 100) / 100;
           // var totalSilver = (newValue / 100) - (totalGold * 100);
            var totalCopper = newValue % 100;
            return totalCopper;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}