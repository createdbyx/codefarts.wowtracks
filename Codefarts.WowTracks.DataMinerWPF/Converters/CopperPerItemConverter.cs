﻿namespace Codefarts.WowTracks.DataMinerWPF
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    public class CopperPerItemConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            if (values.Length != 2)
            {
                throw new ArgumentException("Expected two arguments.");
            }
            if (values[0] == DependencyProperty.UnsetValue)
            {
                return 0;
            }

            if (values[1] == DependencyProperty.UnsetValue)
            {
                return 0;
            }
            for (var i = 0; i < values.Length; i++)
            {
                if (!(values[i] is long) && !(values[i] is int))
                {
                    throw new InvalidCastException(string.Format("Argument {0} is not a long or int type.", i));
                }
            }

            long newValue;
            long totalCopper;
            long quantity;
            switch ((string)parameter)
            {
                case "FormatLastFirst":
                    newValue = (long)values[1];
                    quantity = System.Convert.ToInt64(values[0]);
                    totalCopper = quantity == 0 ? 0 : (newValue % 100) / quantity;
                    return System.Convert.ChangeType(totalCopper, targetType);

                case "FormatNormal":
                default:
                    newValue = (long)values[0];
                    quantity = System.Convert.ToInt64(values[1]);
                    totalCopper = quantity == 0 ? 0 : (newValue % 100) / quantity;
                    return System.Convert.ChangeType(totalCopper, targetType);
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}