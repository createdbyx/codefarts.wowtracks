﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Security.Policy;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;

    using Codefarts.ContentManager;
    using Codefarts.Logging;
    using Codefarts.Logging.WPFControls;
    using Codefarts.Settings;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.Repository;

    using Newtonsoft.Json;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SettingsModel settingsData;


        private DataMinerApplication app;

        private bool rebuildingTables;

        private CancellationTokenSource searchCancelToken;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SetDataFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.settingsData.DataFolder = dialog.SelectedPath;
                }
            }
        }

        public async Task Compose()
        {
            // try to connect with MEF types
            try
            {
                var parts = new MEFComposer();
                MEFHelpers.Compose(new[] { Path.Combine(Environment.CurrentDirectory, "Plugins") }, parts);
                RepositoryFactory.Instance.Repository = parts.Repositories.FirstOrDefault();

                foreach (var reader in parts.StringContentReaders)
                {
                    ContentManager<string>.Instance.Register(reader);
                }

                foreach (var filter in parts.AuctionHouseSearchFilters)
                {
                    this.app.RegisteredAuctionHouseFilters.Add(filter);
                }

                foreach (var plugin in parts.ResultPlugins)
                {
                    this.app.RegisteredResultPlugins.Add(plugin);
                }
            }
            catch
            {
                // ERR: handle error
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.SetupSettings();

            this.app = new DataMinerApplication(this.settingsData);
            this.app.Browse += this.app_Browse;
            await this.Compose();
            this.app.Initialize();
            // this.app.LoadItemNameAndIdTable();                        

            //foreach (var filter in this.app.RegisteredAuctionHouseFilters)
            //{
            //    filter.Connect(this.app);
            //}
            // this.app.RegisteredAuctionHouseFilters.CollectionChanged += this.AuctionHouseFiltersChanged;
            //  this.app.AuctionHouseFilterControls.Add(typeof(Models.AuctionHouseSearchFiltersModel), new Controls.MainAuctionHouseFilterControl());

            Logging.Repositories.Add(new TextBoxLogger(this.LoggingTextBox));

            // load item id names table

            this.settingsData.PropertyChanged += this.settingsModel_PropertyChanged;
            this.SettingsTab.DataContext = this.settingsData;
            this.AuctionsTab.DataContext = this.app;
            this.LoadAuctionHouseDataFilters();

            // this.RawDataTabItem.DataContext = this.dataGridModel;
        }

        void app_Browse(object sender, BrowseToEventArgs e)
        {
            this.Browser.Source = new Uri(e.Url, UriKind.Absolute);
            this.BrowserTab.IsSelected = true;
            this.Browser.Focus();
        }

        //private void AuctionHouseFiltersChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    switch (e.Action)
        //    {
        //        case NotifyCollectionChangedAction.Add:
        //            foreach (var item in e.NewItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                var type = item.GetType();
        //                if (type == typeof(MainAuctionHouseFilterModel))
        //                {
        //                    continue;
        //                }

        //                if (this.AuctionFilters.Children.OfType<IAuctionHouseFilterControl>().All(x => x.GetType() != type))
        //                {
        //                    var control = (IAuctionHouseFilterControl)item.Control.Assembly.CreateInstance(item.Control.FullName);
        //                    control.SetModel(item);
        //                    control.SetApplication(this.app);
        //                    this.AuctionFilters.Children.Add((UIElement)control);
        //                }
        //            }

        //            break;

        //        case NotifyCollectionChangedAction.Remove:
        //            foreach (var item in e.OldItems.OfType<IAuctionHouseSearchFilter>())
        //            {
        //                var type = item.GetType();   

        //                var first = this.AuctionFilters.Children.OfType<IAuctionHouseFilterControl>().FirstOrDefault(x => x.GetType() != type);
        //                this.app.RegisteredAuctionHouseFilters.Remove(item);
        //                if (first != null)
        //                {
        //                    this.AuctionFilters.Children.Remove((UIElement)first);
        //                }
        //            }

        //            break;

        //        case NotifyCollectionChangedAction.Replace:
        //            break;
        //        case NotifyCollectionChangedAction.Move:
        //            break;
        //        case NotifyCollectionChangedAction.Reset:
        //            break;
        //        default:
        //            throw new ArgumentOutOfRangeException();
        //    }
        //}

        private void LoadAuctionHouseDataFilters()
        {
            var control = this.MainFilterControl as IAuctionHouseFilterControl;
            control.SetModel(this.app.MainAuctionHouseFilterModel);
            control.SetApplication(this.app);
            // this.app.RegisteredAuctionHouseFilters.Add(this.mainAuctionHouseFilterModel);
            if (this.app.RegisteredAuctionHouseFilters.Count > 0)
            {
                this.FilterList.SelectedIndex = 0;
            }
        }

        private void settingsModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var settings = SettingsManager.Instance;
            settings.SetValue(SettingConstants.DataFolder, this.settingsData.DataFolder);
            settings.SetValue(SettingConstants.AutomaticallyStartDownloadsOnStartup, this.settingsData.AutomaticallyRunSchedulesOnStartup);
            settings.SetValue(SettingConstants.MinimumWaitTimeBetweenSchedules, this.settingsData.MinimumWaitTimeBetweenSchedules.TotalMinutes);
            settings.SetValue(SettingConstants.MaximumProcessingTasks, this.settingsData.MaxProcessingTasks);
            settings.SetValue(SettingConstants.AutoScrollLogs, this.settingsData.AutoScrollLogs);
        }

        private void SetupSettings()
        {
            var settings = SettingsManager.Instance;

            this.settingsData = new SettingsModel();
            this.settingsData.DataFolder = settings.GetSetting(SettingConstants.DataFolder, string.Empty);
            this.settingsData.LogFolder = settings.GetSetting(SettingConstants.LogFolder, string.Empty);
            this.settingsData.AutomaticallyRunSchedulesOnStartup = settings.GetSetting(SettingConstants.AutomaticallyStartDownloadsOnStartup, false);
            this.settingsData.MaxProcessingTasks = settings.GetSetting(SettingConstants.MaximumProcessingTasks, 4);
            this.settingsData.MinimumWaitTimeBetweenSchedules = TimeSpan.FromMinutes(settings.GetSetting(SettingConstants.MinimumWaitTimeBetweenSchedules, 10f));
            this.settingsData.AutoScrollLogs = settings.GetSetting(SettingConstants.AutoScrollLogs, true);
        }

        private void AddFilterButton_OnClick(object sender, RoutedEventArgs e)
        {
            IAuctionHouseFilterControl control;
            IAuctionHouseSearchFilter filterModel;
            try
            {
                filterModel = this.FilterList.SelectedItem as IAuctionHouseSearchFilter;
                filterModel = filterModel.Clone();
                var type = Type.GetType(filterModel.Control);
                control = (IAuctionHouseFilterControl)type.Assembly.CreateInstance(filterModel.Control);
                filterModel.Connect(this.app);  
                control.SetModel(filterModel);
                control.SetApplication(this.app);  
            }
            catch (Exception ex)
            {
                this.LogError(ex.Message);
                MessageBox.Show(this, ex.Message, "Error");
                return;
            }

            var container = new AuctionHouseFilterWrapperControl();
            container.Content = control;
            container.DataContext = new HeaderModel() { Text = filterModel.Name };
            container.Model = filterModel;
            container.Delete += container_Delete;
            container.MoveDown += container_MoveDown;
            container.MoveUp += container_MoveUp;

            this.AuctionFilters.Children.Add(container);
            this.app.AuctionHouseSearches.Add(filterModel);
        }

        void container_MoveUp(object sender, EventArgs e)
        {
            var control = sender as AuctionHouseFilterWrapperControl;
            if (control == null)
            {
                return;
            }

            var index = this.app.AuctionHouseSearches.IndexOf(control.Model);
            if (index != -1)
            {
                this.app.AuctionHouseSearches.TrySwap(index, index - 1, true);
                this.AuctionFilters.Children.TrySwap(index, index - 1, true);
            }
        }

        void container_MoveDown(object sender, EventArgs e)
        {
            var control = sender as AuctionHouseFilterWrapperControl;
            if (control == null)
            {
                return;
            }

            var index = this.app.AuctionHouseSearches.IndexOf(control.Model);
            if (index != -1)
            {
                this.app.AuctionHouseSearches.TrySwap(index, index + 1, true);
                this.AuctionFilters.Children.TrySwap(index, index + 1, true);
            }
        }

        void container_Delete(object sender, EventArgs e)
        {
            var control = sender as AuctionHouseFilterWrapperControl;
            if (control == null)
            {
                return;
            }

            var index = this.app.AuctionHouseSearches.IndexOf(control.Model);
            if (index != -1)
            {
                control.Model.Disconnect();
                this.app.AuctionHouseSearches.RemoveAt(index);
                this.AuctionFilters.Children.Remove(control);
            }
        }

        private async void SearchButton_OnClick(object sender, RoutedEventArgs e)
        {
            // query the repository for the data set in the main auction house filter to get the initial data set
            var repository = RepositoryFactory.Instance.Repository;

            var model = this.app.MainAuctionHouseFilterModel;
            var realmName = model.SelectedRealm;
            var region = model.SelectedRegion;
            var startDate = model.StartDate;
            var endDate = model.EndDate;
            Action<float, string> callback = (progress, message) =>
                {
                    this.Dispatcher.Invoke(() => this.StatusText.Text = string.Format("{0}% - {1}", Math.Round(progress, 2), message));
                };

            if (this.searchCancelToken == null)
            {
                this.searchCancelToken = new CancellationTokenSource();
            }

            AuctionDataModel[] results;
            try
            {
                results = await repository.GetAuctionHouseData(region, realmName, startDate, endDate, callback, this.searchCancelToken.Token);
                results = this.app.AuctionHouseSearches.Aggregate(results, (current, filter) => filter.Filter(model, current))
                                   .AsParallel()
                                   .ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
                this.LogError(ex.Message);
                return;
            }

            // // show results
            foreach (var plugin in this.app.RegisteredResultPlugins)
            {
                if (!plugin.IsConnected)
                {
                    plugin.Connect(this.app);
                }

                var tab = this.ResultsTabControl.Items.OfType<TabItem>().FirstOrDefault(x => (Guid)x.Tag == plugin.UniqueId);
                if (tab == null)
                {
                    tab = new TabItem() { Tag = plugin.UniqueId, Header = plugin.Name };
                    this.ResultsTabControl.Items.Add(tab);
                }

                try
                {
                    IResultsControl control;
                    if (tab.Content == null)
                    {
                        var type = Type.GetType(plugin.Control);
                        if (type == null)
                        {
                            throw new NullReferenceException(string.Format("Could not get type for control {0}", plugin.Control));
                        }

                        control = type.Assembly.CreateInstance(plugin.Control) as IResultsControl;
                        if (control == null)
                        {
                            throw new InvalidCastException("Control does not implement the IResultsControl interface.");
                        }

                        tab.Content = control;
                        control.Initialize(this.app, plugin);
                    }
                    else
                    {
                        control = tab.Content as IResultsControl;
                    }

                    plugin.UpdateData(results, control);

                    if (this.ResultsTabControl.SelectedIndex < 0)
                    {
                        this.ResultsTabControl.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    this.ResultsTabControl.Items.Remove(tab);
                    this.LogError(ex.Message);
                }
            }

            // display results

            //var dataContext = new AuctionHouseResultsChartModel();
            //var dataModels = results;
            //dataContext.SetModel(dataModels);
            //  this.ChartData.DataContext = dataContext;



            //  this.BuildTestResults();

            //this.dataGridModel.Models.Clear();
            //foreach (var model in dataModels)
            //{
            //    this.dataGridModel.Models.Add(model);
            //}
            //this.ResultsGrid.ItemsSource = dataModels;  
        }

        /*
        private void BuildTestResults()
        {
            var models = new List<AuctionDataGridModel>();

            for (int q = 1; q < 5; q++)
            {
                for (var i = 99; i < 102; i++)
                {
                    var contentManager = ContentManager<string>.Instance;
                    models.Add(
                        new AuctionDataGridModel()
                            {
                                AuctionId = i,
                                ItemId = 4306,
                                //DisplayModel = this.app.DisplayItemModels.FirstOrDefault(y => y.ItemId == 4306),
                                DisplayModel =
                                    new ItemDisplayModel(
                                    Region.US.Name,
                                    contentManager.Load<ItemInfo>(Path.Combine(Region.US.Name, "ItemIds", "4306.json"))),
                                CurrentBidValue = i,
                                BuyoutValue = i,
                                OwnerName = "Meme",
                                Quantity = q
                            });
                }

                for (var i = 999; i < 1002; i++)
                {
                    var contentManager = ContentManager<string>.Instance;
                    models.Add(
                        new AuctionDataGridModel()
                            {
                                AuctionId = i,
                                ItemId = 4306,
                                //DisplayModel = this.app.DisplayItemModels.FirstOrDefault(y => y.ItemId == 4306),
                                DisplayModel =
                                    new ItemDisplayModel(
                                    Region.US.Name,
                                    contentManager.Load<ItemInfo>(Path.Combine(Region.US.Name, "ItemIds", "4306.json"))),
                                CurrentBidValue = i,
                                BuyoutValue = i,
                                OwnerName = "Meme",
                                Quantity = q
                            });
                }

                for (var i = 9999; i < 10002; i++)
                {
                    var contentManager = ContentManager<string>.Instance;
                    models.Add(
                        new AuctionDataGridModel()
                            {
                                AuctionId = i,
                                ItemId = 4306,
                                //DisplayModel = this.app.DisplayItemModels.FirstOrDefault(y => y.ItemId == 4306),
                                DisplayModel =
                                    new ItemDisplayModel(
                                    Region.US.Name,
                                    contentManager.Load<ItemInfo>(Path.Combine(Region.US.Name, "ItemIds", "4306.json"))),
                                CurrentBidValue = i,
                                BuyoutValue = i,
                                OwnerName = "Meme",
                                Quantity = q
                            });
                }
            }

            this.dataGridModel.Models = models.ToArray();
        }
      */

        //private void RebuildItemTablesButton_OnClick(object sender, RoutedEventArgs e)
        //{
        //    if (this.rebuildingTables)
        //    {
        //        return;
        //    }

        //    this.rebuildingTables = true;
        //    Task.Factory.StartNew(
        //        () =>
        //        {
        //            foreach (var region in Region.AllRegions)
        //            {
        //                var path = Path.Combine(this.settingsData.DataFolder, region.Name, "ItemIds");
        //                if (!Directory.Exists(path))
        //                {
        //                    continue;
        //                }

        //                var files = Directory.GetFiles(path, "*.json");
        //                var list = new List<ItemInfo>();

        //                for (var index = 0; index < files.Length; index++)
        //                {
        //                    var file = files[index];
        //                    try
        //                    {
        //                        //var data = JsonConvert.DeserializeObject<ItemInfo>(File.ReadAllText(file));
        //                        var data = ContentManager<string>.Instance.Load<ItemInfo>(file);
        //                        list.Add(data);
        //                        this.Dispatcher.Invoke(
        //                            () =>
        //                            {
        //                                var text = string.Format(
        //                                    "Rebuilding table for region {0} - {1}/{2} files {3}% complete",
        //                                    region.Name,
        //                                    index,
        //                                    files.Length,
        //                                    Math.Round(index / (float)files.Length, 2) * 100);
        //                                this.StatusText.Text = text;
        //                            });
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        this.LogError("Failed to load \"{0}\" {1}", file, ex.Message);
        //                    }
        //                }

        //                var tableFile = Path.Combine(this.settingsData.DataFolder, region.Name, "ItemIdNameTable.json");
        //                try
        //                {
        //                    Directory.CreateDirectory(Path.GetDirectoryName(tableFile));
        //                    File.WriteAllText(tableFile, JsonConvert.SerializeObject(list.ToArray(), Formatting.Indented));
        //                    this.LogInfo("Loaded {0} items into table for region {1}", list.Count, region.Name);
        //                }
        //                catch (Exception ex)
        //                {
        //                    this.LogError("Failed to save item table file \"{0}\" {1}", tableFile, ex.Message);
        //                }
        //            }

        //            this.app.LoadItemNameAndIdTable();
        //            this.rebuildingTables = false;
        //        });
        //}

        private void LogError(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Error, string.Format(message, args), "RebuildItemsTable");
        }

        private void LogInfo(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Information, string.Format(message, args), "RebuildItemsTable");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            var cancellationTokenSource = this.searchCancelToken;
            if (cancellationTokenSource != null)
            {
                cancellationTokenSource.Cancel();
            }

            this.app.CancelBuildDisplayItems();

            foreach (var plugin in this.app.RegisteredResultPlugins)
            {
                if (plugin.IsConnected)
                {
                    plugin.Disconnect();
                }
            }
        }
    }
}
