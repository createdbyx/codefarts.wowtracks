﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    public class SettingConstants
    {
        public const string DataFolder = "Codefarts.WowTracks.DataFolder";

        public const string AutomaticallyStartDownloadsOnStartup = "Codefarts.WowTracks.StartDownloadsOnStartup";

        public const string MinimumWaitTimeBetweenSchedules = "Codefarts.WowTracks.MinimumWaitTimeBetweenSchedules";

        public const string MaximumProcessingTasks = "Codefarts.WowTracks.MaximumNumberOfProcessingTasks";

        public const string AutoScrollLogs = "Codefarts.WowTracks.AutoScrollLogs";
       
        /// <summary>
        /// The name of the setting that holds the log folder location.
        /// </summary>
        public const string LogFolder = "Codefarts.Logging.LogFolder";
    }
}
