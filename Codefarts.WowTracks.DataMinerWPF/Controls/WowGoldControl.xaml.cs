﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerWPF.Models;

    /// <summary>
    /// Interaction logic for WowGoldControl.xaml
    /// </summary>
    public partial class WowGoldControl : UserControl
    {
        //private long value;

        //private int quantity;

        //public long Value
        //{
        //    get
        //    {
        //        return this.value;
        //    }

        //    set
        //    {
        //        this.value = value;
        //        this.UpdateFields();
        //    }
        //}

        private void UpdateFields()
        {
            var model = this.DataContext as AuctionDataGridModel;

            //var totalGold = (this.value / 100) / 100;
            //var totalSilver = (this.value / 100) - (totalGold * 100);
            //var totalCopper = totalSilver % 100;

            //this.GoldValueTotal.Text = totalGold.ToString();
            //this.SilverValueTotal.Text = totalSilver.ToString();
            //this.CopperValueTotal.Text = totalCopper.ToString();

            //totalGold = ((this.value / this.quantity) / 100) / 100;
            //totalSilver = (this.value / 100) - (totalGold * 100);
            //totalCopper = totalSilver % 100;

            //this.GoldValue.Text = totalGold.ToString();
            //this.SilverValue.Text = totalSilver.ToString();
            //this.CopperValue.Text = totalCopper.ToString();
        }

        //public int Quantity
        //{
        //    get
        //    {
        //        return this.quantity;
        //    }
        //    set
        //    {
        //        this.quantity = value;
        //        this.UpdateFields();
        //    }
        //}

        public WowGoldControl()
        {
            InitializeComponent();
        }

        private void UserControl_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            this.UpdateFields();
        }

    }
}
