﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using NavigationEventArgs = System.Windows.Navigation.NavigationEventArgs;

    /// <summary>
    /// Interaction logic for CustomBrowserControl.xaml
    /// </summary>
    public partial class CustomBrowserControl : UserControl
#if AWSOMEIUM
        , IResourceInterceptor  
#endif
    {

#if AWSOMEIUM
        #region IResourceInterceptor
        ResourceResponse IResourceInterceptor.OnRequest(ResourceRequest request)
        {
            // Resume normal processing.
            return null;
        }

        bool IResourceInterceptor.OnFilterNavigation(NavigationRequest request)
        {
            // ResourceInterceptor is global. It applies to all views
            // maintained by the WebCore. We are only interested in 
            // requests targeting our WebControl.
            // CAUTION: IResourceInterceptor members are called on the I/O thread.
            // Cast to IWebView to perform thread-safe access to the Identifier property!
            if (request.ViewId != ((IWebView)this.Browser).Identifier)
                return false;

            if ((request.Url == null) || (request.Url.Scheme != "asset"))
                // Block navigations to resources outside our 
                // application's asset stores.
                return true;

            // Allow navigation.
            return false;
        }
        #endregion
        
#endif

        public CustomBrowserControl()
        {
            InitializeComponent();
            //  ((SHDocVw.DWebBrowserEvents2_Event)this.Browser.ActiveXInstance).NewWindow3 += new SHDocVw.DWebBrowserEvents2_NewWindow3EventHandler(MainWindow_NewWindow3);
        }

        //void MainWindow_NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags, string bstrUrlContext, string bstrUrl)
        //{
        //    this.Browser.Navigate(bstrUrl);
        //    Cancel = true;
        //}

        private void HomeBrowserButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Browser.Source = new Uri("http://us.battle.net/wow/en/game/", UriKind.Absolute);
        }

        public Uri Source
        {
            get
            {
                return this.Browser.Source;
            }

            set
            {
                this.Browser.Source = value;
            }
        }

#if AWSOMEIUM
        private void webControl_ConsoleMessage(object sender, ConsoleMessageEventArgs e)
        {
            Logging.Log(String.Format(">{0}", e.Message), "WebControl");
        }

        private void webControl_NativeViewInitialized(object sender, WebViewEventArgs e)
        {
            // We demonstrate the creation of a child global object.
            // Acquire the parent first.
            JSObject external = this.Browser.CreateGlobalJavascriptObject("external");

            if (external == null)
            {
                return;
            }

            using (external)
            {
                // Create a child using fully qualified name. This only succeeds if
                // the parent is created first.
                JSObject app = this.Browser.CreateGlobalJavascriptObject("external.app");

                if (app == null)
                {
                    return;
                }

                using (app)
                {
                    // Create and bind to an asynchronous custom method. This is called
                    // by JavaScript to have the native app perform some heavy work.
                    // (See: /web/index.html)
                    app.Bind("performHeavyWork", false, OnWork);
                }
            }
        }

        private void OnWork(object sender, JavascriptMethodEventArgs e)
        {
            // Must be a function object.
            if (!e.Arguments[0].IsObject)
                return;

            // You can cache this callback and call it only when your application 
            // has performed all work necessary and has a result ready to send.
            // Note that this callback object is valid for as long as the current 
            // page is loaded. A navigation will invalidate it!
            JSObject callbackArg = e.Arguments[0];

            // Make sure it's a function object.
            if (!callbackArg.HasMethod("call"))
                return;

            // See it!
            Debug.Print(callbackArg.ToString());

            // You need to copy the object if you intend to cache it. The original
            // object argument passed to the handler is destroyed by default by 
            // native Awesomium, when the handler returns. A copy will keep it alive.
            JSObject callback = callbackArg.Clone();

            // Perform your heavy work.
            Task.Factory.StartNew(
                (Func<object, string>)PerformWork, callback).ContinueWith(
                /* Send a response when complete. */
                SendResponse,
                /* Make sure the response is sent on the 
                 * initial thread. */
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private string PerformWork(object callback)
        {
            // Perform heavy work.
            // Thread.Sleep(3000);

            return "Some Result";
        }

        private void SendResponse(Task<string> t)
        {
            // Get the callback. JSObject supports the DLR so for 
            // this example we assign to a dynamic which makes invoking the
            // callback, pretty straightforward (just as you would in JS).
            dynamic callback = t.AsyncState;

            if (callback == null)
            {
                return;
            }

            using (callback)
            {

                if ((t.Exception != null) || String.IsNullOrEmpty(t.Result))
                {
                    // We failed.
                    return;
                }

                // Invoke the callback.
                callback(t.Result);

                // JSObject supports the DLR. However, if you choose to explicitly cast back to 
                // JSObject, this is the technique for invoking the callback, using regular pattern:
                //callback.InvokeAsync( "call", callback, t.Result );
            }
        } 

              private void webControl_DocumentReady(object sender, UrlEventArgs e)
        {
            // When using dynamic, please note that you should always:
            // - Wrap your code in a try...catch, especially if you're
            //   making invocations, to avoid unhandled RuntimeBinder
            //   exceptions.
            // - Check the validity of the returned value to avoid
            //   NullReference exceptions.
            // - Dispose of the objects after you're done with them.
            //   This only disposes the managed wrapper and the
            //   native proxy. You can still re-acquire the objects 
            //   later.

            try
            {
                dynamic document = (JSObject)this.Browser.ExecuteJavascriptWithResult("document");

                if (document == null)
                {
                    return;
                }

                using (document)
                {
                    // This is an invocation. If the object does not contain
                    // a method named 'getElementById', you would get a 
                    // RuntimeBinderException. Of course this cannot be the
                    // case with the 'document' object.
                    dynamic elem1 = document.getElementById("postfield1");

                    if (elem1 == null)
                    {
                        return;
                    }

                    using (elem1)
                    {
                        // This is a property setting. If the object does not
                        // contain a property named 'value', you should NOT get an
                        // exception. JavaScript is dynamic so this code would just
                        // add a 'value' property to the object, even if one was not
                        // there before, and set its value to "test".
                        elem1.value = "Some Value 1";
                    }

                    dynamic elem2 = document.getElementById("postfield2");

                    if (elem2 == null)
                    {
                        return;
                    }

                    using (elem2)
                    {
                        elem2.value = "Some Value 2";
                    }
                }
            }
            catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException ex)
            {
                Logging.Log(LogEntryType.Error, ex.Message, "WebControl");
            }
            catch
            {
                throw;
            }
        }

        private void webControl_WindowClose(object sender, WindowCloseEventArgs e)
        {
            //if (!e.IsCalledFromFrame)
            //{
            //    this.Close();
            //}
        }

        private void Browser_LoadingFrameComplete(object sender, FrameEventArgs e)
        {
            this.AddressBar.Text = this.Browser.Source.ToString();
        }

        private void Browser_OnLoadingFrameFailed(object sender, LoadingFrameFailedEventArgs e)
        {
            Logging.Log(String.Format("{0}: {1}", e.ErrorCode, e.ErrorDescription), "WebControl");
        }
#endif

        private void StopBrowserButton_OnClick(object sender, RoutedEventArgs e)
        {
#if AWSOMEIUM
            this.Browser.Stop(); 
#endif
        }

        private void AddressBar_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.Browser.Source = new Uri(this.AddressBar.Text, UriKind.RelativeOrAbsolute);
            }
        }

        private void RefreshBrowserButton_OnClick(object sender, RoutedEventArgs e)
        {
#if AWSOMEIUM
            this.Browser.Reload(true); 
#else
            this.Browser.Refresh(true);
#endif
        }

        private void NavigateForwardButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Browser.GoForward();
        }

        private void NavigateBackButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Browser.GoBack();
        }

        private void NavigateBackButton_GotFocus(object sender, RoutedEventArgs e)
        {
            this.Browser.Focus();
        }

        private void Browser_OnNavigated(object sender, NavigationEventArgs e)
        {
            //if (this.AddressBar.Text.Length == 0)
            //{
            //    this.AddressBar.Focus();
            //    this.AddressBar.Background = new SolidColorBrush(Colors.Red);
            //}
            //else
            //{
            //    this.AddressBar.Background = new SolidColorBrush(SystemColors.WindowColor);
            //    string url;
            //    if (this.AddressBar.Text.IndexOf("http://") != 0)
            //    {
            //        url = "http://" + this.AddressBar.Text;
            //    }
            //    else
            //    {
            //        url = this.AddressBar.Text;
            //    }

            //    this.Browser.Navigate(new Uri(url));
            this.AddressBar.Text = this.Browser.Source.ToString();
            //}
        }

        private void GoToBrowserButton_OnClick(object sender, RoutedEventArgs e)
        {
            string url;
            if (this.AddressBar.Text.IndexOf(Uri.UriSchemeHttp + Uri.SchemeDelimiter) != 0)
            {
                url = Uri.UriSchemeHttp + Uri.SchemeDelimiter + this.AddressBar.Text;
            }
            else
            {
                url = this.AddressBar.Text;
            }

            this.Browser.Source = new Uri(url, UriKind.RelativeOrAbsolute);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            // Destroy the WebControl and its underlying view.
            this.Browser.Dispose();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //// Initialize the Core before InitializeComponent.
            //// When the WebControl is created, it will automatically
            //// initialize the Core with default configuration.
            //WebCore.Initialize(new WebConfig()
            //{
            //    HomeURL = new Uri("http://us.battle.net/wow/en/game/"),
            //    LogLevel = LogLevel.Verbose,
            //});

            InitializeComponent();

#if           AWSOMEIUM

            // Since we perform lazy initialization, this is a
            // safe way to assign the ResourceInterceptor, only when
            // the WebCore is running.
            if (WebCore.IsRunning)
                WebCore.ResourceInterceptor = this;
            else
            {
                // We could simply write this like that:
                //WebCore.Started += ( s, e ) => WebCore.ResourceInterceptor = this;
                // See below why we don't do it.

                CoreStartEventHandler handler = null;
                handler = (s, e1) =>
                {
                    // Though this example shuts down the core when this
                    // MainWindow closes, in a normal application scenario,
                    // there could be many instances of MainWindow. We don't
                    // want them all referenced by the WebCore singleton
                    // and kept from garbage collection, so the handler
                    // has to be removed.
                    WebCore.Started -= handler;
                    WebCore.ResourceInterceptor = this;
                };
                WebCore.Started += handler;
            }

            // Assign our global ShowCreatedWebView handler.
            this.Browser.ShowCreatedWebView += App.OnShowNewView; 
#endif
        }


    }
}
