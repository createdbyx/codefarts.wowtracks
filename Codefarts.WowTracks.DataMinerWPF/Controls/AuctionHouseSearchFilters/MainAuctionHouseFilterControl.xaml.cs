﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls
{
    using System;
    using System.Linq;
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.Repository;

    /// <summary>
    /// Interaction logic for MainAuctionHouseFilterControl.xaml
    /// </summary>
    public partial class MainAuctionHouseFilterControl : UserControl, IAuctionHouseFilterControl
    {
        private Application app;

        public event EventHandler<RegionEventArgs> RegionChanged;

        protected virtual void OnRegionChanged(string region)
        {
            var handler = this.RegionChanged;
            if (handler != null)
            {
                handler(this, new RegionEventArgs(region));
            }
        }

        public MainAuctionHouseFilterControl()
        {
            InitializeComponent();
        }

        public string SelectedRegion
        {
            get
            {
                return this.cboRegions.Dispatcher.Invoke(() => this.cboRegions.SelectedItem as string);
            }
        }

        public async void SetModel(IAuctionHouseSearchFilter model)
        {
            if (model.GetType() != typeof(MainAuctionHouseFilterModel))
            {
                throw new InvalidCastException();
            }

            this.DataContext = model;

            var filter = this.DataContext as MainAuctionHouseFilterModel;

            var repository = RepositoryFactory.Instance.Repository;
            var count = await repository.GetNumberOfRealmsBeingTracked();
            var realms = await repository.GetRealmsBeingTracked(0, count);
            var realmTrackingModels = realms as RealmTrackingModel[] ?? realms.ToArray();

            filter.Regions = realmTrackingModels.Select(x => x.Region).Distinct().ToArray();
            filter.SelectedRegionIndex = 0;
        }

        public void SetApplication(Application app)
        {
            this.app = app;
        }

        private async void cboRegions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var filter = this.DataContext as MainAuctionHouseFilterModel;

            var repository = RepositoryFactory.Instance.Repository;
            var count = await repository.GetNumberOfRealmsBeingTracked();
            var realms = await repository.GetRealmsBeingTracked(0, count);
            var realmTrackingModels = realms as RealmTrackingModel[] ?? realms.ToArray();

            filter.Realms = realmTrackingModels.Where(x => x.Region == filter.SelectedRegion).ToArray();
            filter.SelectedRealmIndex = 0;
            this.OnRegionChanged(filter.SelectedRegion);
        }

        private void cboRealms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var filter = this.DataContext as MainAuctionHouseFilterModel;
            this.StartDate.DisplayDateStart = filter.SelectedRealmModel.StartedTrackingAuctionHouse;
            this.StartDate.DisplayDateEnd = filter.SelectedRealmModel.LastAuctionHouseUpdate;
            this.EndDate.DisplayDateStart = filter.SelectedRealmModel.StartedTrackingAuctionHouse;
            this.EndDate.DisplayDateEnd = filter.SelectedRealmModel.LastAuctionHouseUpdate;
            this.StartDate.SelectedDate = this.StartDate.DisplayDateEnd;
            this.EndDate.SelectedDate = this.StartDate.DisplayDateEnd;

            this.StartTime.Value = this.StartDate.SelectedDate;
            this.EndTime.Value = this.EndDate.SelectedDate;
        }
    }
}
