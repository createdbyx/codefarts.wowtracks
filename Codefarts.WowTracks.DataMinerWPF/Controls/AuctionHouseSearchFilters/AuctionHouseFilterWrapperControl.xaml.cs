﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Models;

    /// <summary>
    /// Interaction logic for AuctionHouseFilterWrapperControl.xaml
    /// </summary>
    public partial class AuctionHouseFilterWrapperControl : UserControl
    {
        public event EventHandler Delete;


        protected virtual void OnDelete()
        {
            var handler = this.Delete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public event EventHandler MoveUp;

        protected virtual void OnMoveUp()
        {
            var handler = this.MoveUp;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public event EventHandler MoveDown;

        protected virtual void OnMoveDown()
        {
            var handler = this.MoveDown;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public AuctionHouseFilterWrapperControl()
        {
            InitializeComponent();
        }

        public object Content
        {
            get
            {
                return this.Expander.Content;
            }

            set
            {
                this.Expander.Content = value;
            }
        }     

        public IAuctionHouseSearchFilter Model { get; set; }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnDelete();
        }

        private void ModeUpButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnMoveUp();
        }

        private void MoveDownButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnMoveDown();
        }
    }
}
