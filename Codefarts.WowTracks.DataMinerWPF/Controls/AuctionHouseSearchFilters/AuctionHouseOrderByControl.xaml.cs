﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;     

    /// <summary>
    /// Interaction logic for AuctionHouseOrderByControl.xaml
    /// </summary>
    public partial class AuctionHouseOrderByControl : UserControl, IAuctionHouseFilterControl
    {
        public AuctionHouseOrderByControl()
        {
            InitializeComponent();
        }


        public void SetModel(IAuctionHouseSearchFilter model)
        {
            this.DataContext = model;
        }

        public void SetApplication(DataMinerAppCore.Application app)
        {
             
        }                                               
    }
}
