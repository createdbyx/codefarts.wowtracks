﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;

    /// <summary>
    /// Interaction logic for AuctionHouseBuyoutControl.xaml
    /// </summary>
    public partial class AuctionHouseBuyoutControl : UserControl  ,IAuctionHouseFilterControl
    {
        public AuctionHouseBuyoutControl()
        {
            InitializeComponent();
        }

        public void SetModel(IAuctionHouseSearchFilter model)
        {
            this.DataContext = model;
        }

        public void SetApplication(Application app)
        {
            
        }
    }
}
