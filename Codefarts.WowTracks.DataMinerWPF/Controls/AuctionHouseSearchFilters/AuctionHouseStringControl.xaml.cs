﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;

    /// <summary>
    /// Interaction logic for AuctionHouseStringControl.xaml
    /// </summary>
    public partial class AuctionHouseStringControl : UserControl  ,IAuctionHouseFilterControl
    {
        public AuctionHouseStringControl()
        {
            InitializeComponent();
        }

        public void SetModel(IAuctionHouseSearchFilter model)
        {
            this.DataContext = model;
        }

        public void SetApplication(Application app)
        {
            
        }
    }
}
