﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;

    /// <summary>
    /// Interaction logic for AuctionHouseSourceTypeControl.xaml
    /// </summary>
    public partial class AuctionHouseSourceTypeControl : UserControl, IAuctionHouseFilterControl
    {
        public AuctionHouseSourceTypeControl()
        {
            InitializeComponent();
        }

        public void SetModel(IAuctionHouseSearchFilter model)
        {
            this.DataContext = model;
        }

        public void SetApplication(Application app)
        {
        }
    }
}
