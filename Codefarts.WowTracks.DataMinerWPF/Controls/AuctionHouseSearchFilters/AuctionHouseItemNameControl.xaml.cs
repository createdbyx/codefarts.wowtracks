﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls.AuctionHouseSearchFilters
{
    using System;
    using System.Data;
    using System.IO;
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Models.AuctionHouseSearchFilters;

    /// <summary>
    /// Interaction logic for AuctionHouseItemNameControl.xaml
    /// </summary>
    public partial class AuctionHouseItemNameControl : UserControl, IAuctionHouseFilterControl
    {
        private Application app;

        public AuctionHouseItemNameControl()
        {
            InitializeComponent();  
        }

        public void SetModel(IAuctionHouseSearchFilter model)
        {
            if (!(model is AuctionHouseFilterItemNameModel))
            {
                throw new ArgumentException(string.Format("Not of type {0}", typeof(AuctionHouseFilterItemNameModel).Name), "model");
            }

            this.DataContext = model;   
        }

        public void SetApplication(Application app)
        {
            this.app = app;
        }

        private void ItemList_DropDownOpened(object sender, System.EventArgs e)
        {
            
        }

        //private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (e.AddedItems.Count == 0)
        //    {
        //        return;
        //    }

        //    var model = this.DataContext as AuctionHouseFilterItemNameModel;
        //    model.Value = (e.AddedItems[0] as ItemDisplayModel).Name;
        //}

        //private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //    //if (model.Handled)
        //    //{
        //      return;   
        //    //}

        //    // model.Handled = true;
        //    if (e.AddedItems != null && e.AddedItems.Count > 0)
        //    {
        //        var model = this.DataContext as Models.AuctionHouseSearchFilters.AuctionHouseFilterItemNameModel;

        //        var item = e.AddedItems[0] as ItemDisplayModel;
        //        // this.ItemList.SelectedValue = item.Name;
        //        model.Value = item.Name;

        //        // this.ItemList.SelectedItem = item.Name;
        //    }

        //    //   model.Handled = false;
        //}
    }
}
