﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Controls
{
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;

    /// <summary>
    /// Interaction logic for AuctionHouseTimeLeftFilterControl.xaml
    /// </summary>
    public partial class AuctionHouseTimeLeftFilterControl : UserControl, IAuctionHouseFilterControl
    {
        public AuctionHouseTimeLeftFilterControl()
        {
            this.InitializeComponent();
        }

        public void SetModel(IAuctionHouseSearchFilter model)
        {
            this.DataContext = model;
        }

        public void SetApplication(Application app)
        {

        }
    }
}
