﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.ResultPlugins
{
    using System;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;

    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataModels;

    using Application = Codefarts.WowTracks.DataMinerAppCore.Application;

    /// <summary>
    /// Interaction logic for DataGridResultsControl.xaml
    /// </summary>
    public partial class DataGridResultsControl : UserControl, IResultsControl
    {
        private IResultsPlugin plugin;

        private Application app;

        public DataGridResultsControl()
        {
            InitializeComponent();
        }


        public void Initialize(Application app, IResultsPlugin plugin)
        {
            this.plugin = plugin;
            this.app = app;
        }

        private void GoToArmory_Click(object sender, RoutedEventArgs e)
        {
            var selected = this.ResultsGrid.SelectedItem as AuctionDataModel;
            if (selected == null)
            {
                return;
            }

            var region = WOWSharp.Community.Region.AllRegions.FirstOrDefault(x => x.Name == this.app.MainAuctionHouseFilterModel.SelectedRegion);
            if (region != null)
            {
                this.app.BrowseTo(string.Format("http://{0}.battle.net/wow/{2}/item/{1}", region.Name, selected.ItemId, region.DefaultLocale));
            }
        }

        private void GoToWowhead_Click(object sender, RoutedEventArgs e)
        {
            var selected = this.ResultsGrid.SelectedItem as AuctionDataModel;
            if (selected != null)
            {
                this.app.BrowseTo(string.Format("http://www.wowhead.com/item={0}", selected.ItemId));
            }
        }

        private void GoToUndermineJournal_Click(object sender, RoutedEventArgs e)
        {
            var selected = this.ResultsGrid.SelectedItem as AuctionDataModel;
            if (selected != null)
            {
                var faction = selected.Faction.ToString()[0];
                var region = this.app.MainAuctionHouseFilterModel.SelectedRegion == WOWSharp.Community.Region.EU.Name ? "eu." : string.Empty;
                this.app.BrowseTo(
                    string.Format(
                        "https://{3}theunderminejournal.com/item.php?realm={0}-{1}&item={2}",
                        faction,
                        this.app.MainAuctionHouseFilterModel.SelectedRealm,
                        selected.ItemId,
                        region));
            }
        }  
        
         private void GoToWowuction_Click(object sender, RoutedEventArgs e)
        {
            var selected = this.ResultsGrid.SelectedItem as AuctionDataModel;
            if (selected != null)
            {
                var faction = selected.Faction.ToString();
                  this.app.BrowseTo(
                    string.Format(
                        "http://www.wowuction.com/{3}/{1}/{0}/Items/Stats/{2}",
                        faction.ToLower(),
                        this.app.MainAuctionHouseFilterModel.SelectedRealm.Replace(" ","-").ToLower(),
                        selected.ItemId,
                        this.app.MainAuctionHouseFilterModel.SelectedRegion.ToLower()));
            }
        }  
        
    }
}
