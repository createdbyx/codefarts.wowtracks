﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Readers
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;
    using Codefarts.WowTracks.DataModels;

    using Newtonsoft.Json;

    [Export(typeof(IReader<string>))]
    public class ItemIdNamesReader : IReader<string>
    {
        public Type Type
        {
            get
            {
                return typeof(NameIdModel[]);
            }
        }

        public object Read(string key, ContentManager<string> content)
        {
            var path = Path.Combine(string.IsNullOrWhiteSpace(content.RootDirectory) ? string.Empty : content.RootDirectory, key);
            if (Path.IsPathRooted(key))
            {
                path = key;
            }

            return JsonConvert.DeserializeObject<NameIdModel[]>(File.ReadAllText(path));
        }

        public bool CanRead(string key, ContentManager<string> content)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return false;
            }

            if (!Path.HasExtension(key))
            {
                return false;
            }

            if (Path.GetExtension(key).ToLower() != ".json")
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reads a file asynchronously and returns a type representing the data.
        /// </summary>
        /// <param name="key">The id to be read.</param>
        /// <param name="content">A reference to the content manager that invoked the read.</param>
        /// <param name="completedCallback">Specifies a callback that will be invoked when the read is complete.</param>
        public void ReadAsync(string key, ContentManager<string> content, Action<ReadAsyncArgs<string, object>> completedCallback)
        {
            Task.Factory.StartNew(
                () =>
                {
                    var result = this.Read(key, content);
                    completedCallback(new ReadAsyncArgs<string, object>() { State = ReadState.Completed, Result = result });
                });
        }
    }
}