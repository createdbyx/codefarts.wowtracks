﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.Readers
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;
    using Codefarts.WowTracks.DataModels;

    using Newtonsoft.Json;

    [Export(typeof(IReader<string>))]
    public class ItemReader : IReader<string>
    {
        public Type Type
        {
            get
            {
                return typeof(ItemInfo);
            }
        }

        public object Read(string key, ContentManager<string> content)
        {
            var path = Path.Combine(content.RootDirectory, key);
            if (Path.IsPathRooted(key))
            {
                path = key;
            }

            //var tempPath = Path.Combine(content.RootDirectory, "ItemIds", Path.ChangeExtension(key, ".json"));
            //if (!File.Exists(key) && File.Exists(tempPath))
            //{
            //    path = tempPath;
            //}

            return JsonConvert.DeserializeObject<ItemInfo>(File.ReadAllText(path));
        }

        public bool CanRead(string key, ContentManager<string> content)
        {      
            if (string.IsNullOrWhiteSpace(key))
            {
                return false;
            }
            
            var path = Path.Combine(content.RootDirectory, key);
            if (Path.IsPathRooted(key))
            {
                path = key;
            } 

            if (!Path.HasExtension(path))
            {
                return false;
                //return File.Exists(Path.Combine(content.RootDirectory, "ItemIds", Path.ChangeExtension(key, ".json"))) ||
                //       File.Exists(Path.Combine(content.RootDirectory, Path.ChangeExtension(key, ".json")));
            }

            if (Path.GetExtension(path).ToLower() != ".json")
            {
                return false;
            }
                                   
            return File.Exists(path);
            //return File.Exists(Path.Combine(content.RootDirectory, "ItemIds", key)) ||
            //       File.Exists(Path.Combine(content.RootDirectory, key));
        }

        public void ReadAsync(string key, ContentManager<string> content, Action<ReadAsyncArgs<string, object>> completedCallback)
        {
            //    throw new NotImplementedException();
            //}

            //public void ReadAsync(string key, ContentManager<string> content, Action<object> completedCallback)
            //{
            Task.Factory.StartNew(
                () =>
                {
                    var result = this.Read(key, content);
                    completedCallback(new ReadAsyncArgs<string, object>() { State = ReadState.Completed, Result = result });
                });
        }
    }
}
