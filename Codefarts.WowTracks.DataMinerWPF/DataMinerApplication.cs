﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;
    using Codefarts.Logging;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataModels;

    internal class DataMinerApplication : Application
    {
        private SettingsModel settings;
        private bool namesLoaded;

        internal event EventHandler<BrowseToEventArgs> Browse;

        protected virtual void OnBrowse(BrowseToEventArgs e)
        {
            var handler = this.Browse;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public override void BrowseTo(string url)
        {
            this.OnBrowse(new BrowseToEventArgs() { Url = url });
        }

        public DataMinerApplication(SettingsModel settings)
        {
            this.settings = settings;
            ContentManager<string>.Instance.RootDirectory = this.settings.DataFolder;
        }

        public async override void Initialize()
        {
            // base.Initialize();
            //   await this.LoadItemNameAndIdTable();
            //    await this.BuildDisplayItems();
        }

        internal new void CancelBuildDisplayItems()
        {
            base.CancelBuildDisplayItems();
        }

        //public   override  IDictionary<string, List<NameIdModel>> ItemNames
        //{
        //    get
        //    {
        //      this.LoadItemNameAndIdTable();
        //        return base.ItemNames;
        //    }
        //}

        //internal async Task LoadItemNameAndIdTable()
        //{
        //    //var region = this.mainAuctionHouseFilterModel.SelectedRegion;
        //    //if (string.IsNullOrWhiteSpace(region))
        //    //{
        //    //    return;
        //    //}
        //    if (this.namesLoaded)
        //    {
        //        return;
        //    }

        //    await Task.Factory.StartNew(
        //          () =>
        //          {
        //              lock (this.itemsTable)
        //              {
        //                  foreach (var region in WOWSharp.Community.Region.AllRegions)
        //                  {
        //                      var fileName = Path.Combine(this.settings.DataFolder, region.Name, "ItemIdNameTable.json");

        //                      // ensure key exists
        //                      if (this.itemsTable.ContainsKey(region.Name))
        //                      {
        //                          continue;
        //                      }

        //                      this.itemsTable.Add(region.Name, new List<NameIdModel>());

        //                      // check if there is an existing file and if so read it and cache it's data
        //                      if (!File.Exists(fileName))
        //                      {
        //                          continue;
        //                      }

        //                      this.itemsTable[region.Name].Clear();
        //                      //var dataSet = JsonConvert.DeserializeObject<NameIdModel[]>(File.ReadAllText(fileName));
        //                      var dataSet = ContentManager<string>.Instance.Load<NameIdModel[]>(fileName, false);
        //                      this.itemsTable[region.Name].AddRange(dataSet);
        //                      Logging.Log(
        //                          LogEntryType.Information,
        //                          string.Format("Loaded {0} item data set into items table for region {1}", dataSet.Length, region),
        //                          "RebuildItemsTable");

        //                      this.namesLoaded = true;
        //                  }
        //              }
        //          });
        //}
    }
}