﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF
{
    using System;

    public class RegionEventArgs : EventArgs
    {
        public RegionEventArgs(string region)
        {
            this.Region = region;
        }

        public string Region { get; set; }
    }
}