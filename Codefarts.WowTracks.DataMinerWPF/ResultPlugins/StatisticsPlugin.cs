﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.ResultPlugins
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;

    using Codefarts.Logging;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Controls.ResultControls;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataMinerWPF.Models.ResultsModels;
    using Codefarts.WowTracks.DataModels;

    using Application = Codefarts.WowTracks.DataMinerAppCore.Application;

    [Export(typeof(IResultsPlugin))]
    public class StatisticsPlugin : IResultsPlugin
    {
        private bool connected;

        private Application app;

        private StatisticsModel model = new StatisticsModel();

        public async void UpdateData(AuctionDataModel[] dataModels, IResultsControl control)
        {
            var userControl = control as StatisticsResultsControl;
            if (userControl == null)
            {
                throw new InvalidCastException("Control is not of type StatisticsResultsControl");
            }

            userControl.LoadingStatus.Visibility = Visibility.Visible;

            await Task.Factory.StartNew(
                () =>
                {
                    this.GetTotals(dataModels);
                    this.GetMinMaxPricedItems(dataModels);
                    this.GetMostActiveToons(dataModels);

                    userControl.Dispatcher.Invoke(
                        () =>
                        {
                            userControl.DataContext = this.model;
                            userControl.LoadingStatus.Visibility = Visibility.Hidden;
                        });
                });
        }

        private void GetMostActiveToons(AuctionDataModel[] dataModels)
        {
            // find most active posters
            var uniqueAuctions = dataModels.GroupBy(d => d.AuctionId).Select(d => d.First());
            var orderGroups = uniqueAuctions.GroupBy(p => p.OwnerName).Select(g => new { OwnerName = g.Key, Items = g });

            this.model.MostActiveToonsByAuctions = orderGroups.OrderByDescending(g => g.Items.Count()).Select(x => new NameValueModel(x.Items.Count(), x.OwnerName)).ToArray();
            this.model.MostActiveToonsByQuantity = orderGroups.OrderByDescending(g => g.Items.Sum(x => x.Quantity)).Select(x => new NameValueModel(x.Items.Sum(y => y.Quantity), x.OwnerName)).ToArray();

            this.model.BiggestMoneyMakersByBids = orderGroups.OrderByDescending(g => g.Items.Sum(x => x.CurrentBidValue)).Select(x => new NameValueModel(x.Items.Sum(y => y.CurrentBidValue), x.OwnerName)).ToArray();
            this.model.BiggestMoneyMakersByBuyouts = orderGroups.OrderByDescending(g => g.Items.Sum(y => y.BuyoutValue.HasValue ? y.BuyoutValue.Value : 0)).Select(x => new NameValueModel(x.Items.Sum(y => y.BuyoutValue.HasValue ? y.BuyoutValue.Value : 0), x.OwnerName)).ToArray();
        }

        private void GetTotals(AuctionDataModel[] dataModels)
        {
            this.model.TotalItems = dataModels.Length;
            this.model.TotalAllianceItems = dataModels.Count(x => x.Faction == Faction.Alliance);
            this.model.TotalHordeItems = dataModels.Count(x => x.Faction == Faction.Horde);
            this.model.TotalNeutralItems = dataModels.Count(x => x.Faction == Faction.Neutral);

            this.model.ActualItems = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().Count();
            this.model.ActualAllianceItems = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().Count(x => x.Faction == Faction.Alliance);
            this.model.ActualHordeItems = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().Count(x => x.Faction == Faction.Horde);
            this.model.ActualNeutralItems = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().Count(x => x.Faction == Faction.Neutral);
        }

        private void LogError(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Error, string.Format(message, args), "StatisticsResultsPlugin");
        }

        private void LogInfo(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Information, string.Format(message, args), "StatisticsResultsPlugin");
        }

        private void GetMinMaxPricedItems(AuctionDataModel[] dataModels)
        {
            var cheapestBuyouts = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().OrderBy(x => x.BuyoutValue);
            var cheapestBids = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().OrderBy(x => x.CurrentBidValue);
            var expensiveBuyouts = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().OrderByDescending(x => x.BuyoutValue);
            var expensiveBids = dataModels.AsParallel().GroupBy(x => x.AuctionId).Select(x => x.First()).AsParallel().OrderByDescending(x => x.CurrentBidValue);

            var callback = new Func<AuctionDataModel, AuctionDataGridModel>(
                x =>
                {
                    AuctionDataGridModel data = null;
                    try
                    {
                        var region = this.app.MainAuctionHouseFilterModel.SelectedRegion;
                        var contentManager = ContentManager.ContentManager<string>.Instance;
                        var filePath = Path.Combine(contentManager.RootDirectory, region, "ItemIds", x.ItemId + ".json");
                        if (File.Exists(filePath))
                        {
                            var itemDisplayModel = new ItemDisplayModel(region, contentManager.Load<ItemInfo>(filePath));
                            data = new AuctionDataGridModel(x, itemDisplayModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        //throw;
                        this.LogError("ItemID: {0}\r\n{1}", x.ItemId, ex.Message);
                    }

                    return data;
                });

            this.model.CheapestItems = cheapestBids.Select(callback).Where(x => x != null).Take(25).ToArray();
            this.model.MostExpensiveItems = expensiveBuyouts.Select(callback).Where(x => x != null).Take(25).ToArray();

            var cheapestBuyout = cheapestBuyouts.FirstOrDefault();
            var cheapestBid = cheapestBids.FirstOrDefault();
            var expensiveBuyout = expensiveBuyouts.FirstOrDefault();
            var expensiveBid = expensiveBids.FirstOrDefault();

            this.model.CheapestItemBuyout = cheapestBuyout; //.BuyoutValue.HasValue ? cheapestBuyout.BuyoutValue.Value : -1;
            this.model.CheapestItemBid = cheapestBid; //.CurrentBidValue;
            if (cheapestBid != null)
            {
                this.model.CheapestItemBidDisplay = new ItemDisplayModel(this.app.MainAuctionHouseFilterModel.SelectedRegion, cheapestBid);
            }

            if (cheapestBuyout != null)
            {
                this.model.CheapestItemBuyoutDisplay = new ItemDisplayModel(this.app.MainAuctionHouseFilterModel.SelectedRegion, cheapestBuyout);
            }

            this.model.MostExpensiveItemBuyout = expensiveBuyout; //.BuyoutValue.HasValue ? expensiveBuyout.BuyoutValue.Value : -1;
            this.model.MostExpensiveItemBid = expensiveBid; //.CurrentBidValue;
            if (expensiveBid != null)
            {
                this.model.MostExpensiveItemBidDisplay = new ItemDisplayModel(this.app.MainAuctionHouseFilterModel.SelectedRegion, expensiveBid);
            }

            if (expensiveBuyout != null)
            {
                this.model.MostExpensiveItemBuyoutDisplay = new ItemDisplayModel(this.app.MainAuctionHouseFilterModel.SelectedRegion, expensiveBuyout);
            }
        }

        public Guid UniqueId
        {
            get
            {
                return new Guid("AFC01115-18D4-48B3-AF6C-C520EEAD9F82");
            }
        }

        public string Control
        {
            get
            {
                return typeof(StatisticsResultsControl).FullName;
            }
        }

        public string Name
        {
            get
            {
                return "Statistics";
            }
        }

        public void Connect(Application app)
        {
            this.app = app;
            this.connected = true;
        }

        public void Disconnect()
        {
            this.app = null;
            this.connected = false;
        }

        public bool IsConnected
        {
            get
            {
                return this.connected;
            }
        }
    }
}
