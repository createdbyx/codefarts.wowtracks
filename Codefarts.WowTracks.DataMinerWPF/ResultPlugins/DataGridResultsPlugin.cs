﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.DataMinerWPF.ResultPlugins
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;

    using Codefarts.Logging;
    using Codefarts.WowTracks.DataMinerAppCore;
    using Codefarts.WowTracks.DataMinerAppCore.Models;
    using Codefarts.WowTracks.DataMinerWPF.Models;
    using Codefarts.WowTracks.DataModels;

    using Application = Codefarts.WowTracks.DataMinerAppCore.Application;

    [Export(typeof(IResultsPlugin))]
    public class DataGridResultPlugin : IResultsPlugin
    {
        private DataGridModel dataGridModel = new DataGridModel();

        private Application app;

        private bool connected;

        public async void UpdateData(AuctionDataModel[] dataModels, IResultsControl control)
        {
            var userControl = control as DataGridResultsControl;
            if (userControl == null)
            {
                throw new InvalidCastException("Control is not of type DataGridResultsControl");
            }

            userControl.LoadingStatus.Visibility = Visibility.Visible;

            await Task.Factory.StartNew(
                () =>
                    {
                        var contentManager = ContentManager.ContentManager<string>.Instance;
                        this.dataGridModel.Models = dataModels.Select(
                            x =>
                                {
                                    AuctionDataGridModel data = null;
                                    try
                                    {
                                        var region = this.app.MainAuctionHouseFilterModel.SelectedRegion;
                                        var filePath = Path.Combine(contentManager.RootDirectory, region, "ItemIds", x.ItemId + ".json");
                                        if (File.Exists(filePath))
                                        {
                                            var itemDisplayModel = new ItemDisplayModel(region, contentManager.Load<ItemInfo>(filePath));
                                            data = new AuctionDataGridModel(x, itemDisplayModel);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //throw;
                                        this.LogError("ItemID: {0}\r\n{1}", x.ItemId, ex.Message);
                                    }

                                    return data;
                                }).Where(x => x != null).AsParallel().ToArray();

                        userControl.Dispatcher.Invoke(
                            () =>
                                {
                                    userControl.DataContext = this.dataGridModel;
                                    userControl.LoadingStatus.Visibility = Visibility.Hidden;
                                });
                    });
        }

        private void LogError(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Error, string.Format(message, args), "DateGridResultsPlugin");
        }

        private void LogInfo(string message, params object[] args)
        {
            Logging.Log(LogEntryType.Information, string.Format(message, args), "DateGridResultsPlugin");
        }

        public Guid UniqueId
        {
            get
            {
                return new Guid("9E3FB94B-930B-44CC-973C-C5768C596264");
            }
        }

        public string Control
        {
            get
            {
                return typeof(DataGridResultsControl).FullName;
            }
        }

        public string Name
        {
            get
            {
                return "Grid";
            }
        }

        public void Connect(Application app)
        {
            this.app = app;
            this.connected = true;
        }

        public void Disconnect()
        {
            this.app = null;
            this.connected = false;
        }

        public bool IsConnected
        {
            get
            {
                return this.connected;
            }
        }
    }
}
