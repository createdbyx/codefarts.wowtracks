﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Repository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;
    using Codefarts.Settings;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.XmlRepository;

    using WOWSharp.Community.Wow;

    using AuctionTimeLeft = WOWSharp.Community.Wow.AuctionTimeLeft;
    using Faction = Codefarts.WowTracks.DataModels.Faction;
    using ItemQuality = Codefarts.WowTracks.DataModels.ItemQuality;

    [Export(typeof(IRepository))]
    public class XmlRepository : IRepository
    {
        private IdManager.IdManager realmIdManager = new IdManager.IdManager();

        private TrackingModel model;

        private object lockObject = new object();
        private DateTime lastRead;

        public XmlRepository()
        {
            ContentManager<string>.Instance.Register(new AuctionHouseDataReader());
        }

        private async Task DoLoad()
        {
            if (this.model != null)
            {
                return;
            }

            var dataFolder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
            var fileName = Path.Combine(dataFolder, "Trackers.xml");

            if (!File.Exists(fileName))
            {
                this.model = new TrackingModel();
                return;
            }

            var result = await FileStorageAdapter.LoadData<TrackingModel>(fileName);
            lock (this.lockObject)
            {
                this.lastRead = DateTime.Now;
                this.realmIdManager.RegisterId(result.Realms.Select(x => x.Id));
                this.model = result;
            }
        }

        private async Task DoSave()
        {
            var dataFolder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
            var fileName = Path.Combine(dataFolder, "Trackers.xml");
            await FileStorageAdapter.SaveData(this.model, fileName);
        }

        public async Task StopTrackingRealm(RealmTrackingModel model)
        {
            await this.DoLoad();
            var index = 0;
            while (index < this.model.Realms.Count)
            {
                var trackingRealm = this.model.Realms[index];
                if (trackingRealm.Id == model.Id)
                {
                    this.model.Realms.RemoveAt(index);
                    this.realmIdManager.ReleaseId(model.Id);
                    await this.DoSave();
                    return;
                }

                index++;
            }

            //lock (this.lockObject)
            //{
            //    var index = this.model.Realms.TakeWhile(x => x.Id != model.Id).Count() - 1;
            //    if (index > -1)
            //    {
            //        this.model.Realms.RemoveAt(index);
            //    }
            //}

            //await this.DoSave();
        }

        public async Task<bool> IsTrackingRealm(RealmTrackingModel model)
        {
            await this.DoLoad();

            lock (this.lockObject)
            {
                return this.model.Realms.Any(x => x.Id == model.Id ||
                    (string.Compare(x.Name, model.Name, StringComparison.OrdinalIgnoreCase) == 0 &&
                    string.Compare(x.Region, model.Region, StringComparison.OrdinalIgnoreCase) == 0));
            }
        }

        public async Task UpdateRealm(RealmTrackingModel model)
        {
            await this.DoLoad();

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (model.Id < 0)
            {
                throw new ArgumentOutOfRangeException("model");
            }

            RealmTrackingModel item;
            lock (this.lockObject)
            {
                item = this.model.Realms.FirstOrDefault(x => x.Id == model.Id);
            }

            if (item != null)
            {
                item.AuctionHouseTrackingInterval = model.AuctionHouseTrackingInterval;
                item.LastAuctionHouseUpdate = model.LastAuctionHouseUpdate;
                item.LastUpdate = model.LastUpdate;
                item.Name = model.Name;
                item.Region = model.Region;
                item.StartedTracking = model.StartedTracking;
                item.StartedTrackingAuctionHouse = model.StartedTrackingAuctionHouse;
                item.TrackAuctionHouse = model.TrackAuctionHouse;
                item.TrackingInterval = model.TrackingInterval;
            }

            await this.DoSave();
        }

        public async Task<IEnumerable<RealmTrackingModel>> GetRealmsBeingTracked(int page, int itemsPerPage)
        {
            await this.DoLoad();

            itemsPerPage = itemsPerPage < 1 ? 1 : itemsPerPage;
            itemsPerPage = itemsPerPage > 100 ? 100 : itemsPerPage;
            lock (this.lockObject)
            {
                return this.model.Realms.Skip(page * itemsPerPage).Take(itemsPerPage).Select(x => x.Clone());
            }
        }

        public async Task<int> GetNumberOfRealmsBeingTracked()
        {
            await this.DoLoad();

            lock (this.lockObject)
            {
                return this.model.Realms.Count;
            }
        }

        public async Task<AuctionDataModel[]> GetAuctionHouseData(string region, string realmName, DateTime startDate, DateTime endDate, Action<float, string> callback, CancellationToken cancelToken)
        {
            return await Task<AuctionDataModel[]>.Factory.StartNew(
                () =>
                {
                    var dataFolder = SettingsManager.Instance.GetSetting(SettingConstants.DataFolder, string.Empty);
                    var path = Path.Combine(dataFolder, region, realmName, "AuctionHouse");

                    var yearCount = Math.Max(endDate.Year - startDate.Year, 1);
                    var searchDirectories = new List<string>();

                    // build years
                    for (var i = 0; i < yearCount; i++)
                    {
                        if (cancelToken != null && cancelToken.IsCancellationRequested)
                        {
                            return null;
                        }

                        var yearString = (startDate.Year + i).ToString(CultureInfo.InvariantCulture);
                        var directories = Directory.GetDirectories(
                            Path.Combine(path, yearString),
                            "*.*",
                            SearchOption.TopDirectoryOnly);

                        // check to filter directories
                        if (i == 0)
                        {
                            directories = directories.Where(x => int.Parse(Path.GetFileName(x)) >= startDate.Month).ToArray();
                        }
                        else if (i == yearCount - 1)
                        {
                            directories = directories.Where(x => int.Parse(Path.GetFileName(x)) <= endDate.Month).ToArray();
                        }

                        searchDirectories.AddRange(directories);
                    }

                    // callback to convert to AuctionDataModel with faction and capture date

                    // process files in each directory
                    //var files = from folder in searchDirectories
                    //            from file in Directory.GetFiles(folder, "*.zip", SearchOption.TopDirectoryOnly)
                    //            let captureDate = DateTime.ParseExact(Path.GetFileNameWithoutExtension(file), @"M-d-yyyy hh-mm-ss tt", null)
                    //            where captureDate >= startDate && captureDate <= endDate
                    //            select new { Date = captureDate, File = file };

                    var results = new List<AuctionDataModel>();
                    for (var index = 0; index < searchDirectories.Count; index++)
                    {
                        if (cancelToken != null && cancelToken.IsCancellationRequested)
                        {
                            return null;
                        }

                        var folder = searchDirectories[index];

                        var files = Directory.GetFiles(folder, "*.zip", SearchOption.TopDirectoryOnly);
                        for (var i = 0; i < files.Length; i++)
                        {
                            if (cancelToken != null && cancelToken.IsCancellationRequested)
                            {
                                return null;
                            }

                            var file = files[i];

                            var captureDate = DateTime.ParseExact(
                                Path.GetFileNameWithoutExtension(file),
                                @"M-d-yyyy hh-mm-ss tt",
                                null);

                            var progressValue = (((index + 1) / (float)searchDirectories.Count) * 100) + (i / (float)files.Length);
                            progressValue = progressValue > 100 ? 100 : progressValue;

                            if (callback != null)
                            {
                                callback(
                                    progressValue,
                                    string.Format(
                                        "Fetching {0}-{1} {3}/{4} {5}/{6} {2}",
                                        region,
                                        realmName,
                                        captureDate,
                                        index + 1,
                                        searchDirectories.Count,
                                        i + 1,
                                        files.Length));
                            }

                            if (captureDate >= startDate && captureDate <= endDate)
                            {
                                results.AddRange(this.ReadAuctionDataFile(captureDate, file));
                            }

                            if (callback != null)
                            {
                                callback(
                                     progressValue,
                                    string.Format(
                                        "Fetching {0}-{1} {3}/{4} {5}/{6} {2}",
                                        region,
                                        realmName,
                                        captureDate,
                                        index + 1,
                                        searchDirectories.Count,
                                        i + 1,
                                        files.Length));
                            }
                        }
                    }

                    return results.ToArray();

                    //var results = files.SelectMany(x => this.ReadAuctionDataFile(x.Date, x.File));
                    // select this.ReadAuctionDataFile(captureDate, file);

                    //return results;
                },
                cancelToken);
        }

        private AuctionDataModel[] ReadAuctionDataFile(DateTime captureDate, string file)
        {
            var results = new List<AuctionDataModel>();

            var content = ContentManager<string>.Instance;
            try
            {
                var data = content.Load<AuctionDump>(file);

                // clone cached data
                results.AddRange(data.Alliance.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Alliance)));
                results.AddRange(data.Horde.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Horde)));
                results.AddRange(data.Neutral.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Neutral)));
            }
            catch
            {
                var data = content.Load<AuctionDump_Wow_60>(file);

                // clone cached data
                results.AddRange(data.Auctions.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Alliance)));
            }

            //// read json from zip
            //using (var zipStream = File.OpenRead(file))
            //{
            //    using (var reader = new System.IO.Compression.ZipArchive(zipStream, System.IO.Compression.ZipArchiveMode.Read, false))
            //    {
            //        foreach (var entry in reader.Entries.Where(x => x.FullName.EndsWith(".json", StringComparison.OrdinalIgnoreCase)))
            //        {
            //            using (var streamReader = new StreamReader(entry.Open()))
            //            {
            //                var data = JsonConvert.DeserializeObject<AuctionDump>(streamReader.ReadToEnd());

            //                results.AddRange(data.Alliance.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Alliance)));
            //                results.AddRange(data.Horde.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Horde)));
            //                results.AddRange(data.Neutral.Auctions.AsParallel().Select(x => this.CloneAuctionData(x, captureDate, Faction.Neutral)));
            //            }
            //        }
            //    }
            //}

            return results.ToArray();
        }

        private AuctionDataModel CloneAuctionData(Auction x, DateTime capture, Faction faction)
        {
            return new AuctionDataModel()
                       {
                           AuctionId = x.AuctionId,
                           CurrentBidValue = x.CurrentBidValue,
                           ItemId = x.ItemId,
                           PetBreedId = x.PetBreedId,
                           PetLevel = x.PetLevel,
                           PetSpeciesId = x.PetSpeciesId,
                           Quantity = x.Quantity,
                           Random = x.Random,
                           Seed = x.Seed,
                           BuyoutValue = x.BuyoutValue,
                           OwnerName = x.OwnerName,
                           PetQuality = (ItemQuality)x.PetQuality,
                           TimeLeft = (DataModels.AuctionTimeLeft)x.TimeLeft,
                           CaptureDate = capture,
                           Faction = faction
                       };
        }

        public async Task TrackRealm(RealmTrackingModel model)
        {
            await this.DoLoad();

            lock (this.lockObject)
            {
                var newModel = model.Clone();
                newModel.Id = this.realmIdManager.NewId();

                this.model.Realms.Add(newModel);
                model.Id = newModel.Id;
            }

            await this.DoSave();
        }
    }
}
