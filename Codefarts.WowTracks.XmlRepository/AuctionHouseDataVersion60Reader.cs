﻿namespace Codefarts.WowTracks.Repository
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using Codefarts.ContentManager;

    using Newtonsoft.Json;

    using WOWSharp.Community.Wow;

    [Export(typeof(IReader<string>))]
    public class AuctionHouseDataVersion60Reader : IReader<string>
    {
        public Type Type
        {
            get
            {
                return typeof(AuctionDump_Wow_60 );
            }
        }

        public object Read(string key, ContentManager<string> content)
        {
            AuctionDump_Wow_60 data = null;

            // read json from zip
            var file = Path.IsPathRooted(key) ? key : Path.Combine(content.RootDirectory, key);
            using (var zipStream = File.OpenRead(file))
            {
                using (var reader = new System.IO.Compression.ZipArchive(zipStream, System.IO.Compression.ZipArchiveMode.Read, false))
                {
                    foreach (var entry in reader.Entries.Where(x => x.FullName.EndsWith(".json", StringComparison.OrdinalIgnoreCase)))
                    {
                        using (var streamReader = new StreamReader(entry.Open()))
                        {
                            data = JsonConvert.DeserializeObject<AuctionDump_Wow_60>(streamReader.ReadToEnd());
                        }
                    }
                }
            }

            return data;
        }

        public bool CanRead(string key, ContentManager<string> content)
        {
            return (Path.IsPathRooted(key) && File.Exists(Path.ChangeExtension(key, ".zip"))) || File.Exists(Path.Combine(content.RootDirectory, Path.ChangeExtension(key, ".zip")));
        }

        public void ReadAsync(string key, ContentManager<string> content, Action<ReadAsyncArgs<string, object>> completedCallback)
        {
            Task.Factory.StartNew(
                () =>
                    {
                        var data = this.Read(key, content);
                        completedCallback(new ReadAsyncArgs<string, object>() { Result = data, State = ReadState.Completed, Progress = 100 });
                    });
        }
    }
}