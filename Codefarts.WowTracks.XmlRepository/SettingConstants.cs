﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.Repository
{
    public class SettingConstants
    {
        public const string DataFolder = "Codefarts.WowTracks.DataFolder";
    }
}
