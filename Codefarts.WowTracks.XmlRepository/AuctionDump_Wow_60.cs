namespace Codefarts.WowTracks.Repository
{
    using System.Runtime.Serialization;

    using WOWSharp.Community;
    using WOWSharp.Community.Wow;

    [DataContract]
    public class AuctionDump_Wow_60 : ApiResponse
    {
        [DataMember(IsRequired = true, Name = "realm")]
        public Realm Realm { get; internal set; }

        [DataMember(IsRequired = true, Name = "auctions")]
        public AuctionHouse Auctions { get; internal set; }   
    }
}