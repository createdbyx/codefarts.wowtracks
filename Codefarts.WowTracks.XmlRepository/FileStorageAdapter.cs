﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.XmlRepository
{
    using System.IO;
    using System.Threading;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using System.Threading.Tasks;


    public static class FileStorageAdapter
    {
        async public static Task<T> LoadData<T>(string filename)
        {
            var semaphore = getSemaphore(filename);
            await semaphore.WaitAsync();

            try
            {
                //var storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync(filename);

                //using (var stream = await storageFile.OpenStreamForReadAsync())

                using (var storageFile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None, 8192))
                {
                    using (var stream = new MemoryStream())
                    {
                        await storageFile.CopyToAsync(stream);
                        stream.Seek(0, SeekOrigin.Begin);

                        var serializer = new XmlSerializer(typeof(T));
                        return (T)serializer.Deserialize(stream);
                    }
                }
            }
            finally
            {
                semaphore.Release();
            }
        }

        async public static Task SaveData<T>(T data, string filename)
        {
            var semaphore = getSemaphore(filename);
            await semaphore.WaitAsync();

            try
            {
                //var storageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

                //using (var stream = await storageFile.OpenStreamForWriteAsync())


                using (var stream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(stream, data);
                    stream.Seek(0, SeekOrigin.Begin);

                    using (var storageFile = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None, 8192))
                    {
                        await stream.CopyToAsync(storageFile);
                    }
                }
            }
            finally
            {
                semaphore.Release();
            }
        }

        private static SemaphoreSlim getSemaphore(string filename)
        {
            if (_semaphores.ContainsKey(filename))
            {
                return _semaphores[filename];
            }

            var semaphore = new SemaphoreSlim(1);
            _semaphores[filename] = semaphore;
            return semaphore;
        }

        private static readonly Dictionary<string, SemaphoreSlim> _semaphores = new Dictionary<string, SemaphoreSlim>();
    }
}
