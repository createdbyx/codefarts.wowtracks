﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>                                

namespace Codefarts.WowTracks.FileDownloader
{
    using System.Collections.Generic;
    using System.IO;
    using System;

    using Codefarts.WowTracks.CommandLibrary;

    class Program
    {
        private static bool verbose = true;

        static void Main(string[] args)
        {
            var parameters = new List<string>(args);
            GetSwitches(parameters);

            if (parameters.Count < 2)
            {
                Write("Expected 2 arguments!");
                Environment.ExitCode = -1;
                return;
            }

            Uri uri;
            if (!Uri.TryCreate(parameters[0], UriKind.Absolute, out uri))
            {
                Write("Invalid url specified!");
                Environment.ExitCode = -2;
                return;
            }

            var fileName = parameters[1];
            var bufferSize = 8196;

            var commandArgs = new Dictionary<string, object>
                              {
                                  { "Url", uri }, 
                                  { "FileName", fileName },
                                  { "Callback", new Action<string>(Write) },
                                  { "ProgressCallback", new Func<float, bool>(progress => !Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape) },
                                  { "BufferSize", bufferSize }  
                              };

            if (File.Exists(fileName))
            {
                commandArgs.Add("IfModifiedSince", File.GetLastWriteTime(fileName));
            }

            using (var outputStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize))
            {
                commandArgs.Add("OutputStream", outputStream);

                // execute the command
                var result = DownloadFileCommand.DownloadFile(commandArgs);

                int exitCode;
                if (result.ReturnValues.TryGetParameter("ExitCode", out exitCode))
                {
                    Environment.ExitCode = exitCode;
                }
            }
        }

        private static void GetSwitches(List<string> parameters)
        {
            var index = parameters.IndexOf("-v");
            if (index != -1)
            {
                verbose = false;
                parameters.RemoveAt(index);
            }
        }

        private static void Write(string text)
        {
            if (verbose)
            {
                Console.WriteLine(text);
            }
        }
    }
}
