﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.UnitTests
{
    public abstract class BaseRepositoryUnitTest
    {
        public abstract void SetRepository();  
    }
}
