﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.UnitTests
{
    public class SettingConstants
    {
        public const string DataFolder = "Codefarts.WowTracks.DataFolder";

        public const string AutomaticallyStartDownloadsOnStartup = "Codefarts.WowTracks.StartDownloadsOnStartup";
    }
}
