﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.UnitTests
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    using Codefarts.Settings;
    using Codefarts.WowTracks.DataModels;
    using Codefarts.WowTracks.Repository;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class XmlRepositoryTests : BaseRepositoryUnitTest
    {
        /// <summary>
        /// The folder.
        /// </summary>
        private string folder;

        /// <summary>
        /// The startup.
        /// </summary>
        [TestInitialize]
        public void Startup()
        {
            this.folder = Path.Combine(Environment.CurrentDirectory, "WowTracksRepository");

            this.CleanupFolder();

            // ensure folder exists
            Directory.CreateDirectory(this.folder);

            SettingsManager.Instance.Values = new SettingsMockup() { Path = this.folder };

            this.SetRepository();
        }

        /// <summary>
        /// The cleanup folder.
        /// </summary>
        private void CleanupFolder()
        {
            if (Directory.Exists(this.folder))
            {
                Directory.Delete(this.folder, true);
            }
        }

        /// <summary>
        /// The shut down.
        /// </summary>
        [TestCleanup]
        public void ShutDown()
        {
            this.CleanupFolder();
        }

        [TestMethod]
        public void Constructor()
        {
            Assert.IsNotNull(RepositoryFactory.Instance.Repository, "Expected repository to exist!");
            Assert.AreEqual(0, Directory.GetFiles(this.folder).Length, "There should not be any files created!");
        }

        [TestMethod]
        public async Task AddRealmTracker()
        {
            var repository = RepositoryFactory.Instance.Repository;
            var dateTime = DateTime.Now;
            var model = new RealmTrackingModel()
            {
                Region = "US",
                Name = "Sisters of Elune",
                StartedTracking = dateTime,
                LastUpdate = dateTime.Subtract(TimeSpan.FromDays(30)),
                TrackingInterval = TimeSpan.FromDays(1).Ticks,
                TrackAuctionHouse = true,
                StartedTrackingAuctionHouse = dateTime,
                LastAuctionHouseUpdate = dateTime.Subtract(TimeSpan.FromDays(30)),
                AuctionHouseTrackingInterval = TimeSpan.FromHours(1).Ticks
            };

            await repository.TrackRealm(model);

            var files = Directory.GetFiles(this.folder);
            Assert.AreEqual(1, files.Length, "There should be 1 file created!");
            Assert.AreEqual("Trackers.xml", Path.GetFileName(files[0]), "Expected filename to be Trackers.xml");

            TrackingModel result;
            using (var storageFile = new FileStream(files[0], FileMode.Open, FileAccess.Read, FileShare.None, 8192))
            {
                var serializer = new XmlSerializer(typeof(TrackingModel));
                result = (TrackingModel)serializer.Deserialize(storageFile);
            }

            Assert.IsNotNull(result, "Result should no be null!");
            Assert.AreEqual(1, result.Realms.Count, "Should only be one realm!");

            // check that the data matches
            Assert.AreEqual(0, model.Id, "Id does not match!");
            Assert.AreEqual("US", model.Region, "Region does not match!");
            Assert.AreEqual("Sisters of Elune", model.Name, "Name does not match!");
            Assert.AreEqual(dateTime, model.StartedTracking, "StartedTracking does not match!");
            Assert.AreEqual(dateTime.Subtract(TimeSpan.FromDays(30)), model.LastUpdate, "LastUpdate does not match!");
            Assert.AreEqual(TimeSpan.FromDays(1).Ticks, model.TrackingInterval, "TrackingInterval does not match!");
            Assert.AreEqual(true, model.TrackAuctionHouse, "TrackAuctionHouse does not match!");
            Assert.AreEqual(dateTime, model.StartedTrackingAuctionHouse, "StartedTrackingAuctionHouse does not match!");
            Assert.AreEqual(dateTime.Subtract(TimeSpan.FromDays(30)), model.LastAuctionHouseUpdate, "LastAuctionHouseUpdate does not match!");
            Assert.AreEqual(TimeSpan.FromHours(1).Ticks, model.AuctionHouseTrackingInterval, "AuctionHouseTrackingInterval does not match!");
        }

        public override void SetRepository()
        {
            RepositoryFactory.Instance.Repository = new XmlRepository();
        }
    }
}
