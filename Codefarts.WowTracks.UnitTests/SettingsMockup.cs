﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.UnitTests
{
    using System;
    using System.Collections.Generic;

    using Codefarts.Settings;

    public class SettingsMockup : IValues<string>
    {
        public SettingsMockup()
        {
            this.HasDataFolderSetting = true;
        }

        public event EventHandler<ValueChangedEventArgs<string>> ValueChanged;
        public string Path { get; set; }
        public int MaxLogFileSize { get; set; }
        public bool HasDataFolderSetting { get; set; }

        public T GetValue<T>(string key)
        {
            switch (key)
            {
                case SettingConstants.DataFolder:
                    if (this.HasDataFolderSetting)
                    {
                        return (T)Convert.ChangeType(this.Path, typeof(T));
                    }

                    break;
            }

            throw new ArgumentException("Bad key given!", "key");
        }

        public bool HasValue(string name)
        {
            switch (name)
            {
                case SettingConstants.DataFolder:
                    return this.HasDataFolderSetting;
            }

            return false;
        }

        public void RemoveValue(string name)
        {
            // do nothing
        }

        public void SetValue(string name, object value)
        {
            // do nothing
        }

        public string[] GetValueKeys()
        {
            var values = new List<string>();
            if (this.HasDataFolderSetting)
            {
                values.Add(SettingConstants.DataFolder);
            }

            return values.ToArray();
        }
    }
}
