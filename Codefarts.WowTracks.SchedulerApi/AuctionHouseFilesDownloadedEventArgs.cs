﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SchedulerApi
{
    public class AuctionHouseFilesDownloadedEventArgs : FileDownloadCompleteEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuctionHouseFilesDownloadedEventArgs"/> class.
        /// </summary>
        public AuctionHouseFilesDownloadedEventArgs()
        {
        }

        public AuctionHouseFilesDownloadedEventArgs(string[] files, AuctionHouseFile fileType)
            : base(files)
        {
            this.FileType = fileType;
        }

        public AuctionHouseFilesDownloadedEventArgs(AuctionHouseFile fileType)
        {
            this.FileType = fileType;
        }

        public AuctionHouseFilesDownloadedEventArgs(string realmName, string region, AuctionHouseFile fileType)
        {
            this.RealmName = realmName;
            this.Region = region;
            this.FileType = fileType;
        }

        public AuctionHouseFilesDownloadedEventArgs(string[] files, string realmName, string region, AuctionHouseFile fileType)
            : base(files)
        {
            this.RealmName = realmName;
            this.Region = region;
            this.FileType = fileType;
        }

        public AuctionHouseFile FileType { get; set; }

        public string Region { get; set; }
        public string RealmName { get; set; }
    }
}