﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SchedulerApi
{
    public interface IPlugin
    {
        void Connect(Application app);

        void Disconnect();
    }
}