﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SchedulerApi
{
    using System;

    public class FileDownloadCompleteEventArgs : EventArgs
    {
        public string[] Files { get; set; }

        public FileDownloadCompleteEventArgs(string[] files)
        {
            this.Files = files;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDownloadCompleteEventArgs"/> class.
        /// </summary>
        public FileDownloadCompleteEventArgs()
        {
        }
    }
}