﻿namespace Codefarts.WowTracks.DataModels
{
    using System;

    public class RealmTrackingModelEventArgs :     EventArgs
    {
        public RealmTrackingModelEventArgs(RealmTrackingModel model)
        {
            this.Model = model;
        }

        public RealmTrackingModelEventArgs()
        {
        }

        public RealmTrackingModel Model { get; set; }
    }
}