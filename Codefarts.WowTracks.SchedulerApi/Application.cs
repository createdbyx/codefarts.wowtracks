﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SchedulerApi
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    public abstract class Application
    {
        private readonly ObservableCollection<IPlugin> plugins = new ObservableCollection<IPlugin>();
        public event EventHandler<AuctionHouseFilesDownloadedEventArgs> AuctionHouseFilesSaved;
        public event EventHandler<ItemDownloadedEventArgs> ItemSaved;

        public virtual void OnItemSaved(ItemDownloadedEventArgs e)
        {
            var handler = this.ItemSaved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler DataFolderChanged;

        public virtual void OnDataFolderChanged()
        {
            var handler = this.DataFolderChanged;
            if (handler != null)
            {                                                        
                handler(this, EventArgs.Empty);
            }
        }

        public virtual void OnAuctionHouseFilesSaved(AuctionHouseFilesDownloadedEventArgs e)
        {
            var handler = this.AuctionHouseFilesSaved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public ObservableCollection<IPlugin> Plugins
        {
            get
            {
                return this.plugins;
            }

            set
            {
                //this.plugins = value;
            }
        }

        public abstract string DataFolder { get; set; }

        public abstract void DownloadItemData(IEnumerable<int> newIds, string region);
    }
}
