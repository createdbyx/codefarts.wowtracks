﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.SchedulerApi
{
    using System;

    public class ItemDownloadedEventArgs : EventArgs
    {
        public ItemDownloadedEventArgs(int itemId, string region, string fileName)
        {
            this.ItemId = itemId;
            this.Region = region;
            this.FileName = fileName;
        }

        public ItemDownloadedEventArgs()
        {
            
        }

        public int ItemId { get; set; }
        public string Region { get; set; }
        public string FileName { get; set; }
    }
}