﻿namespace Codefarts.WowTracks.DataModels
{
    using System;

    public class ItemIdRegionModelEventArgs        : EventArgs
    {
        public ItemIdRegionModelEventArgs(ItemIdRegionModel model)
        {
            this.Model = model;
        }

        public ItemIdRegionModelEventArgs()
        {
        }

        public ItemIdRegionModel Model { get; set; }
    }
}